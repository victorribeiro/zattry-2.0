var elixir = require('laravel-elixir');

var assets_path = 'resources/assets/';
var bower_path = 'vendor/bower_components/';
var module_path = 'app/Modules/';
var store_assets_path = module_path + '/Store/Resources/Assets/';
var correios_assets_path = module_path + '/Correios/Resources/Assets/';
var backoffice_assets_path = module_path + '/Backoffice/Resources/Assets/';

elixir( function (mix) {
  mix

  /**
   * Copy folders
   */
   // Font Awesome
  .copy(bower_path + '/font-awesome/fonts', 'public/fonts/font-awesome')

  /**
   * Admin
   */
  // javascripts
  .scripts([
    // Vendors
    bower_path + 'bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
    bower_path + 'jquery-meiomask/dist/meiomask.min.js',

    // Vendors
    backoffice_assets_path + 'javascripts/modules/activation.js',
    backoffice_assets_path + 'javascripts/modules/distributors.js',

    // Application
    assets_path + 'javascripts/application.js',
    assets_path + 'javascripts/vendors/php2js.js',
    assets_path + 'javascripts/vendors/meiomask.js',
    assets_path + 'javascripts/vendors/datepicker.js',
  ],
  'public/javascripts/admin.js', './')
  // stylesheets
  .less('admin.less', 'public/stylesheets/admin.css')

  /**
   * Default
   */
   // javascripts
  .scripts([
    // Libraries
    bower_path + 'jquery/dist/jquery.min.js',
    bower_path + 'bootstrap/dist/js/bootstrap.min.js',

    // Vendors
    bower_path + 'bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
    bower_path + 'jquery-meiomask/dist/meiomask.min.js',

    // Modules
    store_assets_path + 'javascripts/modules/products.js',
    store_assets_path + 'javascripts/modules/checkout.js',
    store_assets_path + 'javascripts/modules/orders.js',
    correios_assets_path + 'javascripts/modules/correios.js',

    // Application
    assets_path + 'javascripts/application.js',
    assets_path + 'javascripts/vendors/php2js.js',
    assets_path + 'javascripts/vendors/meiomask.js',
    assets_path + 'javascripts/vendors/datepicker.js',
  ],
  'public/javascripts/default.js', './')

  // stylesheets
  .less('default.less', 'public/stylesheets/default.css');
});
