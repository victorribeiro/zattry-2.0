<?php

return [

    'app_name' => 'Zattry 2.0',
    'admin' => 'layouts/admin',
    'backoffice' => 'layouts/backoffice',
    'default' => 'layouts/default',
    'auth' => 'auth::layouts/auth',
    'mail' => 'layouts/mail',
];
