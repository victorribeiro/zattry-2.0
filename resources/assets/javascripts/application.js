var Application = {
  init: function () {
    this.confirmation();
  },

  hideLoading: function () {
    $('#ajax-loading').fadeOut('slow');
  },

  showLoading: function () {
    $('#ajax-loading').fadeIn('fast');
  },

  confirmation: function () {
    $('[data-confirm]').click(function (event) {
      console.log($(this).data('confirm'))
      return confirmation = confirm( $(this).data('confirm') );
    });
  }
};

$(document).on('ready', function () { Application.init(); });
