var Datepicker = {
  init: function () {
    this.simple();
  },

  simple: function () {
    $('.bs-datepicker').datepicker({ format: 'dd/mm/yyyy' });
  }
};

$(document).on('ready', function () { Datepicker.init(); });
