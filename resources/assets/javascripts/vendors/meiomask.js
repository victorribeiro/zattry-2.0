var MeioMask = {
  init: function () {
    this.extras();
    this.simple();
  },

  simple: function () {
    $('input[data-mask]').each(function() {
      var input = $(this);
      input.setMask( input.data('mask') );
    });
  },

  extras: function () {
    $.mask.rules.n = /[0-9]?/

    $.mask.masks['phone-br'] = { mask : '(99) 9999-9999n' };
    $.mask.masks['percent'] = { mask : '199' };
  }
};

$(document).on('ready', function () { MeioMask.init(); });
