<div class="row">
  <div class="col-md-12">
    @if (Session::has('status'))
      <div class="alert alert-success">
        {{ Session::get('status') }}
      </div>
    @endif

    @if(Session::has('success'))
      <div class="alert alert-success">
        <strong>Sucesso! </strong>  {!! Session::get('success') !!}
      </div>
    @endif

    @if(Session::has('danger'))
      <div class="alert alert-danger">
        <strong>Erro! </strong>  {!! Session::get('danger') !!}
      </div>
    @endif

    @if(Session::has('error'))
      <div class="alert alert-danger">
        <strong>Erro! </strong>  {!! Session::get('error') !!}
      </div>
    @endif

    @if(Session::has('info'))
      <div class="alert alert-info">
        <strong>Informação! </strong>  {!! Session::get('info') !!}
      </div>
    @endif

    @if(Session::has('warning'))
      <div class="alert alert-warning">
        <strong>Atenção! </strong>  {!! Session::get('warning') !!}
      </div>
    @endif

    @if ($errors->any())
      <ul class="alert alert-danger">
        <li><strong>Erro!</strong></li>

        @if($errors->any())
          @foreach($errors->getMessages('default') as $key => $messages)
            @foreach ($messages as $message)
              <li><small>* {!! ucfirst($message) !!}</small></li>
            @endforeach
          @endforeach
        @endif
      </ul>
    @endif
  </div>
</div>
