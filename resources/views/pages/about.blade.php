@extends(config('layout.default'))

@section('content')
<div class="container">
  @include('shared/_top_pages')
  <h3>A Zattry</h3>
  <p>é uma empresa de moda que veio para mudar o conceito no mercado de vestuário masculino e feminino de uma forma avassaladora, e com objetivo em acessibilizar seus produtos para todas as classes sociais por meio da venda direta!</p>

  <h3>O Brasil e o mercado em que atuamos</h3>
  <p>O Brasil é considerado, de longe, o maior mercado varejista de roupas da América do Sul. É o que indica o relatório publicado pela A.T. Kearney, renomada empresa de consultoria empresarial norte-americana, considerada uma das maiores empresas de consultoria do mundo. O mesmo relatório diz ainda que nosso mercado lidera com U$42 bilhões em vendas, líder absoluto em relação aos U$14 bilhões do segundo colocado, México. E não é só isso: espera-se que esse mercado alcance mais de U$48 bilhões de vendas até 2025, impulsionado principalmente pela venda direta e pelo rápido desenvolvimento do e-commerce no País.</p>

  <h3>Nossos produtos</h3>
  <p>São produtos de altíssima qualidade que proporcionam bem-estar e conforto trazendo a sensação de liberdade e o prazer de se vestir bem.</p>
</div>
@endsection
