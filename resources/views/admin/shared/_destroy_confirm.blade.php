{{-- Modal --}}
<div class="modal fade" id="{{ $target_modal_id }}">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <span class="fa fa-times close" data-dismiss="modal"></span>
        <h3 class="modal-title modal-title-delete">Atenção!</h3>
      </div>

      <div class="modal-body"> Deseja realmente apagar? </div>

      <div class="modal-footer">
        {!! Form::open([ 'url' => $form_action, 'method' => 'DELETE' ]) !!}
          <button type="button" class="btn btn-flat btn-default btn-modal-close" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-danger">Apagar</button>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
