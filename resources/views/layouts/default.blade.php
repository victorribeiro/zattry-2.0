@extends('layouts/master')

@section('head')
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>{{ config('layout.app_name') }}</title>

  <link href="{{ asset('/stylesheets/default.css') }}" rel="stylesheet" />

  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
@stop

@section('nav')
  <nav id="navigation" class="navbar navbar-inverse">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a href="{{ action('HomeController@index') }}">
          <img id="logo" src="{{ asset('images/logo.png') }}" alt="Zattry" />
        </a>
      </div>

      <ul class="nav navbar-nav">
        <li>
          <a href="{{ action('HomeController@index') }}">Página Inicial</a>
        </li>
        <li>
          <a href="{{ action('PagesController@about') }}">A Empresa</a>
        </li>
        <li>
          <a href="{{ Module::action('Store', 'ProductsController@index') }}">Coleção 2015</a>
        </li>
        {{-- <li>
          <a href="#">Eventos</a>
        </li> --}}
        <li>
          <a href="{{ Module::action('Contact', 'ContactsController@index') }}">Contato</a>
        </li>
      </ul>

      <ul class="nav navbar-nav navbar-right">
        @include('store::shared/_navigation_cart')

        @if (Auth::check())
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-user"></i>
              <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu">
              <li><a href="{{ Module::action('Store', 'HomeController@index') }}">Minhas compras</a></li>
              <li><a href="{{ Module::action('Store', 'ClientsController@index') }}">Meus dados</a></li>
              <li role="separator" class="divider"></li>
              <li>
                <a href="{{ Module::action('Auth', 'SessionsController@logout') }}">
                  <i class="fa fa-sign-out fa-fw"></i> Sair
                </a>
              </li>
            </ul>
          </li>
        @else
          <li>
            <a href="{{ Module::action('Auth', 'SessionsController@login') }}">
              <i class="fa fa-sign-in fa-fw"></i> Entrar
            </a>
          </li>
        @endif
      </ul>
    </div>
  </nav>
@stop

@section('container')
  <div id="ajax-loading">
    <div class="wrap"></div>
    <div class="content">
      <i class="fa fa-refresh fa-4x fa-spin fa-fw"></i>
      <br /> Carrengando...
    </div>
  </div>

  <div class="container">
    @include('shared/_flash_messages')
  </div>

  @yield('content')
@stop

@section('footer')
  <footer class="container">
    <hr />
    <div class="row well">
      <div class="col-md-10">
        <div class="col-md-3">
          <h4>Informações</h4> <hr />
          <p><a href="{{ action('PagesController@about') }}">Sobre nós</a></p>
          <p><a href="#privacy">Política de privacidade</a></p>
        </div>
        <div class="col-md-3">
          <h4>Contato</h4> <hr />
          <p><a href="{{ Module::action('Contact', 'ContactsController@index') }}">Fale conosco</a></p>
          <p><a href="{{ Module::action('Contact', 'ContactsController@index') }}">SAC</a></p>
        </div>
        <div class="col-md-3">
          <h4>Site Seguro</h4> <hr />
          <table width="125" border="0" cellspacing="0" cellpadding="0" title="CLIQUE PARA VERIFICAR: Este site usa GlobalSign SSL Certificate para proteger suas informações pessoais."><tr><td><span id="ss_img_wrapper_gmogs_image_125-50_en_dblue"><a href="https://www.globalsign.com/" target=_blank title="GlobalSign Site Seal" rel="nofollow"><img alt="SSL" border=0 id="ss_img" src="//seal.globalsign.com/SiteSeal/images/gs_noscript_125-50_en.gif"></a></span><script type="text/javascript" src="//seal.globalsign.com/SiteSeal/gmogs_image_125-50_en_dblue.js"></script></td></tr></table>
        </div>
        <div class="col-md-3">
          <h4>Pagamentos</h4> <hr />
          <img class="img-responsive" src="{{ asset('images/content/payments.png') }}" alt="Pagamentos" />
        </div>
      </div>
      <div class="col-md-2">
        <h4>Redes Sociais</h4> <hr />
        <div class="row">
          <div class="col-xs-6">
            <a href="//facebook.com/zattryoficial" target="blank" class="text-info">
              <i class="fa fa-3x fa-facebook-square"></i>
            </a>
          </div>
          <div class="col-xs-6">
            <a href="//twitter.com/zattryoficial" target="blank" class="text-primary">
              <i class="fa fa-3x fa-twitter-square"></i>
            </a>
          </div>
        </div>
      </div>
    </div>
  </footer>

  <hr />
  <footer class="container">
    <div class="row">
      <div class="col-md-3">Todos os Direitos Reservados © 2015</div>
      <div class="col-md-3">
      <div class="row">
        <div class="col-xs-2">
          <i class="fa fa-4x fa-map-marker"></i>
        </div>
        <div class="col-xs-10">
          Av. Republica do Líbano, 251 Sala 2516 Torre A - Pina - Recife - PE | CEP: 51.110-160
        </div>
      </div>
      </div>
      <div class="col-md-3 text-center">
        <b>Email:</b> contato@zattry.com.br
      </div>
      <div class="col-md-3 text-right">
        <a href="//emmet.com.br" target="blank">
          <img src="//emmet.com.br/img/logo.png" style="height:21px;">
        </a>
      </div>
    </div>
  </footer>
@stop

@section('scripts')
  {{-- javascripts --}}
  <script src="{{ asset('/javascripts/default.js') }}"></script>
@stop
