<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ config('layout.app_name') }} Email Template</title>

    @include('layouts/shared/mails/_stylesheets')
  </head>
  <body>
    <div class="block" style="height:50px"></div>

    <div class="block">
      <!-- start of header -->
      <table width="100%" bgcolor="#f6f4f5" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="header">
        <tbody>
          <tr>
            <td>
              <table width="580" bgcolor="{{ config('templates.color.primary') }}" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" hlitebg="edit" shadow="edit">
                <tbody>
                  <tr>
                    <td>
                      <!-- logo -->
                      <table width="100%" cellpadding="0" cellspacing="0" border="0" align="left" class="devicewidth">
                        <tbody>
                          <tr>
                            <td valign="middle" width="270" style="padding: 10px 0 10px 20px;" class="logo">
                              <div class="imgpop">
                                <a href="#">
                                  <img src="{{ asset('/images/logo.png') }}" alt="logo" border="0" style="display:block; border:none; outline:none; text-decoration:none;" st-image="edit" class="logo" />
                                </a>
                              </div>
                            </td>
                            <td valign="middle" style="font-family: Helvetica, Arial, sans-serif;font-size: 14px; color: #ffffff;line-height: 24px; padding: 10px 0;" align="right" class="menu" st-content="menu">
                              <h3><br />
                                Zattry <br />
                                <small style="font-weight: normal;">Vida Com Estilo!</small>
                              </h3>

                            </td>
                            <td width="20">
                            </td>
                          </tr>
                        </tbody>
                      </table>
                      <!-- End of Menu -->
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
      <!-- end of header -->
    </div>
    <div class="block">
      <!-- Full + text -->
      <table width="100%" bgcolor="#f6f4f5" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="fullimage">
        <tbody>
          <tr>
            <td>
              <table bgcolor="#ffffff" width="580" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth" modulebg="edit">
                <tbody>
                  <tr>
                    <td width="100%" height="20"></td>
                  </tr>
                  <tr>
                    <td>
                      @yield('content')
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </body>
</html>
