@extends('layouts/master')

@section('head')
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui" />
  <meta name="description" content="bootstrap admin template" />
  <meta name="author" content="" />

  <title>Zattry</title>

  <link rel="apple-touch-icon" href="{{ asset('/templates/remark/images/apple-touch-icon.png') }}" />
  <link rel="shortcut icon" href="{{ asset('/templates/remark/images/favicon.ico') }}" />

  {{-- Stylesheets --}}
  <link rel="stylesheet" href="{{ asset('/templates/remark/css/bootstrap.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('/templates/remark/css/bootstrap-extend.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('/templates/remark/css/site.min.css') }}" />

  <link rel="stylesheet" href="{{ asset('/templates/remark/vendor/asscrollable/asScrollable.css') }}" />
  <link rel="stylesheet" href="{{ asset('/templates/remark/vendor/switchery/switchery.css') }}" />
  <link rel="stylesheet" href="{{ asset('/templates/remark/vendor/intro-js/introjs.css') }}" />
  <link rel="stylesheet" href="{{ asset('/templates/remark/vendor/slidepanel/slidePanel.css') }}" />
  <link rel="stylesheet" href="{{ asset('/templates/remark/vendor/flag-icon-css/flag-icon.css') }}" />

  {{-- Plugin --}}
  <link rel="stylesheet" href="{{ asset('/templates/remark/vendor/jvectormap/jquery-jvectormap.css') }}" />
  <link rel="stylesheet" href="{{ asset('/templates/remark/vendor/select2/select2.css') }}" />
  <link rel="stylesheet" href="{{ asset('/templates/remark/vendor/bootstrap-tokenfield/bootstrap-tokenfield.css') }}" />
  <link rel="stylesheet" href="{{ asset('/templates/remark/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" />
  <link rel="stylesheet" href="{{ asset('/templates/remark/vendor/bootstrap-select/bootstrap-select.css') }}" />
  <link rel="stylesheet" href="{{ asset('/templates/remark/vendor/icheck/icheck.css') }}" />
  <link rel="stylesheet" href="{{ asset('/templates/remark/vendor/switchery/switchery.css') }}" />
  <link rel="stylesheet" href="{{ asset('/templates/remark/vendor/asrange/asRange.css') }}" />
  <link rel="stylesheet" href="{{ asset('/templates/remark/vendor/asspinner/asSpinner.css') }}" />
  <link rel="stylesheet" href="{{ asset('/templates/remark/vendor/clockpicker/clockpicker.css') }}" />
  <link rel="stylesheet" href="{{ asset('/templates/remark/vendor/ascolorpicker/asColorPicker.css') }}" />
  <link rel="stylesheet" href="{{ asset('/templates/remark/vendor/bootstrap-touchspin/bootstrap-touchspin.css') }}" />
  <link rel="stylesheet" href="{{ asset('/templates/remark/vendor/card/card.css') }}" />
  <link rel="stylesheet" href="{{ asset('/templates/remark/vendor/jquery-labelauty/jquery-labelauty.css') }}" />
  <link rel="stylesheet" href="{{ asset('/templates/remark/vendor/bootstrap-datepicker/bootstrap-datepicker.css') }}" />
  <link rel="stylesheet" href="{{ asset('/templates/remark/vendor/bootstrap-maxlength/bootstrap-maxlength.css') }}" />
  <link rel="stylesheet" href="{{ asset('/templates/remark/vendor/jt-timepicker/jquery-timepicker.css') }}" />
  <link rel="stylesheet" href="{{ asset('/templates/remark/vendor/jquery-strength/jquery-strength.css') }}" />
  <link rel="stylesheet" href="{{ asset('/templates/remark/vendor/multi-select/multi-select.css') }}" />
  <link rel="stylesheet" href="{{ asset('/templates/remark/vendor/typeahead-js/typeahead.css') }}" />

  {{-- Page --}}
  <link rel="stylesheet" href="{{ asset('/templates/remark/fonts/weather-icons/weather-icons.css') }}" />
  <link rel="stylesheet" href="{{ asset('/templates/remark/css/dashboard/v1.css') }}" />

  {{-- Fonts --}}
  <link rel="stylesheet" href="{{ asset('/templates/remark/fonts/web-icons/web-icons.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('/templates/remark/fonts/brand-icons/brand-icons.min.css') }}" />
  <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>

  <link rel="stylesheet" href="{{ asset('/stylesheets/admin.css') }}" />

  <!--[if lt IE 9]>
    <script src="{{ asset('/templates/remark/vendor/html5shiv/html5shiv.min.js') }}"></script>
  <![endif]-->

  <!--[if lt IE 10]>
    <script src="{{ asset('/templates/remark/vendor/media-match/media.match.min.js') }}"></script>
    <script src="{{ asset('/templates/remark/vendor/respond/respond.min.js') }}"></script>
  <![endif]-->

  {{-- Scripts --}}
  <script src="{{ asset('/templates/remark/vendor/modernizr/modernizr.js') }}"></script>
  <script src="{{ asset('/templates/remark/vendor/breakpoints/breakpoints.js') }}"></script>
  <script>Breakpoints();</script>
@stop

@section('class-body')
  dashboard
@stop

@section('nav')
  <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
  <![endif]-->

  @include('layouts/shared/admin/_navigation')

  @include('layouts/shared/admin/_sidebar')
@stop

@section('container')
  <div id="ajax-loading">
    <div class="wrap"></div>
    <div class="content">
      <i class="fa fa-refresh fa-4x fa-spin fa-fw"></i>
      <br /> Carrengando...
    </div>
  </div>
  <div class="page animsition">
    @include('layouts/shared/admin/_message_errors')

    @yield('content')
  </div>
@stop

@section('footer')
  <footer class="site-footer">
    <span class="site-footer-legal">© 2015 Zattry</span>
    <div class="site-footer-right">
      <img src="//emmet.com.br/img/logo.png" style="height:21px;" />
    </div>
  </footer>
@stop

@section('scripts')
  {{-- Core --}}
  <script src="{{ asset('templates/remark/vendor/jquery/jquery.js') }}"></script>
  <script src="{{ asset('templates/remark/vendor/bootstrap/bootstrap.js') }}"></script>
  <script src="{{ asset('templates/remark/vendor/animsition/jquery.animsition.js') }}"></script>
  <script src="{{ asset('templates/remark/vendor/asscroll/jquery-asScroll.js') }}"></script>
  <script src="{{ asset('templates/remark/vendor/mousewheel/jquery.mousewheel.js') }}"></script>
  <script src="{{ asset('templates/remark/vendor/asscrollable/jquery.asScrollable.all.js') }}"></script>
  <script src="{{ asset('templates/remark/vendor/ashoverscroll/jquery-asHoverScroll.js') }}"></script>

  {{-- Plugins --}}
  <script src="{{ asset('templates/remark/vendor/switchery/switchery.min.js') }}"></script>
  <script src="{{ asset('templates/remark/vendor/intro-js/intro.js') }}"></script>
  <script src="{{ asset('templates/remark/vendor/screenfull/screenfull.js') }}"></script>
  <script src="{{ asset('templates/remark/vendor/slidepanel/jquery-slidePanel.js') }}"></script>
  <script src="{{ asset('templates/remark/vendor/skycons/skycons.js') }}"></script>
  <script src="{{ asset('templates/remark/vendor/aspieprogress/jquery-asPieProgress.min.js') }}"></script>
  <script src="{{ asset('templates/remark/vendor/jvectormap/jquery-jvectormap.min.js') }}"></script>
  <script src="{{ asset('templates/remark/vendor/jvectormap/maps/jquery-jvectormap-ca-lcc-en.js') }}"></script>
  <script src="{{ asset('templates/remark/vendor/matchheight/jquery.matchHeight-min.js') }}"></script>

  <script src="{{ asset('templates/remark/vendor/select2/select2.min.js') }}"></script>

  {{-- Scripts --}}
  <script src="{{ asset('templates/remark/js/core.js') }}"></script>
  <script src="{{ asset('templates/remark/js/site.js') }}"></script>

  <script src="{{ asset('templates/remark/js/sections/menu.js') }}"></script>
  <script src="{{ asset('templates/remark/js/sections/menubar.js') }}"></script>
  <script src="{{ asset('templates/remark/js/sections/sidebar.js') }}"></script>

  <script src="{{ asset('templates/remark/js/configs/config-colors.js') }}"></script>
  <script src="{{ asset('templates/remark/js/configs/config-tour.js') }}"></script>

  <script src="{{ asset('templates/remark/js/components/asscrollable.js') }}"></script>
  <script src="{{ asset('templates/remark/js/components/animsition.js') }}"></script>
  <script src="{{ asset('templates/remark/js/components/slidepanel.js') }}"></script>
  <script src="{{ asset('templates/remark/js/components/switchery.js') }}"></script>
  <script src="{{ asset('templates/remark/js/components/matchheight.js') }}"></script>
  <script src="{{ asset('templates/remark/js/components/jvectormap.js') }}"></script>
  <script src="{{ asset('templates/remark/js/components/select2.js') }}"></script>
  <script src="{{ asset('javascripts/admin.js') }}"></script>

  {{-- Init --}}
  <script src="{{ asset('templates/remark/js/init.js') }}"></script>
@stop
