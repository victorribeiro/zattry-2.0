<div class="container">

@if (Session::has('status'))
  <div class="alert alert-success">
    {{ Session::get('status') }}
  </div>
@endif

@if(Session::has('success'))
  <div class="alert alert-success">
    <strong>Sucesso! </strong>  {!! Session::get('success') !!}
  </div>
@endif

@if(Session::has('danger'))
  <div class="alert alert-danger">
    <strong>Erro! </strong>  {!! Session::get('danger') !!}
  </div>
@endif

@if(Session::has('error'))
  <div class="alert alert-danger">
    <strong>Erro! </strong>  {!! Session::get('error') !!}
  </div>
@endif

@if(Session::has('info'))
  <div class="alert alert-info">
    <strong>Info! </strong>  {!! Session::get('info') !!}
  </div>
@endif

@if(Session::has('warning'))
  <div class="alert alert-warning">
    <strong>Atenção! </strong>  {!! Session::get('warning') !!}
  </div>
@endif

@if ($errors->any())
  <div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert">
      <span class="fa fa-times"></span>
    </button>
    <p><strong>Oops!</strong> Ocorreu erro(s)</p>
    <ul>
      @if($errors->any())
        @foreach($errors->getMessages('default') as $key => $messages)
          @foreach ($messages as $message)
            <li>{{ ucfirst($message) }}</li>
          @endforeach
        @endforeach
      @endif
    </ul>
  </div>
@endif

</div>
