@extends(config('layout.auth'))

@section('content')
    <div class="row">
        <div class="col-md-offset-2 col-md-8">
            <h2 class="form-signin-heading">Esqueci a senha</h2>

            {!! Form::open([ 'url' => '/password/email' ]) !!}
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            {!! Form::label('email', 'Email') !!}
                            <div class="input-group">
                                {!! Form::text('email', null, [ 'class' => 'form-control input-lg', 'placeholder' => 'Email', 'required' => '' ]) !!}
                                <span class="input-group-btn">
                                    <button class="btn btn-lg btn-primary" type="submit">Resetar senha</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

            {!! Form::close() !!}
        </div>
    </div>
@endsection
