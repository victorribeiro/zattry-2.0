@extends(config('layout.auth'))

@section('content')
    <div class="row">
        <div class="col-md-offset-2 col-md-8">
            <h2 class="form-signin-heading">Esqueci a senha</h2>

            {!! Form::open([ 'url' => '/password/email' ]) !!}
                <input type="hidden" name="token" value="{{ $token }}">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            {!! Form::label('email', 'email') !!}
                            {!! Form::text('email', null, [ 'class' => 'form-control input-lg', 'placeholder' => 'Email', 'required' => '', 'autofocus' => '' ]) !!}
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-5">
                            {!! Form::label('password', 'Senha') !!}
                            {!! Form::password('password', [ 'class' => 'form-control input-lg', 'placeholder' => 'Email ou login', 'required' => '', 'autofocus' => '' ]) !!}
                        </div>

                        <div class="col-md-7">
                            {!! Form::label('password_confirmation', 'Confirmação de senha') !!}
                            <div class="input-group">
                                {!! Form::password('password_confirmation', [ 'class' => 'form-control input-lg', 'placeholder' => 'Email', 'required' => '' ]) !!}
                                <span class="input-group-btn">
                                    <button class="btn btn-lg btn-primary" type="submit">Alterar senha</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
