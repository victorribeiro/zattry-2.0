<div class="container">
  <div id="carousel-home-page" class="carousel slide">
    {{-- Wrapper for slides --}}
    <div class="carousel-inner">
      <div class="item active">
        <img class="img-responsive" src="{{ asset('images/carousel/home/1.jpg') }}" />
      </div>

      <div class="item">
        <img class="img-responsive" src="{{ asset('images/carousel/home/2.jpg') }}" />
      </div>

      <div class="item">
        <img class="img-responsive" src="{{ asset('images/carousel/home/3.jpg') }}" />
      </div>

      <div class="item">
        <img class="img-responsive" src="{{ asset('images/carousel/home/4.jpg') }}" />
      </div>

      <div class="item">
        <img class="img-responsive" src="{{ asset('images/carousel/home/5.jpg') }}" />
      </div>

      <div class="item">
        <img class="img-responsive" src="{{ asset('images/carousel/home/6.jpg') }}" />
      </div>

      <div class="item">
        <img class="img-responsive" src="{{ asset('images/carousel/home/7.jpg') }}" />
      </div>
    </div>

    {{-- Indicators --}}
    <ol class="carousel-indicators">
      <li data-target="#carousel-home-page" data-slide-to="0" class="active"></li>
      <li data-target="#carousel-home-page" data-slide-to="1"></li>
      <li data-target="#carousel-home-page" data-slide-to="2"></li>
      <li data-target="#carousel-home-page" data-slide-to="3"></li>
      <li data-target="#carousel-home-page" data-slide-to="4"></li>
      <li data-target="#carousel-home-page" data-slide-to="5"></li>
      <li data-target="#carousel-home-page" data-slide-to="6"></li>
    </ol>

    {{-- Left and right controls --}}
    <a class="left carousel-control" href="#carousel-home-page" data-slide="prev">
      <i class="fa fa-chevron-left"></i>
    </a>
    <a class="right carousel-control" href="#carousel-home-page" data-slide="next">
      <i class="fa fa-chevron-right"></i>
    </a>
  </div>
</div>
