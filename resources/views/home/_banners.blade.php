<div class="container">
  <div class="row">
    <div class="col-md-4 text-center">
      <img src="{{ asset('images/banners/home/colecao-feminina.png') }}" alt="Coleção feminina" />
    </div>
    <div class="col-md-4 text-center">
      <img src="{{ asset('images/banners/home/colecao-masculina.png') }}" alt="Coleção masculina" />
    </div>
    <div class="col-md-4 text-center">
      <img src="{{ asset('images/banners/home/seja-um-distribuidor-autorizado.png') }}" alt="Seja um distribuidor autorizado" />
    </div>
  </div>
</div>
