@if (!$products->isEmpty())
<div class="container">
  <h3 class="text-center">Nova coleção</h3>
  <div class="row">
    @foreach ($products as $key => $product)
      <div class="col-xs-6 col-sm-3">
        @include('store::products/_product', [ 'product' => $product ])
      </div>
      @if (($key+1) % 4 == 0)
        {!! '</div><div class="row">' !!}
      @endif
    @endforeach
  </div>

  <div class="row">
    <div class="col-md-12 text-center">
      <a class="btn btn-default" href="{{ Module::action('Store', 'ProductsController@index') }}">
        ver todos os produtos
      </a>
    </div>
  </div>
</div>
@endif
