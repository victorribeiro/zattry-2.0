@extends(config('layout.default'))

@section('content')
  @include('home/_carousel')
  <hr />
  @include('home/_banners')
  <hr />
  @include('home/_products')
@endsection
