<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', [ 'uses' => 'HomeController@index' ]);
Route::get('/home', [ 'uses' => 'HomeController@index' ]);

Route::group([ 'prefix' => 'admin', 'middleware' => 'auth' ], function() {
    Route::get('/', 'Admin\HomeController@index');
});

Route::get('/about', 'PagesController@about');
