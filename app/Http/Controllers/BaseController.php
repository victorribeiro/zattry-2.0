<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Store\Repositories\CartRepository;
use App\Modules\Store\Models\Stock;

class BaseController extends Controller
{
    public function __construct()
    {
        $cart_repository = new CartRepository(new Stock);
        view()->share('cart_stocks', $cart_repository->all());
        view()->share('logged', \Auth::user());
    }
}
