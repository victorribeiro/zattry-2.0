<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\BaseController;

use App\Modules\Store\Repositories\ProductRepository;

class HomeController extends BaseController
{
    public function __construct(ProductRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    public function index()
    {
        $products = $this->repository->latestHighlighted();

        return view('home/index')->with('products', $products);
    }
}
