<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;

class PagesController extends BaseController
{
    public function about()
    {
        return view('pages/about');
    }
}
