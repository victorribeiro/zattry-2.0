<?php
namespace App\Modules\Contact\Repositories;

use App\Repositories\BaseRepository;
use App\Modules\Contact\Models\Contact;

class ContactRepository extends BaseRepository
{
    public $name = 'Contato';
    public $gender = 'o';

    public function __construct(Contact $eloquent)
    {
        $this->eloquent = $eloquent;
    }

    public function afterStore($attributes)
    {
        $contact = $this->eloquent;
        \Mail::send('contact::emails/contact', [ 'contact' => $contact ], function ($message) use ($contact) {
            $message->to('wfsneto@gmail.com', 'Zattry')->from($contact->email, $contact->name)
                ->subject('Contato do formulário :: Zattry');
        });
    }
}
