<?php
namespace App\Modules\Contact\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class ContactServiceProvider extends ServiceProvider
{
	/**
	 * Register the Contact module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\Contact\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the Contact module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('contact', realpath(__DIR__.'/../Resources/Lang'));

		View::addNamespace('contact', realpath(__DIR__.'/../Resources/Views'));
	}
}
