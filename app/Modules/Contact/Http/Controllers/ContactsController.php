<?php

namespace App\Modules\Contact\Http\Controllers;

use App\Http\Controllers\BaseController;
use App\Modules\Contact\Http\Requests\ContactSendRequest;
use App\Modules\Contact\Repositories\ContactRepository;

class ContactsController extends BaseController
{
    public function __construct(ContactRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    public function index()
    {
        $contact = $this->repository->init();
        return view('contact::contacts/index', [ 'contact' => $contact ]);
    }

    public function store(ContactSendRequest $request)
    {
        $stored = $this->repository->store($request->get('contact'));

        if ($stored->success()) {
            return \Module::redirectAction('Contact', 'ContactsController@index')
                ->with('success', $stored->message);
        }
        else {
            return \Module::action('Contact', 'ContactsController@create')
                ->withInput()->withErrors($stored->errors);
        }
    }
}
