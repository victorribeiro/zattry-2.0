@extends(config('layout.default'))

@section('content')
<div class="container">
  @include('shared/_top_pages')
  <h2>Formulário de contato</h2> <hr />

  <div class="row">
    <div class="col-md-4">
      <div class="row">
        <div class="col-md-12 col-sm-6">
          <h3>Endereço</h3>
          <p>
            Av. Republica do Líbano, 251 Sala 2516 Torre A <br />
            Pina - Recife - PE | CEP: 51.110-160
          </p>
        </div>
        <div class="col-md-12 col-sm-6">
          <h3>Contato de email</h3>
          <p>
            Administração: <b>administrator@zattry.com.br</b> <br />
            Contato: <b>contato@zattry.com.br</b> <br />
          </p>
        </div>
      </div>
    </div>
    <div class="col-md-8">
      {!! Form::model($contact, [ 'url' => Module::action('Contact', 'ContactsController@index'), 'method' => 'POST' ]) !!}
        <div class="form-group row">
          <div class="col-sm-12">
            {!! Form::label('contact[name]', 'Nome: *') !!}
            {!! Form::text('contact[name]', null, [ 'class' => 'form-control' ]) !!}
          </div>
        </div>

        <div class="form-group row">
          <div class="col-sm-12">
            {!! Form::label('contact[email]', 'Email: *') !!}
            {!! Form::text('contact[email]', null, [ 'class' => 'form-control' ]) !!}
          </div>
        </div>

        <div class="form-group row">
          <div class="col-sm-12">
            {!! Form::label('contact[phone]', 'Telefone:') !!}
            {!! Form::text('contact[phone]', null, [ 'class' => 'form-control' ]) !!}
          </div>
        </div>

        <div class="form-group row">
          <div class="col-sm-12">
            {!! Form::label('contact[message]', 'Mensagem: *') !!}
            {!! Form::textarea('contact[message]', null, [ 'class' => 'form-control', 'rows' => 3 ]) !!}
          </div>
        </div>
        <div class="form-group">
          <button class="btn btn-primary btn-block">
            <i class="fa fa-fw fa-send"></i> Enviar mensagem
          </button>
        </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
@endsection
