@extends(config('layout.mail'))

@section('content')
<table width="540" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidthinner">
  <tbody>
    <tr>
      <td colspan="2" width="100%" height="10"></td>
    </tr>
    <tr>
      <td class="title">
        Nome
      </td>
      <td class="paragraph">
        {{ $contact->name }}
      </td>
    </tr>
    <tr>
      <td colspan="2" width="100%" height="10"></td>
    </tr>
    <tr>
      <td class="title">
        Email
      </td>
      <td class="paragraph">
        {{ $contact->email }}
      </td>
    </tr>
    <tr>
      <td colspan="2" width="100%" height="10"></td>
    </tr>
    <tr>
      <td class="title">
        Telefone
      </td>
      <td class="paragraph">
        {{ $contact->phone }}
      </td>
    </tr>
    <tr>
      <td colspan="2" width="100%" height="10"></td>
    </tr>
    <tr>
      <td class="title">
        Mensagem
      </td>
      <td class="paragraph">
        {{ $contact->message }}
      </td>
    </tr>
    <tr>
      <td colspan="2" width="100%" height="10"></td>
    </tr>
    <tr>
      <td colspan="2" class="paragraph">
        <small>
          @if ($contact->sent_from == 'store')
            Mensagem enviada diretamente do contato da Loja
          @else
            Mensagem enviada diretamente do contato do Backoffice
          @endif
        </small>
      </td>
    </tr>
    <tr>
      <td colspan="2" width="100%" height="10"></td>
    </tr>
  </tbody>
</table>
@endsection
