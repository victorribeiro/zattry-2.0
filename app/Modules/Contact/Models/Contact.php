<?php

namespace App\Modules\Contact\Models;

use App\Models\BaseModel;

class Contact extends BaseModel
{
    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $fillable = [ 'name', 'email', 'phone', 'message' ];
}
