<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImportDataFromContatosToContacts extends Migration
{
    public function up()
    {
        $contatos = \DB::table('contatos')
            ->where('email', 'not like', '%email.tst%')
            ->where('email', 'like', '%@%')
        ->get();

        $values = [];
        foreach ($contatos as $key => $contato) {
            if (filter_var($contato->email, FILTER_VALIDATE_EMAIL)) {
                $sent_from = $contato->local == 'site' ? 'store' : 'backoffice';
                $user_id = $contato->usuario_id == 0 ? 'NULL' : $contato->usuario_id;

                $nome = str_replace("'", "\'", $contato->nome);
                $email = str_replace("'", "\'", $contato->email);
                $fone = str_replace("'", "\'", $contato->fone);
                $mensagem = str_replace("'", "\'", $contato->mensagem);

                $values[] = "({$contato->id}, {$user_id}, '{$sent_from}',
                    '" . $nome . "',
                    '" . $email . "',
                    '" . $fone . "',
                    '" . $mensagem . "',
                    NOW(), NOW(), NULL)";
            }
        }

        if (is_array($values)) {
            $sql = "INSERT INTO `contacts` (`id`, `user_id`, `sent_from`, `name`, `email`, `phone`, `message`, `created_at`, `updated_at`, `deleted_at`) VALUES " . implode(", ", $values) . ";";
            \DB::statement( $sql );
        }
    }

    public function down()
    {
        \DB::table('contacts')->truncate();
    }
}
