<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameBackupContatosTable extends Migration
{
    public function up()
    {
        Schema::rename('contatos', 'z_contatos');
    }

    public function down()
    {
        Schema::rename('z_contatos', 'contatos');
    }
}
