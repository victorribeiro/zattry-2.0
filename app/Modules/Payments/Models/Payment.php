<?php

namespace App\Modules\Payments\Models;

use App\Models\BaseModel;

class Payment extends BaseModel
{
    protected $fillable = [ 'order_id' ];

    public function order()
    {
        return $this->belongsToMany('App\Modules\Store\Models\Order');
    }
}
