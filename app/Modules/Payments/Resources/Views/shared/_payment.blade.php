<div class="panel panel-default">
  <div class="panel-heading">
    <div class="panel-title">
      <div class="row">
        <div class="col-xs-6">
          <h4 style="margin:0">Forma de pagamento</h4>
        </div>
      </div>
    </div>
  </div>

  <div class="panel-body">
    <div class="form-group">
      <div class="row">
        <div class="col-md-12"> <br />
          <div class="btn-group" data-toggle="buttons">
            {{-- <label class="btn btn-default{{ old('card.form') == 'paypal' ? ' active' : '' }}">
              {!! Form::radio('card[form]', 'paypal') !!}
              <div data-toggle="tooltip" data-original-title="PayPal">
                <img style="height:30px;" src="{{ asset('images/content/payments/paypal.png') }}" alt="PayPal" />
              </div>
            </label>

            <label class="btn btn-default{{ old('card.form') == 'boleto' ? ' active' : '' }}">
              {!! Form::radio('card[form]', 'boleto') !!}
              <div data-toggle="tooltip" data-original-title="Boleto">
                <img style="height:30px;" src="{{ asset('images/content/payments/boleto.png') }}" alt="Boleto" />
              </div>
            </label> --}}

            <label class="btn btn-default{{ old('card.form') == 'visa' ? ' active' : '' }}">
              {!! Form::radio('card[form]', 'visa') !!}
              <div data-toggle="tooltip" data-original-title="Visa">
                <img style="height:30px;" src="{{ asset('images/content/payments/visa.png') }}" alt="Visa" />
              </div>
            </label>

            <label class="btn btn-default{{ old('card.form') == 'mastercard' ? ' active' : '' }}">
              {!! Form::radio('card[form]', 'mastercard') !!}
              <div data-toggle="tooltip" data-original-title="Mastercard">
                <img style="height:30px;" src="{{ asset('images/content/payments/mastercard.png') }}" alt="Mastercard" />
              </div>
            </label>

            <label class="btn btn-default{{ old('card.form') == 'diners' ? ' active' : '' }}">
              {!! Form::radio('card[form]', 'diners') !!}
              <div data-toggle="tooltip" data-original-title="Diners">
                <img style="height:30px;" src="{{ asset('images/content/payments/diners.png') }}" alt="Diners" />
              </div>
            </label>

            <label class="btn btn-default{{ old('card.form') == 'elo' ? ' active' : '' }}">
              {!! Form::radio('card[form]', 'elo') !!}
              <div data-toggle="tooltip" data-original-title="ELO">
                <img style="height:30px;" src="{{ asset('images/content/payments/elo.png') }}" alt="ELO" />
              </div>
            </label>

            {{-- <label class="btn btn-default{{ old('card.form') == 'hipercard' ? ' active' : '' }}">
              {!! Form::radio('card[form]', 'hipercard') !!}
              <div data-toggle="tooltip" data-original-title="Hipercard">
                <img style="height:30px;" src="{{ asset('images/content/payments/hipercard.png') }}" alt="Hipercard" />
              </div>
            </label> --}}
          </div>
        </div>
      </div>
    </div>

    <div id="activation-form-card">
      <div class="row">
        <div class="col-md-12">
          <div class="form-group row">
            <div class="col-sm-7">
              {!! Form::label('card[name]', 'Nome') !!}
              {!! Form::text('card[name]', null, [ 'class' => 'form-control', 'placeholder' => 'Nome do títular do cartão' ]) !!}
            </div>

            <div class="col-sm-5">
              {!! Form::label('card[cpf]', 'CPF') !!}
              {!! Form::text('card[cpf]', null, [ 'class' => 'form-control', 'placeholder' => 'CPF do títular do cartão', 'data-mask' => 'cpf' ]) !!}
            </div>
          </div>

          <div class="form-group row">
            <div class="col-sm-7">
              {!! Form::label('card[card]', 'Número do cartão') !!}
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="fa fa-credit-card"></i>
                </span>
                {!! Form::text('card[card]', null, [ 'class' => 'form-control', 'placeholder' => 'Número do cartão', 'data-mask' => 'cc' ]) !!}
              </div>
            </div>

            <div class="col-sm-5">
              {!! Form::label('card[code]', 'Cód. Segurança') !!}
              <div class="input-group">
                <span class="input-group-addon">
                  <img style="height:17px;" src="{{ asset('images/content/payments/card-security-code.png') }}" alt="Cód. Segurança" />
                </span>
                {!! Form::text('card[code]', null, [ 'class' => 'form-control', 'placeholder' => 'Cód. Segurança', 'data-mask' => '999' ]) !!}
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-sm-3">
              {!! Form::label('card[year]', 'Ano') !!}
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </span>
                {!! Form::text('card[year]', null, [ 'class' => 'form-control', 'placeholder' => 'YYYY', 'data-mask' => '9999' ]) !!}
              </div>
            </div>

            <div class="col-sm-3">
              {!! Form::label('card[month]', 'Mês') !!}
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </span>
                {!! Form::text('card[month]', null, [ 'class' => 'form-control', 'placeholder' => 'MM', 'data-mask' => '99' ]) !!}
              </div>
            </div>

            <div class="col-sm-6">
              {!! Form::label('card[installments]', 'Parcelas') !!}
              {!! Form::select('card[installments]', isset($installments) ? $installments : [], null, [ 'id' => 'select-installments', 'class' => 'form-control', 'data-quantity' => old('card.installments', 1) ]) !!}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
