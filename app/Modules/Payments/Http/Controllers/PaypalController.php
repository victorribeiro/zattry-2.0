<?php

namespace App\Modules\Payments\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;

use App\Modules\Payments\Repositories\PaypalRepository;

class PaypalController extends BaseController
{
    public function __construct(PaypalRepository $repository)
    {
        $this->repository = $repository;
    }

    public function callback(Request $request)
    {
        $status = $request->get('payment_status');

        $order = $this->repository->orderByCode( $request->get('invoice') );

        if (is_null($order)) {
            return redirect(action('HomeController@index'));
        }

        $callback = $this->repository->callback( $order, $request->all() );

        return \Module::redirectAction('Store', 'OrdersController@show', $order->code);
    }
}
