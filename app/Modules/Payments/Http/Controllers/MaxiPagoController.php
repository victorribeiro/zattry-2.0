<?php

namespace App\Modules\Payments\Http\Controllers;

use App\Http\Controllers\BaseController;

use App\Modules\Payments\Services\MaxiPagoService;

class MaxiPagoController extends BaseController
{
    public function installments($total)
    {
        return MaxiPagoService::installments($total);
    }
}
