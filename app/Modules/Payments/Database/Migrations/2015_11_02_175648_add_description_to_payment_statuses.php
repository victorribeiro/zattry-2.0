<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDescriptionToPaymentStatuses extends Migration
{
    public function up()
    {
        Schema::table('payment_statuses', function($table)
        {
            $table->text('description')->after('code');
        });
    }

    public function down()
    {
        Schema::table('payment_statuses', function($table)
        {
            $table->dropColumn([ 'description' ]);
        });
    }
}
