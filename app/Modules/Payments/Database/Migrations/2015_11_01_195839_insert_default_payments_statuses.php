<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertDefaultPaymentsStatuses extends Migration
{
    public function up()
    {
        DB::table('statuses')->insert([
            [
                'type' => 'payment',
                'code' => 'started',
                'name' => 'Iniciado',
                'description' => 'Iniciado',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ], [
                'type' => 'payment',
                'code' => 'pending',
                'name' => 'Pendente',
                'description' => 'Pendente',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ], [
                'type' => 'payment',
                'code' => 'approved',
                'name' => 'Aprovado',
                'description' => 'Aprovado',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ], [
                'type' => 'payment',
                'code' => 'declined',
                'name' => 'Rejeitado',
                'description' => 'Rejeitado',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ], [
                'type' => 'payment',
                'code' => 'cancelled',
                'name' => 'Cancelado',
                'description' => 'Cancelado',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]
        ]);
    }

    public function down()
    {
        DB::table('statuses')->where('type', 'payment')->delete();
    }
}
