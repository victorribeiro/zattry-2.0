<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaymentStatusIdGatewayOrderIdAuthorizationCodeFormCardToPayments extends Migration
{
    public function up()
    {
        Schema::table('payments', function($table)
        {
            $table->integer('payment_status_id')->after('order_id')->unsigned()->index();
            $table->foreign('payment_status_id')->references('id')->on('payment_statuses');
            $table->string('gateway_order_id')->after('gateway');
            $table->string('authorization_code')->after('gateway_order_id');
            $table->string('form')->after('authorization_code');
            $table->string('card')->after('form');
            $table->decimal('parcel', 17, 2)->after('total');
        });
    }

    public function down()
    {
        Schema::table('payments', function($table)
        {
            $table->dropForeign('payments_payment_status_id_foreign');
            $table->dropColumn([ 'payment_status_id', 'gateway_order_id',
                'authorization_code', 'form', 'card', 'parcel' ]);
        });
    }
}
