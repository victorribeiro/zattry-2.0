<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertDefaultStatusesForMaxipagoToPaymentsStatuses extends Migration
{
    public function up()
    {
        DB::table('payment_statuses')->insert([
            // started = Iniciado
            [
                'status_id' => 1,
                'gateway' => 'maxipago',
                'code' => 'started',
                'description' => 'Iniciado',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            // 0 = Aprovado
            [
                'status_id' => 3,
                'gateway' => 'maxipago',
                'code' => 0,
                'description' => 'Aprovado',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            // 1 = Recusado
            [
                'status_id' => 4,
                'gateway' => 'maxipago',
                'code' => 1,
                'description' => 'Recusado',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            // 2 = Recusado devido à Fraude ou Duplicity
            [
                'status_id' => 4,
                'gateway' => 'maxipago',
                'code' => 2,
                'description' => 'Recusado devido à Fraude ou Duplicity',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            // 5 = Fraude comentário
            [
                'status_id' => 2,
                'gateway' => 'maxipago',
                'code' => 5,
                'description' => 'Fraude comentário',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            // 2048 = Erro interno no maxiPago!
            [
                'status_id' => 4,
                'gateway' => 'maxipago',
                'code' => 2048,
                'description' => 'Erro interno no maxiPago!',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            // 4097 = Adiquirente expirou
            [
                'status_id' => 4,
                'gateway' => 'maxipago',
                'code' => 4097,
                'description' => 'Adiquirente expirou',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
        ]);
    }

    public function down()
    {
        DB::table('payment_statuses')->where('gateway', 'maxipago')->delete();
    }
}
