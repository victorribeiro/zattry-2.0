<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveStatusColumnOnPayments extends Migration
{
    public function up()
    {
        Schema::table('payments', function($table)
        {
            $table->dropColumn('status');
        });
    }

    public function down()
    {
        Schema::table('payments', function($table)
        {
            $table->string('status');
        });
    }
}
