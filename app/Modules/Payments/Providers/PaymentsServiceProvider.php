<?php
namespace App\Modules\Payments\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class PaymentsServiceProvider extends ServiceProvider
{
	/**
	 * Register the Payments module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\Payments\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the Payments module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('payments', realpath(__DIR__.'/../Resources/Lang'));

		View::addNamespace('payments', realpath(__DIR__.'/../Resources/Views'));
	}
}
