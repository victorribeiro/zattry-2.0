<?php

namespace App\Modules\Payments\Services;

require_once base_path('vendor/zardo/maxipago/lib/maxipago/Autoload.php');
require_once base_path('vendor/zardo/maxipago/lib/maxiPago.php');

use maxiPago as MaxiPagoLib;

class MaxiPagoService extends MaxiPagoLib
{
    const INTEREST = 6.8;
    const INTEREST_PER_MONTH = 2.33;

    public function __construct($attributes = [])
    {
        $this->setCredentials(env('MAXIPAGO_ID'), env('MAXIPAGO_KEY'));
        $this->setDebug(env('MAXIPAGO_DEBUG'));
        $this->setEnvironment(env('MAXIPAGO_ENV'));
    }

    public function pay($attributes)
    {
        return $this->creditCardSale($attributes);
    }

    public function success()
    {
        return !$this->isErrorResponse() &&
            $this->isTransactionResponse() &&
            $this->getResponseCode() == 0;
    }

    public function message()
    {
        $message = env('APP_DEBUG') ? ('<br />' . $this->getResponseCode() . ': <small>(' . $this->getMessage() . ')</small>') : null;
        // 0 = Aprovado
        // 1 = Recusado
        // 2 = Recusado devido à Fraude ou Duplicity
        // 5 = Fraude comentário
        // 1022 = Erro no cartão de crédito
        // 1024 = Erro nos parâmetros enviados
        // 1025 = Erro de credenciais de comerciante
        // 2048 = Erro interno no maxiPago!
        // 4097 = Acquirer expirou
        switch ($this->getResponseCode()) {
            case 0:
                return 'Sua compra foi efetuada com sucesso' . $message;
            case 1:
            case 2:
                return 'Sua compra foi recusada pela operadora' . $message;
            case 1025:
                return 'Ocorreu um erro com as credenciais de comerciante' . $message;
            case 5:
                return 'Sua compra está em análise pela operadora' . $message;
            case 1022:
                return 'Ocorreu um erro no cartão de crédito, favor tente novamente mais tarde' . $message;
            case 1024:
                if ($this->getMessage() == 'Credit Card  Number is not a valid credit card number.') {
                    return 'Cartão de crédito não é válido, por favor verfique e tente novamente' . $message;
                }
                else {
                    return 'Ocorreu um erro interno, favor entrar em contato com a nosso suporte' . $message;
                }
            case 2048:
                return 'A operadora está fora do ar, por favor tente novamente mais tarde' . $message;
            case 4097:
                return 'Banco adquirente expirou' . $message;
        }
    }

    public static function installments($total)
    {
        for ($i = 1; $i <= 10; $i++) {
            $_total = self::calculateInstallment($total, $i);

            $installments[ $i ] = (object) [
                'quantity' => $i,
                'amount' => $_total,
                'total' => $i * $_total,
            ];
        }

        return $installments;
    }

    public static function calculateInstallment($amount, $quantity)
    {
        $quantity = round($quantity);

        if ($quantity > 0 && $quantity == 1) {
            return $amount;
        }
        else {
            $interest = self::INTEREST + self::INTEREST_PER_MONTH * ($quantity - 1);
            return (float) \Number::format($amount / $quantity * $interest / 100 + $amount / $quantity, 2, '.', '');
        }
    }

    public static function total($amount, $installments)
    {
        return MaxiPagoService::calculateInstallment( $amount, $installments) * $installments;
    }
}
