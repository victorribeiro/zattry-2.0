<?php

namespace App\Modules\Payments\Services;

class PayPalService extends MaxiPagoLib
{
    public function pay($attributes)
    {
        $values = [
            'upload' => 1,
            'cmd' => '_xclick',
            'charset' => 'UTF-8',
            'currency_code' => 'BRL',
            'business' => env('PAYPAL_BUSINESS_MAIL'),
            'return' => \Module::action('Payments', 'PaypalController@callback'),
            'notify_url' => \Module::action('Payments', 'PaypalController@callback'),
            'invoice' => $stored->eloquent->code,
            'amount' => $stored->eloquent->total(),
            'item_name' => 'Zattry #' . $stored->eloquent->code,
            'item_number' => $stored->eloquent->id,
            'quantity' => 1,
        ];

        return redirect(env('PAYPAL_HOST') . '?' . http_build_query($values));
    }
}
