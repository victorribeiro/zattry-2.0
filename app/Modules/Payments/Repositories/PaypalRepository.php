<?php
namespace App\Modules\Payments\Repositories;

use App\Repositories\BaseRepository;
use App\Modules\Store\Models\Order;
use App\Modules\Payments\Models\Payment;
use App\Modules\Payments\Models\PaypalLog;
use App\Modules\Backoffice\Models\Point;
use App\Modules\Backoffice\Models\DistributorMove;

class PaypalRepository extends BaseRepository
{
    public function __construct(Order $eloquent)
    {
        $this->eloquent = $eloquent;
    }

    public function orderByCode($code)
    {
        return $this->eloquent->where('orders.code', $code)->first();
    }

    public function callback($order, $attributes)
    {
        \DB::beginTransaction();

        $payment = $this->payment( $order, $attributes );

        $paymentLog = $this->paymentLog( $order, $attributes );

        $activationMonthly = $this->activationMonthly( $order, $attributes );

        ($payment && $activationMonthly) ? \DB::commit() : \DB::rollback();

        return $payment && $activationMonthly;
    }

    public function payment($order, $attributes)
    {
        $payment = Payment::firstOrNew(['order_id' => $order->id]);
        $payment->gateway = 'paypal';
        $payment->status = $attributes['payment_status'];
        $payment->total = $attributes['mc_gross'];
        $payment->save();

        return $payment;
    }

    public function paymentLog($order, $attributes)
    {
        $log = new PaypalLog(['order_id' => $order->id]);
        $log->parameters = json_encode($attributes);
        $log->save();

        return $log;
    }

    public function activationMonthly($order, $attributes)
    {
        if (in_array($order->type, [ 'ativacao_mensal', 'ativacao_mensal_cd' ])) {
            if ($order->amount >= 90) {
                $user = \Auth::user();

                if (!is_null($user->distributor)) {
                    if (!$order->raised_points) {
                        $point = new Point;
                        $point->distributor_id = $user->distributor->id;
                        $point->quantity = 60;
                        $point->side = $point->lower_side();
                        $point->save();

                        $order->raised_points = true;
                        $order->save();
                    };

                    $moves = $user->distributor_moves();
                    if (!$order->raised_moves) {
                        foreach ($moves as $key => $move) {
                            $new_move = new DistributorMove;
                            $new_move->bonus_id = 2;
                            $new_move->type = 'in';
                            $new_move->distributor_id = $move->distributor_id;
                            $new_move->amount = $move->amount;
                            $new_move->save();
                        };

                        $order->raised_moves = true;
                        $order->save();
                    };
                };
            };
        };

        return true;
    }
}
