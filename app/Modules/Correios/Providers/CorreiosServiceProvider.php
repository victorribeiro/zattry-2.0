<?php
namespace App\Modules\Correios\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class CorreiosServiceProvider extends ServiceProvider
{
	/**
	 * Register the Correios module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\Correios\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the Correios module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('correios', realpath(__DIR__.'/../Resources/Lang'));

		View::addNamespace('correios', realpath(__DIR__.'/../Resources/Views'));
	}
}
