<?php
namespace App\Modules\Correios\Helpers;

use Cagartner\CorreiosConsulta\CorreiosConsulta;

class CorreiosHelper
{
    protected static $cep;

    protected static $origin = '51030-390';

    protected static $options = [
        /**
        * Código da empresa junto aos correios, não obrigatório.
        */
        'empresa' => '',
        /**
        * Senha da empresa junto aos correios, não obrigatório.
        */
        'senha' => '',
        /**
        * opções: (
        *  sedex, sedex_a_cobrar, sedex_10, sedex_hoje,
        *  pac, pac_contrato, sedex_contrato , esedex
        * )
        */
        'tipo' => 'pac',
        /**
        * opções: (caixa, rolo, envelope)
        */
        'formato' => 'caixa',
        /**
        * Obrigatorio
        */
        'cep_origem' => '51030-390',
        /**
        * Obrigatório
        */
        'cep_destino' => '89062-086',
        /**
        * Peso em kilos
        */
        'peso' => 0.3,
        /**
        * Em centímetros
        */
        'comprimento' => 35,
        /**
        * Em centímetros
        */
        'altura' => 10,
        /**
        * Em centímetros
        */
        'largura' => 30,
        /**
        * Em centímetros, no caso de rolo
        */
        'diametro' => 0,
        /**
        * Não obrigatórios
        */
        # 'mao_propria' => 1,
        /**
        * Não obrigatórios
        */
        # 'valor_declarado' => 1,
        /**
        * Não obrigatórios
        */
        # 'aviso_recebimento' => 1,
    ];

    public static function cep($cep = null)
    {
        self::$cep = is_null($cep) ? self::$cep : $cep;
        $cep = str_replace('-', '', self::$cep);

        return substr($cep, 0, 5) . '-' . substr($cep, 5, 3);
    }

    public static function address($cep)
    {
        $correios = new CorreiosConsulta;

        $address = $correios->cep( self::cep($cep) );

        if (empty($address)) {
            return (object) [
                'success' => false,
                'message' => 'Nenhum endereço encontrado para o cep "' . $cep . '"'
            ];
        }
        else {
            return (object) [
                'success' => true,
                'address' => [
                    'cep' => self::cep(),
                    'street' => $address['logradouro'],
                    'neighborhood' => $address['bairro'],
                    'city' => $address['cidade'],
                    'state' => $address['uf'],
                    'country' => 'Brasil',
                ]
            ];
        }
    }

    public static function tracking($code)
    {
        $correios = new CorreiosConsulta;

        $traces = $correios->rastrear($code);

        if (is_array($traces)) {
            $statuses = [];
            foreach ($traces as $key => $trace) {
                $datetime = explode(' ', $trace['data']);
                $statuses[] = (object) [
                    'date' => $datetime[0],
                    'time' => $datetime[1],
                    'local' => $trace['local'],
                    'status' => $trace['status'],
                    'forwarding' => isset($trace['encaminhado']) ? $trace['encaminhado'] : null,
                ];
            };
            return [ 'success' => true, 'statuses' => $statuses ];
        }
        else {
            return [ 'success' => false, 'message' => 'Não há nenhum status cadastrado ao código de rastreio "' . $code . '", ' ];
        };
    }

    public static function freight($cep, $options = [])
    {
        $correios = new CorreiosConsulta;

        self::$options = array_merge(self::$options, $options);

        self::$options['cep_origem'] = self::$origin;
        self::$options['cep_destino'] = self::cep($cep);

        $freight = $correios->frete(self::$options);

        if ($freight['erro']['codigo'] == 0) {
            return (object) [
                'success' => true,
                'deadline' => (int) $freight['prazo'],
                'price' => (float) $freight['valor'],
            ];
        }
        else {
            return (object) [
                'success' => false,
                'price' => 0.00,
                'message' => 'Nenhum endereço encontrado para o cep "' . self::cep() . '"'
            ];
        }
    }

    public static function saveFreight($cep)
    {
        $freight = \Correios::freight($cep);

        \Cart::freight( $freight->price );
        if ($freight->success) {
            \Cart::cep( \Correios::cep() );
            \Cart::deadline( $freight->deadline );
        }

        return $freight;
    }
}
