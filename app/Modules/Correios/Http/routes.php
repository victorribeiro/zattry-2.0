<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(['prefix' => 'correios'], function() {
    Route::get('freight/{cep}', 'CorreiosController@freight');
    Route::get('address/{cep}', 'CorreiosController@address');
    Route::get('tracking/{code}', 'CorreiosController@tracking');
});
