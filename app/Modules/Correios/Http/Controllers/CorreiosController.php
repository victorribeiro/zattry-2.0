<?php

namespace App\Modules\Correios\Http\Controllers;

use App\Http\Controllers\BaseController;

class CorreiosController extends BaseController
{
    public function freight($cep)
    {
        return (array) \Correios::saveFreight($cep);
    }

    public function address($cep)
    {
        return (array) \Correios::address($cep);
    }

    public function tracking($code)
    {
        return (array) \Correios::tracking($code);
    }
}
