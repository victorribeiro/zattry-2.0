var Correios = {
  init: function () {
    this.address();
  },

  address: function () {
    $('#address-cep').on('blur', function () {
      var field = $(this);
      var prefix = field.data('prefix');
      if (field.val() != '') {
        Application.showLoading();

        $.ajax({
          url: '/correios/address/' + field.val(),
          success: function (response) {
            if (response.success) {
              $('#' + prefix + '-street').val( response.address.street );
              $('#' + prefix + '-neighborhood').val( response.address.neighborhood );
              $('#' + prefix + '-city').val( response.address.city );
              $('#' + prefix + '-state').val( response.address.state );
              $('#correios-message').text( '' );
            }
            else {
              $('#correios-message').text( response.message );
            };
          }
        })
        .always(function () {
          Application.hideLoading()
        });
      };
    })
  }
};

$(document).on('ready', function () { Correios.init() })
