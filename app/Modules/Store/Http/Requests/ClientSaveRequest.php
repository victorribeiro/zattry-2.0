<?php
namespace App\Modules\Store\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientSaveRequest extends FormRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            'client.document' => [ 'required', 'cpf' ],
            'client.born_in' => [ 'required', 'date_format:d/m/Y', 'before:' . date('d/m/Y') ],
            'client.phone' => [ 'required' ],
            'client.agree' => [ 'required' ],
		];
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}
}
