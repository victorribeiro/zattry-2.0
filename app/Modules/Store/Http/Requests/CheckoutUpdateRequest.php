<?php
namespace App\Modules\Store\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CheckoutUpdateRequest extends FormRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            'quantity' => [ 'required' ],
		];
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}
}
