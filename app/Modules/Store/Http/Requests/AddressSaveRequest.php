<?php
namespace App\Modules\Store\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddressSaveRequest extends FormRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            'address.name' => [ 'required' ],
            'address.receiver' => [ 'required' ],
            'address.cep' => [ 'required' ],
            'address.street' => [ 'required' ],
            'address.reference' => [ 'required' ],
            'address.neighborhood' => [ 'required' ],
            'address.city' => [ 'required' ],
            'address.state' => [ 'required' ],
		];
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}
}
