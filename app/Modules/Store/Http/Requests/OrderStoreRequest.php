<?php
namespace App\Modules\Store\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderStoreRequest extends FormRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            'address_id' => [ 'required' ],
            'card.form' => [ 'required' ],
            'card.name' => [ 'required', /*'required_if:card.type,1'*/ ],
            'card.cpf' => [ 'required', /*'required_if:card.type,1'*/ ],
            'card.card' => [ 'required', /*'required_if:card.type,1'*/ ],
            'card.code' => [ 'required', /*'required_if:card.type,1'*/ ],
            'card.year' => [ 'required', /*'required_if:card.type,1'*/ ],
            'card.month' => [ 'required', /*'required_if:card.type,1'*/ ],
            'card.installments' => [ 'required', /*'required_if:card.type,1'*/ ],
		];
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}
}
