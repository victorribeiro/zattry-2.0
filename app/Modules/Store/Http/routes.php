<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('products', 'ProductsController@index');
Route::get('products/{code}/{slug}', 'ProductsController@show');

Route::resource('checkout', 'CheckoutController', [ 'except' => [ 'show', 'create' ] ]);

Route::group([ 'middleware' => 'auth' ], function() {
    Route::resource('orders', 'OrdersController', [ 'only' => [ 'index', 'create', 'store', 'show' ] ]);
    Route::resource('users/addresses', 'AddressesController', [ 'only' => [ 'index', 'create', 'store', 'edit', 'update', 'destroy' ] ]);
});

Route::group([ 'prefix' => 'dashboard', 'middleware' => 'auth' ], function() {
    Route::get('/', 'HomeController@index');
});

Route::group([ 'prefix' => 'admin', 'middleware' => 'auth' ], function() {
    Route::resource('categories', 'Admin\CategoriesController');
});

Route::group([ 'prefix' => 'users', 'middleware' => 'auth' ], function() {
    Route::resource('clients', 'ClientsController', [ 'only' => [ 'index', 'store' ] ]);
});

Route::get('costumes/{kit_id}/activation', 'CostumesController@activation');
