<?php

namespace App\Modules\Store\Http\Controllers;

use App\Http\Controllers\BaseController;

use App\User;
use App\Modules\Store\Repositories\OrderRepository;
use App\Modules\Store\Http\Requests\OrderStoreRequest;

class OrdersController extends BaseController
{
    public function __construct(OrderRepository $order)
    {
        parent::__construct();
        $this->repository = $order;
    }

    public function index()
    {
        $orders = $this->repository->all();
        return view('store::orders/index')->with('orders', $orders);
    }

    public function create()
    {
        $user = User::find( \Auth::id() );
        $order = $this->repository->init([ 'user_id' => \Auth::id() ]);

        return view('store::orders/create', [ 'order' => $order, 'user' => $user ]);
    }

    public function store(OrderStoreRequest $request)
    {
        $stored = $this->repository->store( $request->except('_token') );

        if ($stored->success()) {
            return \Module::redirectAction('Store', 'OrdersController@show',$stored->eloquent->code)
                ->with('success', $stored->message);
        }
        else {
            return \Module::redirectAction('Store', 'OrdersController@create')
                ->withInput()->withErrors($stored->message);
        }
    }

    public function show($code)
    {
        $order = $this->repository->findByCode($code);
        return view('store::orders/show', [ 'order' => $order ]);
    }
}
