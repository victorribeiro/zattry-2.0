<?php

namespace App\Modules\Store\Http\Controllers;

use App\Http\Controllers\BaseController;

use App\Modules\Store\Repositories\ProductRepository;

class ProductsController extends BaseController
{
    public function __construct(ProductRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    public function index()
    {
        $categories = $this->repository->parentCategories();
        $products = $this->repository->filterByCategory( \Input::get('category') );

        return view('store::products/index')
            ->with('categories', $categories)
            ->with('products', $products);
    }

    public function show($code)
    {
        $product = $this->repository->findByCode($code);
        $colors = $this->repository->productColorsByCostume($product->costume_id);
        $sizes = $this->repository->productSizesByProduct($product->id);

        return view('store::products/show')
            ->with('sizes', $sizes)
            ->with('colors', $colors)
            ->with('product', $product);
    }
}
