<?php

namespace App\Modules\Store\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Modules\Store\Repositories\CategoryRepository;
use App\Modules\Store\Http\Requests\Admin\CategorySaveRequest;

class CategoriesController extends Controller
{
    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $categories = $this->repository->all();
        return view('store::admin/categories/index')->with('categories', $categories);
    }

    public function create()
    {
        $category = $this->repository->init();
        $categories = $this->repository->categoryLists();
        return view('store::admin/categories/create')->with('category', $category)->with('categories', $categories);
    }

    public function store(CategorySaveRequest $request)
    {
        $stored = $this->repository->store( $request->except('_token', '_method') );

        if ($stored->success()) {
            return redirect('admin/categories')->with('success', $stored->message);
        }
        else {
            return redirect('admin/categories/create')->withInput()->withErrors($stored->errors);
        }
    }

    public function edit($id)
    {
        $category = $this->repository->find($id);
        if (is_null($category)) return redirect('admin/categories')->with('error', 'Categoria não encontrada');

        $categories = $this->repository->categoryLists();
        return view('store::admin/categories/edit')->with('category', $category)->with('categories', $categories);
    }

    public function update($id, CategorySaveRequest $request)
    {
        $updated = $this->repository->update( $id, $request->except('_token', '_method') );

        if ($updated->success()) {
            return redirect('admin/categories')->with('success', $updated->message);
        }
        else {
            return redirect('admin/categories/' . $id . '/edit')->withInput()->withErrors($updated->message);
        }
    }

    public function destroy($id)
    {
        $destroyed = $this->repository->destroy( $id );

        if ($destroyed->success()) {
            return redirect('admin/categories')->with('success', $destroyed->message);
        }
        else {
            return redirect('admin/categories')->with('error', $destroyed->message);
        } # endif;
    }
}
