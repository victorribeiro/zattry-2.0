<?php

namespace App\Modules\Store\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Modules\Store\Repositories\ProductsRepository;
use App\Modules\Store\Http\Requests\Admin\ProductsSaveRequest;
use App\Modules\Store\Models\Category;

class ProductsController extends Controller
{
    public function __construct(ProductsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $products = $this->repository->all();
        return view('store::admin/products/index')->with('products', $products);
    }

    public function create()
    {
        $product = $this->repository->init();
        $categories = Category::lists('name', 'id');
        return view('store::admin/products/create')->with('product', $product)->with('categories', $categories);
    }

    public function store(ProductsSaveRequest $request)
    {
        $stored = $this->repository->store( $request->except('_token', '_method') );

        if ($stored->success()) {
            return redirect('admin/products')->with('success', $stored->message);
        }
        else {
            return redirect('admin/products/create')->withInput()->withErrors($stored->errors);
        }
    }

    public function edit($id)
    {
        $product = $this->repository->find($id);
        if (is_null($product)) return redirect('admin/products')->with('error', 'Produto não encontrado');

        $categories = Category::lists('name', 'id');
        return view('store::admin/products/create')->with('product', $product)->with('categories', $categories);
    }

    public function update($id, ProductsSaveRequest $request)
    {
        $updated = $this->repository->update( $id, $request->except('_token', '_method') );

        if ($updated->success()) {
            return redirect('admin/products')->with('success', $updated->message);
        }
        else {
            return redirect('admin/products/' . $id . '/edit')->withInput()->withErrors($updated->message);
        }
    }

    public function destroy($id)
    {
        $destroyed = $this->repository->destroy( $id );

        if ($destroyed->success()) {
            return redirect('admin/products')->with('success', $destroyed->message);
        }
        else {
            return redirect('admin/products')->with('error', $destroyed->message);
        } # endif;
    }
}
