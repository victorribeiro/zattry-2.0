<?php

namespace App\Modules\Store\Http\Controllers;

use App\Http\Controllers\BaseController;
use App\Modules\Store\Repositories\OrderRepository;

class HomeController extends BaseController
{
    public function __construct(OrderRepository $order)
    {
        parent::__construct();
        $this->order = $order;
    }

    public function index()
    {
        return view('store::home/index', [
            'orders' => $this->order->myLatestOrders(),
            'addresses' => \Auth::user()->addresses()->limit(3)->latest()->get()
        ]);
    }
}
