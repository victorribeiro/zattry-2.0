<?php

namespace App\Modules\Store\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\BaseController;

use App\Modules\Store\Repositories\AddressRepository;
use App\Modules\Store\Http\Requests\AddressSaveRequest;

class AddressesController extends BaseController
{
    public function __construct(AddressRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
        $this->types = $repository;
    }

    public function index()
    {
        $addresses = $this->repository->all();
        return view('store::addresses/index')->with('addresses', $addresses);
    }

    public function create()
    {
        $address = $this->repository->init();
        return view('store::addresses/create', [ 'address' => $address ]);
    }

    public function store(AddressSaveRequest $request)
    {
        $stored = $this->repository->store( $request->get('address') );

        if ($stored->success()) {
            return \Module::redirectAction('Store', 'AddressesController@index')
                ->with('success', $stored->message);
        }
        else {
            return \Module::action('Store', 'AddressesController@create')
                ->withInput()->withErrors($stored->errors);
        }
    }

    public function edit($code)
    {
        $address = $this->repository->findByCode($code);
        if (is_null($address)) {
            return \Module::redirectAction('Store', 'AddressesController@index')
                ->with('error', 'Endereço não encontrado');
        }

        return view('store::addresses/edit')->with('address', $address);
    }

    public function update($code, AddressSaveRequest $request)
    {
        $updated = $this->repository->updateByCode( $code, $request->get('address') );

        if ($updated->success()) {
            return \Module::redirectAction('Store', 'AddressesController@index')
                ->with('success', $updated->message);
        }
        else {
            return \Module::action('Store', 'AddressesController@edit', $code)
                ->withInput()->withErrors($updated->errors);
        }
    }

    public function destroy($code)
    {
        $destroyed = $this->repository->destroyByCode( $code );

        if ($destroyed->success()) {
            return \Module::redirectAction('Store', 'AddressesController@index')
                ->with('success', $destroyed->message);
        }
        else {
            return \Module::redirectAction('Store', 'AddressesController@index')
                ->with('error', $destroyed->message);
        } # endif;
    }
}
