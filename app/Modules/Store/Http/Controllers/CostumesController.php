<?php

namespace App\Modules\Store\Http\Controllers;

use App\Http\Controllers\BaseController;

use App\Modules\Store\Models\Stock;
use App\Modules\Store\Models\KitCostume;

class CostumesController extends BaseController
{
    public function activation($kit_id)
    {
        $kit_costumes = KitCostume::select('kit_costumes.*')
            ->where('kit_costumes.kit_id', $kit_id)
        ->get();

        foreach ($kit_costumes as $key => $kit_costume) {
            $stocks = Stock::select('stocks.*')
                ->join('products', 'products.id', '=', 'stocks.product_id')
                ->join('kit_costumes', 'kit_costumes.costume_id', '=', 'products.costume_id')
                ->where('kit_costumes.kit_id', $kit_id)
                ->where('kit_costumes.id', $kit_costume->id)
                ->where('stocks.quantity', '>', 0)
            ->get();

            $kit_costume->stocks = $stocks;
        }

        return view('store::costumes/activation', [ 'kit_costumes' => $kit_costumes ]);
    }
}
