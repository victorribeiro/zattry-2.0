<?php

namespace App\Modules\Store\Http\Controllers;

use App\Http\Controllers\BaseController;

use App\Modules\Store\Repositories\CartRepository;
use App\Modules\Store\Http\Requests\CheckoutStoreRequest;
use App\Modules\Store\Http\Requests\CheckoutUpdateRequest;

class CheckoutController extends BaseController
{
    public function __construct(CartRepository $cart)
    {
        parent::__construct();
        $this->cart = $cart;
    }

    public function index()
    {
        return view('store::checkout/index');
    }

    public function store(CheckoutStoreRequest $request)
    {
        $stock_id = $request->get('size');
        $cart = $this->cart->add( $stock_id );

        if ($cart->success) {
            return redirect( \Module::action('Store', 'CheckoutController@index') )
                ->with('success', $cart->message);
        }
        else {
            return redirect( $request->get('color') )->with('error', $cart->message);
        }
    }

    public function update($stock_id, CheckoutUpdateRequest $request)
    {
        $cart = $this->cart->update($stock_id, $request->get('quantity') );

        $previous_url = \Module::action('Store', 'CheckoutController@index');
        if ($cart->success) {
            return redirect($previous_url)->with('success', $cart->message);
        }
        else {
            return redirect($previous_url)->with('error', $cart->message);
        }
    }

    public function destroy($stock_id)
    {
        $cart = $this->cart->destroy($stock_id);

        $previous_url = \Module::action('Store', 'CheckoutController@index');
        if ($cart->success) {
            return redirect($previous_url)->with('success', $cart->message);
        }
        else {
            return redirect($previous_url)->with('error', $cart->message);
        }
    }
}
