<?php

namespace App\Modules\Store\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Modules\Store\Repositories\ClientRepository;
use App\Modules\Store\Http\Requests\ClientSaveRequest;

class ClientsController extends Controller
{
    public function __construct(ClientRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $client = $this->repository->firstOrNew();
        return view('store::clients/index')->with('client', $client);
    }

    public function store(ClientSaveRequest $request)
    {
        $updated = $this->repository->saveByUser( $request->get('client') );

        if ($updated->success()) {
            return \Module::redirectAction('Store', 'ClientsController@index')
                ->with('success', $updated->message);
        }
        else {
            return \Module::action('Store', 'ClientsController@edit', $code)
                ->withInput()->withErrors($updated->errors);
        }
    }
}
