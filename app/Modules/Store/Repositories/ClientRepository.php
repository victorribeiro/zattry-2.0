<?php
namespace App\Modules\Store\Repositories;

use App\Repositories\BaseRepository;
use App\Modules\Store\Models\Client;

class ClientRepository extends BaseRepository
{
    use Finders\ClientFinder;
    public $name = 'Dados';
    public $gender = 'o';

    public function __construct(Client $eloquent)
    {
        $this->eloquent = $eloquent;
    }

    public function firstOrNew()
    {
        return $this->eloquent->firstOrNew([ 'user_id' => \Auth::id() ]);
    }

    public function saveByUser($attributes)
    {
        $this->eloquent = $this->firstOrNew();
        $this->eloquent->user_id = \Auth::id();

        if (empty($this->eloquent->id)) {
            return $this->store($attributes);
        }
        else {
            return $this->update($this->eloquent->id, $attributes);
        }

        return $this;
    }

    public function beforeSave($attributes)
    {
        $attributes['born_in'] = \Date::format($attributes['born_in'], 'Y-m-d');
        $attributes['agree'] = $attributes['agree'] == 'on' ? 1 : 0;

        return $attributes;
    }
}
