<?php
namespace App\Modules\Store\Repositories;

use App\Repositories\BaseRepository;
use App\Modules\Store\Models\Product;

class ProductRepository extends BaseRepository
{
    use Finders\ProductFinder;

    public $name = 'produto';
    public $gender = 'o';

    public function __construct(Product $eloquent)
    {
        $this->eloquent = $eloquent;
    }
}
