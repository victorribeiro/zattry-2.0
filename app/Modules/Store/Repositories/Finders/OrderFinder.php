<?php

namespace App\Modules\Store\Repositories\Finders;

trait OrderFinder
{
    public function defaultScope()
    {
        return $this->eloquent->where('user_id', \Auth::id());
    }

    public function findByCode($code)
    {
        return $this->defaultScope()
            ->where('code', $code)
        ->first();
    }

    public function myLatestOrders($limit = 5)
    {
        return $this->defaultScope()->limit($limit)->latest()->get();
    }
}
