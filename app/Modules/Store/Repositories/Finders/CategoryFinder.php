<?php

namespace App\Modules\Store\Repositories\Finders;

trait CategoryFinder
{
    protected function defaultScope()
    {
        return $this->eloquent->with('category');
    }

    public function categoryLists()
    {
        return [ '' => '' ] + $this->eloquent->lists('name', 'id')->toarray();
    }
}
