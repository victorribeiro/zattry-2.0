<?php

namespace App\Modules\Store\Repositories\Finders;

trait AddressFinder
{
    protected function defaultScope()
    {
        return $this->eloquent->where('owner_id', \Auth::id())->where('owner_type', 'user');
    }

    public function findByCode($code)
    {
        return $this->defaultScope()->where('code', $code)->first();
    }
}
