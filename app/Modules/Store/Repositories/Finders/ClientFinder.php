<?php

namespace App\Modules\Store\Repositories\Finders;

trait ClientFinder
{
    protected function defaultScope()
    {
        return $this->eloquent->where('user_id', \Auth::id());
    }
}
