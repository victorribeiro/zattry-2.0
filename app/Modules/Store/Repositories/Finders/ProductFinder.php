<?php

namespace App\Modules\Store\Repositories\Finders;

use App\Modules\Store\Models\Stock;
use App\Modules\Store\Models\Category;

trait ProductFinder
{
    protected function defaultScope()
    {
        return $this->eloquent->select('products.*')->with('color')
            ->with('costume')->with('costume.category');
    }

    public function latestHighlighted($limit = 8)
    {
        return $this->defaultScope()
            ->highlighted()
            ->limit($limit)
        ->get();
    }

    public function filterByCategory($category_id)
    {

        $query = $this->defaultScope()
            ->leftjoin('costumes', 'costumes.id', '=', 'products.costume_id');
        if ($category_id) {
            $first_ids = Category::where('category_id', $category_id)->lists('id');
            $second_ids = Category::whereIn('category_id', $first_ids)->lists('id');

            $category_ids = array_merge($first_ids->toarray(), $second_ids->toarray(), [ $category_id ]);

            $query->whereIn('costumes.category_id', $category_ids);
        }
        return $query->paginate($this->perpage);
    }

    public function productSizesByProduct($product_id)
    {
        $lists = [];

        $stocks = Stock::with('size')->where('product_id', $product_id)->where('quantity', '>', 0)->get();

        foreach ($stocks as $key => $stock) {
            $lists[ $stock->id ] = $stock->size->name . ' (' . $stock->quantity . ' unidades)';
        }

        return [ '' => 'Selecione o tamanho' ] + $lists;
    }

    public function productColorsByCostume($costume_id)
    {
        $lists = [];

        $products = $this->eloquent->with('color')->where('costume_id', $costume_id)->get();
        foreach ($products as $key => $product) {
            $lists[ $product->url() ] = $product->color->name;
        }

        return $lists;
    }

    public function parentCategories()
    {
        return Category::with('categories.categories')
            ->with('categories')
            ->where('category_id', 0)
        ->get();
    }

    public function findByCode($code)
    {
        return $this->defaultScope()->where('products.code', $code)->first();
    }
}
