<?php
namespace App\Modules\Store\Repositories;

use App\Modules\Store\Models\Stock;

class CartRepository
{
    protected $session_name = 'cart';
    protected $session_stock_name = 'cart.stocks';

    public function __construct(Stock $stock)
    {
        $this->stock = $stock;
    }

    public function all()
    {
        $cart = session()->get( $this->session_stock_name );
        $ids = is_array($cart) ? array_keys( $cart ) : [];

        return $this->stock->whereIn('id', $ids)->get();
    }

    public function add($id)
    {
        $stock = $this->stock->available()->find($id);

        if (is_null($stock)) {
            $success = false;
            $message = 'O produto "' . $stock->product->name() . '" não está mais disponível';
        }
        else {
            $quantity = session()->get($this->session_stock_name . '.' . $stock->id);
            $this->putToCart( $stock->id, $quantity + 1 );

            $success = true;
            $message = 'O produto "' . $stock->product->name() . '" foi adicionado ao carrinho';
        }

        return (object) [ 'success' => $success, 'message' => $message ];
    }

    public function update($id, $quantity)
    {
        $stock = $this->stock->available()->find($id);

        if (is_null($stock)) {
            $success = false;
            $message = 'O produto "' . $stock->product->name() . '" não está mais disponível';
        }
        else {
            $this->putToCart( $stock->id, $quantity );

            $success = true;
            $message = 'A quantidade do produto "' . $stock->product->name() . '" foi alterada';
        }

        return (object) [ 'success' => $success, 'message' => $message ];
    }

    public function destroy($id)
    {
        $stock = $this->stock->available()->find($id);

        if (is_null($stock)) {
            return (object) [ 'success' => false, 'message' => 'O produto "' . $stock->product->name() . '" não está mais disponível' ];
        }
        else {
            $this->removeToCart( $stock->id );
            return (object) [ 'success' => true, 'message' => 'O produto "' . $stock->product->name() . '" foi removido do carrinho' ];
        }
    }

    protected function removeToCart($id)
    {
        session()->forget($this->session_stock_name . '.' . $id);
    }

    protected function putToCart($id, $quantity)
    {
        $quantity = round($quantity) > 0 ? round($quantity) : 1;
        session()->put($this->session_stock_name . '.' . $id, $quantity);
    }
}
