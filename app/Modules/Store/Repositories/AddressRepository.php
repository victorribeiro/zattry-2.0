<?php
namespace App\Modules\Store\Repositories;

use App\Repositories\BaseRepository;
use App\Modules\Store\Models\Address;

class AddressRepository extends BaseRepository
{
    use Finders\AddressFinder;
    public $name = 'endereço';
    public $gender = 'o';

    public function __construct(Address $eloquent)
    {
        $this->eloquent = $eloquent;
    }

    public function beforeStore($attributes)
    {
        $this->eloquent->owner_id = \Auth::id();
        $this->eloquent->owner_type = 'user';
        $this->eloquent->code = str_random(13);

        return $attributes;
    }

    public function updateByCode($code, $attributes)
    {
        $address = $this->findByCode($code);

        return parent::update($address->id, $attributes);
    }

    public function destroyByCode($code)
    {
        $address = $this->findByCode($code);

        return parent::destroy($address->id);
    }
}
