<?php
namespace App\Modules\Store\Repositories;

use App\Repositories\BaseRepository;
use App\Modules\Store\Models\Category;

class CategoryRepository extends BaseRepository
{
    use Finders\CategoryFinder;
    public $name = 'categoria';
    public $gender = 'a';

    public function __construct(Category $eloquent)
    {
        $this->eloquent = $eloquent;
    }
}
