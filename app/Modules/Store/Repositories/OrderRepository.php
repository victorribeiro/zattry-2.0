<?php
namespace App\Modules\Store\Repositories;

use App\Repositories\BaseRepository;
use App\Modules\Store\Models\Order;
use App\Modules\Store\Models\Item;
use App\Modules\Payments\Models\PaymentStatus;
use App\Modules\Payments\Models\Payment;
use App\Modules\Payments\Services\MaxiPagoService;

class OrderRepository extends BaseRepository
{
    use Finders\OrderFinder;

    public $name = 'produto';
    public $gender = 'o';

    public function __construct(Order $eloquent)
    {
        $this->eloquent = $eloquent;
    }

    public function beforeStore($attributes)
    {
        $this->eloquent->address_id = empty($attributes['address_id']) || $attributes['address_id'] == 0 ? null : $attributes['address_id'];
        $this->eloquent->user_id = \Auth::id();
        $this->eloquent->code = str_random(13);
        $this->eloquent->amount = \Cart::subtotal();
        $this->eloquent->type = $this->eloquent->handle_type();
        $this->eloquent->freight_type = \Cart::freight() > 0 ? 'cd' : 'pac';
        $this->eloquent->freight_amount = \Cart::freight();

        return $attributes;
    }

    public function afterStore($attributes)
    {
        $items = [];
        foreach (\Cart::stocks() as $key => $stock) {
            $items[] = new Item([
                'stock_id' => $stock->id,
                'quantity' => $stock->quantity(),
                'amount' => $stock->product->discounted_amount(),
            ]);
        };

        if ($this->eloquent->items()->saveMany( $items )) {
            $this->maxipago($attributes);
        }
        else {
            $this->message(false, self::TRY_AGAIN_MESSAGE);
        };
    }

    private function maxipago($attributes)
    {
        $this->maxipago = new MaxiPagoService;

        $installments = $attributes['card']['installments'];
        $total = MaxiPagoService::total( \Cart::total(), $installments);

        $pay_attributes = [
            'processorID' => env('MAXIPAGO_PROCESS_ID'), // REQUIRED - Use '1' for testing. Contact our team for production values //
            'referenceNum' => $this->eloquent->code, // REQUIRED - Merchant internal order number //
            'chargeTotal' => $total, // REQUIRED - Transaction amount in US format //
            'numberOfInstallments' => $installments, // Optional - Number of installments ("parcelas") //
            'number' => str_replace(' ', '', $attributes['card']['card']), // REQUIRED - Full credit card number //
            'expMonth' => $attributes['card']['month'], // REQUIRED - Credit card expiration month //
            'expYear' => $attributes['card']['year'], // REQUIRED - Credit card expiration year //
            'cvvNumber' => $attributes['card']['code'], // RECOMMENDED - Credit card verification code //
            'bname' => $attributes['card']['name'], //RECOMMENDED - Customer name //
            'chargeInterest' => 'N', // Optional - Charge interest flag (Y/N), used with installments ("com" ou "sem" juros) //
            'fraudCheck' => 'N', // Optional - Trigger fraud analysis for the transaction //
        ];

        $this->maxipago->pay($pay_attributes);

        if ($this->maxipago->success()) {
            $status = PaymentStatus::where('gateway', 'maxipago')->where('code', 0)->first();

            $payment = Payment::firstOrNew([ 'order_id' => $this->eloquent->id ]);
            $payment->payment_status_id = $status->id;
            $payment->gateway = 'maxipago';
            $payment->gateway_order_id = $this->maxipago->getOrderID();
            $payment->authorization_code = $this->maxipago->getAuthCode();
            $payment->total = $total;
            $payment->card = $attributes['card']['form'];
            $payment->parcel = MaxiPagoService::calculateInstallment( \Cart::total(), $installments);
            $payment->installments = $installments;

            if ($payment->save()) {
                \Cart::destroy();
                // $dados['id'] = $this->_usuario->distribuidor_id;
                // $dados['ativo'] = 1;
                // // $this->_db->confirmarCompra($dados);
            };
        };

        $this->message($this->maxipago->success(), $this->maxipago->message());
    }
}
