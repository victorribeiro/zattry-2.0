<?php

namespace App\Modules\Store\Helpers;

use App\Modules\Store\Repositories\CartRepository;
use App\Modules\Store\Models\Stock;

class CartHelper
{
    static $session_cart = 'cart';
    static $stocks;

    public static function stocks()
    {
        $cart_repository = new CartRepository(new Stock);
        return $cart_repository->all();
    }

    public static function subtotal($stocks = null)
    {
        if (empty($stocks)) {
            if (empty(self::$stocks)) {
                self::$stocks = self::stocks();
            }
        }

        $total = 0;
        foreach (self::$stocks as $key => $stock) {
            $total += $stock->total();
        }
        return $total;
    }

    public static function formatted_subtotal($stocks = null)
    {
        return \Number::format(self::subtotal($stocks));
    }

    public static function total($stocks = null)
    {
        return self::subtotal($stocks) + self::freight();
    }

    public static function formatted_total($stocks = null)
    {
        return \Number::format(self::total($stocks));
    }

    public static function freight($freight = null)
    {
        if (is_null($freight)) {
            $freight = session()->get( self::$session_cart . '.freight.price' );
        }
        else {
            session()->put( self::$session_cart . '.freight.price', $freight );
        }

        return is_numeric($freight) ? $freight : 0;
    }

    public static function formatted_freight()
    {
        return \Number::format(self::freight());
    }

    public static function deadline($deadline = null)
    {
        if (is_null($deadline)) {
            $deadline = session()->get( self::$session_cart . '.deadline' );
        }
        else {
            session()->put( self::$session_cart . '.deadline', $deadline );
        }

        return session()->get(self::$session_cart . '.deadline');
    }

    public static function formatted_deadline()
    {
        $deadline = self::deadline();

        if ($deadline > 0) {
            return 'Seu pedido será entregue em até ' . $deadline . ' dia(s) após a aprovação do pagamento';
        };
    }

    public static function cep($cep = null)
    {
        if (is_null($cep)) {
            $cep = session()->get( self::$session_cart . '.cep' );
        }
        else {
            session()->put( self::$session_cart . '.cep', $cep );
        }

        return session()->get(self::$session_cart . '.cep');
    }

    public static function hasCep()
    {
        $cep = self::cep();

        return !empty($cep);
    }

    public static function count()
    {
        $stocks = session()->get(self::$session_cart . '.stocks');

        return array_sum($stocks);
    }

    public static function destroy()
    {
        session()->forget(self::$session_cart . '.stocks');
        session()->forget(self::$session_cart . '.freight');
        session()->forget(self::$session_cart . '.deadline');
    }
}
