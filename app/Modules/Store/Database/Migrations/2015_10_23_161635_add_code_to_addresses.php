<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCodeToAddresses extends Migration
{
    public function up()
    {
        Schema::table('addresses', function (Blueprint $table) {
            $table->string('code')->after('id');
        });
    }

    public function down()
    {
        Schema::table('addresses', function ($table) {
            $table->dropColumn([ 'code' ]);
        });
    }
}
