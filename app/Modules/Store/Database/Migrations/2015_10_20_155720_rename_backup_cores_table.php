<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameBackupCoresTable extends Migration
{
    public function up()
    {
        Schema::rename('cores', 'z_cores');
    }

    public function down()
    {
        Schema::rename('z_cores', 'cores');
    }
}
