<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCostumeSizeTable extends Migration
{
    public function up()
    {
        Schema::create('costume_size', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('costume_id')->unsigned()->index();
            $table->integer('size_id')->unsigned()->index();
        });
    }

    public function down()
    {
        Schema::dropIfExists('costume_size');
    }
}
