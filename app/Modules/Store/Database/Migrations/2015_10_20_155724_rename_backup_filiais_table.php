<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameBackupFiliaisTable extends Migration
{
    public function up()
    {
        Schema::rename('filiais', 'z_filiais');
    }

    public function down()
    {
        Schema::rename('z_filiais', 'filiais');
    }
}
