<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImportDataFromPedidosToOrders extends Migration
{
    public function up()
    {
        $pedidos = \DB::table('pedidos')->get();

        $values = [];
        foreach ($pedidos as $key => $pedido) {
            $values[] = "({$pedido->id}, '{$pedido->usuario_id}', '{$pedido->codigo}', '{$pedido->valor}', '{$pedido->tipo}', '{$pedido->frete}', '{$pedido->valor_frete}', '{$pedido->status}', '{$pedido->forma_pagamento}', '{$pedido->parcelas}', '{$pedido->codigo_autorizacao}', '{$pedido->pagamento}', '{$pedido->cartao}', '{$pedido->criado_em}', '{$pedido->atualizado_em}', NULL)";
        }

        if (is_array($values)) {
            $sql = "INSERT INTO `orders` (`id`, `user_id`, `code`, `amount`, `type`, `freight_type`, `freight_amount`, `status`, `form_payment`, `installments`, `authorization_code`, `payment`, `card`, `created_at`, `updated_at`, `deleted_at`) VALUES " . implode(", ", $values) . ";";
            \DB::statement( $sql );
        }
    }

    public function down()
    {
        \DB::table('orders')->truncate();
    }
}
