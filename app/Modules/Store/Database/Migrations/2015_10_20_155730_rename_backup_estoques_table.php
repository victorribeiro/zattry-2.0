<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameBackupEstoquesTable extends Migration
{
    public function up()
    {
        Schema::rename('estoques', 'z_estoques');
    }

    public function down()
    {
        Schema::rename('z_estoques', 'estoques');
    }
}
