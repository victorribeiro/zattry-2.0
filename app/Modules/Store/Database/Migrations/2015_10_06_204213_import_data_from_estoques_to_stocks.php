<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImportDataFromEstoquesToStocks extends Migration
{
    public function up()
    {
        $estoques = \DB::table('estoques')->get();

        $values = [];
        foreach ($estoques as $key => $estoque) {
            $values[] = "({$estoque->id}, '{$estoque->mercadoria_id}', '{$estoque->filial_id}', '{$estoque->tamanho_id}', '{$estoque->quantidade}', '{$estoque->criado_em}', '{$estoque->atualizado_em}', NULL)";
        }

        if (is_array($values)) {
            $sql = "INSERT INTO `stocks` (`id`, `product_id`, `office_id`, `size_id`,  `quantity`, `created_at`, `updated_at`, `deleted_at`) VALUES " . implode(", ", $values) . ";";
            \DB::statement( $sql );
        }
    }

    public function down()
    {
        \DB::table('stocks')->truncate();
    }
}
