<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameBackupProdutosTamanhosTable extends Migration
{
    public function up()
    {
        Schema::rename('produtos_tamanhos', 'z_produtos_tamanhos');
    }

    public function down()
    {
        Schema::rename('z_produtos_tamanhos', 'produtos_tamanhos');
    }
}
