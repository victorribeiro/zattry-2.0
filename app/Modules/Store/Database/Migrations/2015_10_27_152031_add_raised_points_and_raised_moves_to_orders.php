<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRaisedPointsAndRaisedMovesToOrders extends Migration
{
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->boolean('raised_points')->default(false)->after('status');
            $table->boolean('raised_moves')->default(false)->after('raised_points');
        });

        \DB::table('orders')->update([ 'raised_points' => true, 'raised_moves' => true ]);
    }

    public function down()
    {
        Schema::table('orders', function ($table) {
            $table->dropColumn([ 'raised_points', 'raised_moves' ]);
        });
    }
}
