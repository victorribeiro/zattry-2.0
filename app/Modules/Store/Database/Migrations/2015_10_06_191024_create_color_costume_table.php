<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColorCostumeTable extends Migration
{
    public function up()
    {
        Schema::create('color_costume', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('color_id')->unsigned()->index();
            $table->integer('costume_id')->unsigned()->index();
        });
    }

    public function down()
    {
        Schema::dropIfExists('color_costume');
    }
}
