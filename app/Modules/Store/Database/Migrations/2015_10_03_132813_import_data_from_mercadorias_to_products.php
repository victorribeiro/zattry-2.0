<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImportDataFromMercadoriasToProducts extends Migration
{
    public function up()
    {
        $mercadorias = \DB::table('mercadorias')->get();

        $values = [];
        foreach ($mercadorias as $key => $mercadoria) {
            $status = $mercadoria->status == 'ativo';
            $values[] = "({$mercadoria->id}, '{$mercadoria->produto_id}', '{$mercadoria->filial_id}', '{$mercadoria->cor_id}', '{$mercadoria->classificacao_id}', '{$mercadoria->codigo}', '{$mercadoria->nome}', '{$mercadoria->imagem}', '{$mercadoria->valor}', '{$mercadoria->desconto}', '{$mercadoria->peso}', '{$status}', '{$mercadoria->estoque_minimo}', '{$mercadoria->ncm}', '{$mercadoria->destaque}', '{$mercadoria->criado_em}', '{$mercadoria->atualizado_em}', NULL)";
        }


        if (is_array($values)) {
            $sql = "INSERT INTO `products` (`id`, `costume_id`, `office_id`, `color_id`,  `classification_id`, `code`, `name`, `image`, `amount`, `discount`, `weight`, `status`, `minimum_stock`, `ncm`, `highlight`, `created_at`, `updated_at`, `deleted_at`) VALUES " . implode(", ", $values) . ";";
            \DB::statement( $sql );
        }
    }

    public function down()
    {
        \DB::table('products')->truncate();
    }
}
