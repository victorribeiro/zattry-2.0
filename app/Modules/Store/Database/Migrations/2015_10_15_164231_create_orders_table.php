<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('code');
            $table->decimal('amount', 17, 2);
            $table->string('type');
            $table->string('freight_type');
            $table->decimal('freight_amount', 17, 2)->default(0);
            $table->string('status')->nullable();
            $table->string('form_payment')->nullable();
            $table->integer('installments')->default(1);
            $table->string('authorization_code')->nullable();
            $table->string('payment')->nullable();
            $table->string('card')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
