<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
	public function up()
	{
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('costume_id')->unsigned()->index();
            $table->foreign('costume_id')->references('id')->on('costumes');
            $table->integer('office_id')->unsigned()->index();
            $table->foreign('office_id')->references('id')->on('offices');
            $table->integer('color_id')->unsigned()->index();
            $table->foreign('color_id')->references('id')->on('colors');
            $table->integer('classification_id')->unsigned()->index();
            $table->foreign('classification_id')->references('id')->on('classifications');
            $table->string('code');
            $table->string('name')->nullable();
            $table->string('image')->nullable();
            $table->decimal('amount', 10, 2);
            $table->decimal('discount', 10, 2);
            $table->integer('weight')->default(0);
            $table->boolean('status');
            $table->integer('minimum_stock')->default(0);
            $table->string('ncm')->nullable();
            $table->boolean('highlight');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('products');
    }
}
