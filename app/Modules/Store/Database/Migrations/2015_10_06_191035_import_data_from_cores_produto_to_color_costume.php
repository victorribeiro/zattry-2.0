<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImportDataFromCoresProdutoToColorCostume extends Migration
{
    public function up()
    {
        $ids = \DB::table('cores_produto')->get();

        $values = [];
        foreach ($ids as $key => $id) {
            $values[] = "({$id->id_cor}, {$id->id_produto})";
        }

        if (is_array($values)) {
            $sql = "INSERT INTO `color_costume` (`color_id`, `costume_id`) VALUES " . implode(", ", $values) . ";";
            \DB::statement( $sql );
        }
    }

    public function down()
    {
        \DB::table('color_costume')->truncate();
    }
}
