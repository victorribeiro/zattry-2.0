<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemovePaymentInstallmentsAutorizationCodeFormPaymentCardToOrders extends Migration
{
    public function up()
    {
        Schema::table('orders', function($table)
        {
            $table->dropColumn([ 'form_payment', 'installments',
                'authorization_code', 'payment', 'card' ]);
        });
    }

    public function down()
    {
        Schema::table('orders', function($table)
        {
            $table->string('form_payment');
            $table->integer('installments')->default(1);
            $table->string('authorization_code');
            $table->string('payment');
            $table->string('card');
        });
    }
}
