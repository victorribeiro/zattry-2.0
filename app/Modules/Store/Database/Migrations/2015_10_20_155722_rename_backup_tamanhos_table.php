<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameBackupTamanhosTable extends Migration
{
    public function up()
    {
        Schema::rename('tamanhos', 'z_tamanhos');
    }

    public function down()
    {
        Schema::rename('z_tamanhos', 'tamanhos');
    }
}
