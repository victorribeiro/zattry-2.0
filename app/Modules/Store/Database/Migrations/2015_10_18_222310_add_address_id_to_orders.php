<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAddressIdToOrders extends Migration
{
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->integer('address_id')->unsigned()->index()->after('user_id')->nullable();
            $table->foreign('address_id')->references('id')->on('addresses');
        });
    }

    public function down()
    {
        Schema::table('orders', function ($table) {
            $table->dropForeign('orders_address_id_foreign');
            $table->dropColumn([ 'address_id' ]);
        });
    }
}
