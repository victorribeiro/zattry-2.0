<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImportDataFromFiliaisToOffices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $filiais = \DB::table('filiais')->get();

        $values = [];
        foreach ($filiais as $key => $filial) {
            $values[] = "({$filial->id}, '{$filial->nome}', NOW(), NOW(), NULL)";
        }

        if (is_array($values)) {
            $sql = "INSERT INTO `offices` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES " . implode(", ", $values) . ";";
            \DB::statement( $sql );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::table('offices')->truncate();
    }
}
