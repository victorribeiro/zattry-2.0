<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetValueCodeToAddresses extends Migration
{
    public function up()
    {
        $addresses = \DB::table('addresses')->get();

        foreach ($addresses as $key => $address) {
            \DB::table('addresses')->where('id', $address->id)->update([ 'code' => str_random(13) ]);
        }
    }

    public function down()
    {
        \DB::table('addresses')->update([ 'code' => '' ]);
    }
}
