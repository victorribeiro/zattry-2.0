<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameBackupProdutosTable extends Migration
{
    public function up()
    {
        Schema::rename('produtos', 'z_produtos');
    }

    public function down()
    {
        Schema::rename('z_produtos', 'produtos');
    }
}
