<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameBackupPedidosTable extends Migration
{
    public function up()
    {
        Schema::rename('pedidos', 'z_pedidos');
    }

    public function down()
    {
        Schema::rename('z_pedidos', 'pedidos');
    }
}
