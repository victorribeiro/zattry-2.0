<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImportDataFromCategoriaToCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $categorias = \DB::table('categoria')->get();

        $values = [];
        foreach ($categorias as $key => $categoria) {
            $deleted_at = $categoria->ativo == 0 ? 'NOW()' : 'NULL';
            $values[] = "({$categoria->id}, '{$categoria->categoriapai_id}', '{$categoria->categoria}', NOW(), NOW(), {$deleted_at})";
        }

        if (is_array($values)) {
            $sql = "INSERT INTO `categories` (`id`, `category_id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES " . implode(", ", $values) . ";";
            \DB::statement( $sql );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::table('categories')->truncate();
    }
}
