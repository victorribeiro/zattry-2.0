<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImportForeignKeyFromUsersToClients extends Migration
{
    public function up()
    {
        $users = \DB::table('users')->whereNotNull('client_id')->get();

        foreach ($users as $key => $user) {
            \DB::table('clients')->where('id', $user->client_id)->update([ 'user_id' => $user->id ]);
        }
    }

    public function down()
    {
        \DB::table('clients')->update([ 'user_id' => null ]);
    }
}
