<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImportDataFromProdutosToCostumes extends Migration
{
    public function up()
    {
        $produtos = \DB::table('produtos')->get();

        $values = [];
        foreach ($produtos as $key => $produto) {
            $values[] = "({$produto->id}, {$produto->categoria_id}, '{$produto->nome}', '{$produto->descricao}', '{$produto->ativo}', '{$produto->destaque}', '{$produto->imagem}', NOW(), NOW(), NULL)";
        }

        if (is_array($values)) {
            $sql = "INSERT INTO `costumes` (`id`, `category_id`, `name`, `description`, `active`,  `highlight`,  `image`, `created_at`, `updated_at`, `deleted_at`) VALUES " . implode(", ", $values) . ";";
            \DB::statement( $sql );
        }
    }

    public function down()
    {
        \DB::table('costumes')->truncate();
    }
}
