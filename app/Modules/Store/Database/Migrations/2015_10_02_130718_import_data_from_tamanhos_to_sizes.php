<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImportDataFromTamanhosToSizes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tamanhos = \DB::table('tamanhos')->get();

        $values = [];
        foreach ($tamanhos as $key => $tamanho) {
            $deleted_at = $tamanho->ativo == 0 ? 'NOW()' : 'NULL';
            $values[] = "({$tamanho->id}, '{$tamanho->tamanho}', '{$tamanho->exibir}', NOW(), NOW(), {$deleted_at})";
        }

        if (is_array($values)) {
            $sql = "INSERT INTO `sizes` (`id`, `name`, `active`, `created_at`, `updated_at`, `deleted_at`) VALUES " . implode(", ", $values) . ";";
            \DB::statement( $sql );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::table('sizes')->truncate();
    }
}
