<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameBackupCategoriaTable extends Migration
{
    public function up()
    {
        Schema::rename('categoria', 'z_categoria');
    }

    public function down()
    {
        Schema::rename('z_categoria', 'categoria');
    }
}
