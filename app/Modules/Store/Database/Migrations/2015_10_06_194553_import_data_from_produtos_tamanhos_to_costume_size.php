<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImportDataFromProdutosTamanhosToCostumeSize extends Migration
{
    public function up()
    {
        $ids = \DB::table('produtos_tamanhos')->get();

        $values = [];
        foreach ($ids as $key => $id) {
            $values[] = "({$id->produto_id}, {$id->tamanho_id})";
        }

        if (is_array($values)) {
            $sql = "INSERT INTO `costume_size` (`costume_id`, `size_id`) VALUES " . implode(", ", $values) . ";";
            \DB::statement( $sql );
        }
    }

    public function down()
    {
        \DB::table('costume_size')->truncate();
    }
}
