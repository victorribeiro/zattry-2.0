<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameBackupCoresProdutoTable extends Migration
{
    public function up()
    {
        Schema::rename('cores_produto', 'z_cores_produto');
    }

    public function down()
    {
        Schema::rename('z_cores_produto', 'cores_produto');
    }
}
