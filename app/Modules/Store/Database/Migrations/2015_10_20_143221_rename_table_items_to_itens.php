<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameTableItemsToItens extends Migration
{
    public function up()
    {
        Schema::rename('items', 'z_items');
    }

    public function down()
    {
        Schema::rename('z_items', 'items');
    }
}
