<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImportDataFromClassificacaosToClassifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $classificacoes = \DB::table('classificacoes')->get();

        $values = [];
        foreach ($classificacoes as $key => $classificacao) {
            $values[] = "({$classificacao->id}, '{$classificacao->nome}', NOW(), NOW(), NULL)";
        }

        if (is_array($values)) {
            $sql = "INSERT INTO `classifications` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES " . implode(", ", $values) . ";";
            \DB::statement( $sql );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::table('classifications')->truncate();
    }
}
