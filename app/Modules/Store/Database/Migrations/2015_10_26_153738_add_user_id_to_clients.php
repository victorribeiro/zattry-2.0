<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserIdToClients extends Migration
{
    public function up()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->index()->after('id')->nullable()->default(null);
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    public function down()
    {
        Schema::table('clients', function ($table) {
            $table->dropForeign('clients_user_id_foreign');
            $table->dropColumn([ 'user_id' ]);
        });
    }
}
