<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameBackupClassificacoesTable extends Migration
{
    public function up()
    {
        Schema::rename('classificacoes', 'z_classificacoes');
    }

    public function down()
    {
        Schema::rename('z_classificacoes', 'classificacoes');
    }
}
