<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImportDataFromCoresToColors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $cores = \DB::table('cores')->get();

        $values = [];
        foreach ($cores as $key => $cor) {
            $deleted_at = $cor->ativo == 0 ? 'NOW()' : 'NULL';
            $values[] = "({$cor->id}, '{$cor->descricao}', NOW(), NOW(), {$deleted_at})";
        }

        if (is_array($values)) {
            $sql = "INSERT INTO `colors` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES " . implode(", ", $values) . ";";
            \DB::statement( $sql );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::table('colors')->truncate();
    }
}
