@extends(config('layout.default'))

@section('content')
  <div class="container">

    <div class="row">
      <div class="col-md-3">
        <h3>&nbsp;</h3><hr />
        @include('store::shared/_navigation_user')
      </div>

      <div class="col-md-9">
        <h3>
          <i class="fa fa-home"></i>
          Cadastrar outro endereço
        </h3>
        <hr />
        @include('store::addresses/_form', [ 'form_attributes' => [ 'method' => 'POST' , 'url' => Module::action('Store', 'AddressesController@store') ] ])
      </div>
    </div>
  </div>
@endsection
