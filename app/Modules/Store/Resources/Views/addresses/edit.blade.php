@extends(config('layout.default'))

@section('content')
  <div class="container">

    <div class="row">
      <div class="col-md-3">
        <h3>&nbsp;</h3><hr />
        @include('store::shared/_navigation_user')
      </div>

      <div class="col-md-9">
        <h3>
          <i class="fa fa-home"></i>
          Alterar endereço # {{ $address->name }}
        </h3>
        <hr />

        @include('store::addresses/_form', [ 'form_attributes' => [ 'method' => 'PUT' , 'url' => Module::action('Store', 'AddressesController@update', $address->code) ] ])
      </div>
    </div>
  </div>
@endsection
