@extends(config('layout.default'))

@section('content')

<div class="container">
  <div class="row">
    <div class="col-md-3">
      <div class="row">
        <div class="col-md-12">
          <h3>&nbsp;</h3>
        </div>
      </div> <hr />
      @include('store::shared/_navigation_user')
    </div>
    <div class="col-md-9">
      <div class="row">
        <div class="col-md-9">
          <h3>meus endereços</h3>
        </div>
        <div class="col-md-3 text-right">
        </div>
      </div>

      <hr />

      @if (!$addresses->isEmpty())
        <div class="row">
          @foreach ($addresses as $key => $address)
            @include('store::addresses/_address', [ 'address' => $address ])
          @endforeach

          <div class="col-md-4">
            <a style="font-size:27px;padding:35px 0px" href="{{ Module::action('Store', 'AddressesController@create') }}" class="btn btn-block btn-primary">
              <i class="fa fa-fw fa-4x fa-plus"></i> <br />
              <span style="font-size:27px;">Adicionar endereço</span>
            </a>
          </div>
        @else
          <div class="alert alert-info">
            <b>Atenção!</b> Nenhum pedido foi encontrado
          </div>
        </div>
      @endif
    </div>
  </div>
</div>

@endsection
