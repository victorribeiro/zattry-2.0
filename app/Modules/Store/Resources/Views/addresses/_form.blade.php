{!! Form::model($address, $form_attributes) !!}
<div class="form-group">
  <div class="row">
    <div class="col-sm-7">
      {!! Form::label('address[name]', 'Identificador do endereço *') !!}
      {!! Form::text('address[name]', $address->name, [ 'class' => 'form-control', 'placeholder' => 'Casa, Apartament, Trabalho...' ]) !!}
    </div>

    <div class="col-sm-5">
      {!! Form::label('address[receiver]', 'Nome do destinatário *') !!}
      {!! Form::text('address[receiver]', \Auth::user()->name, [ 'class' => 'form-control', 'placeholder' => 'Nome da pessoa que vai receber' ]) !!}
    </div>
  </div>
</div>

<div class="form-group">
  <div class="row">
    <div class="col-sm-3">
      {!! Form::label('address[cep]', 'CEP *') !!}
      {!! Form::text('address[cep]', $address->cep, [ 'id' => 'address-cep', 'data-prefix' => 'address', 'class' => 'form-control', 'placeholder' => '54759-195' ]) !!}
    </div>

    <div class="col-sm-12">
      <span id="correios-message" class="text-danger"></span>
    </div>
  </div>
</div>

<div class="form-group">
  <div class="row">
    <div class="col-sm-9">
      {!! Form::label('address[street]', 'Endereço *') !!}
      {!! Form::text('address[street]', $address->street, [ 'id' => 'address-street', 'class' => 'form-control', 'placeholder' => 'Rua, Av, etc...' ]) !!}
    </div>

    <div class="col-sm-3">
      {!! Form::label('address[number]', 'Número') !!}
      {!! Form::text('address[number]', $address->number, [ 'class' => 'form-control', 'placeholder' => '999' ]) !!}
    </div>
  </div>
</div>

<div class="form-group">
  <div class="row">
    <div class="col-sm-4">
      {!! Form::label('address[complement]', 'Complemento') !!}
      {!! Form::textarea('address[complement]', $address->complement, [ 'class' => 'form-control', 'rows' => 2, 'placeholder' => 'Apartamento, Comercial (Opcional)' ]) !!}
    </div>

    <div class="col-sm-8">
      {!! Form::label('address[reference]', 'Informações de referência *') !!}
      {!! Form::textarea('address[reference]', $address->reference, [ 'class' => 'form-control', 'rows' => 2, 'placeholder' => 'Descreva como chegar ao destino' ]) !!}
    </div>
  </div>
</div>

<div class="form-group">
  <div class="row">
    <div class="col-sm-4">
      {!! Form::label('address[neighborhood]', 'Bairro *') !!}
      {!! Form::text('address[neighborhood]', $address->neighborhood, [ 'id' => 'address-neighborhood', 'class' => 'form-control', 'placeholder' => 'Ex:. Boa Viagem' ]) !!}
    </div>

    <div class="col-sm-6">
      {!! Form::label('address[city]', 'Cidade *') !!}
      {!! Form::text('address[city]', $address->city, [ 'id' => 'address-city', 'class' => 'form-control', 'placeholder' => 'Ex:. Recife' ]) !!}
    </div>

    <div class="col-sm-2">
      {!! Form::label('address[state]', 'Estado *') !!}
      {!! Form::text('address[state]', $address->state, [ 'id' => 'address-state', 'class' => 'form-control', 'placeholder' => 'Ex:. PE' ]) !!}
    </div>
  </div>
</div>

<hr />

<div class="row">
  <div class="col-sm-12">
    <button class="btn btn-block btn-primary">
      <i class="fa fa-home fa-fw"></i>Salvar endereço
    </button>
  </div>
</div>
{!! Form::close() !!}
