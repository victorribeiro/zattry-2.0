<div class="col-md-4">
  <div class="panel-group">
    <div class="panel panel-default">
      <div class="col-xs-12">
        <h3>{{ $address->name }} <small>{{ $address->receiver }}</small></h3>
        <p>{{ $address->street }}, {{ $address->number }}{{ $address->complement ? ' - ' . $address->complement : null }}</p>
        <p>{{ $address->cep }} - {{ $address->neighborhood }}</p>
        <p>{{ $address->city }}/{{ $address->state }}</p>
        {{-- <p><small>{{ $address->reference }}</small></p> --}}
        <hr style="margin:17px 0" />
        <p class="text-center">
          {{-- <span class="btn-group" data-toggle="buttons">
            <label class="btn btn-sm btn-primary">
              <input type="checkbox" name="freight" value="cd" />
              <i class="fa fa-check"></i> principal
            </label>
          </span> --}}

          <span class="btn-group">
            <a href="{{ Module::action('Store', 'AddressesController@edit', $address->code) }}" class="btn btn-sm btn-success">
              <i class="fa fa-pencil"></i> editar
            </a>
            <span class="btn btn-sm btn-danger" data-toggle="modal" data-target="#address_destroy_{{ $address->code }}">
              <i class="fa fa-times"></i> apagar
            </span>

            @include('admin/shared/_destroy_confirm', [ 'target_modal_id' => 'address_destroy_' . $address->code, 'form_action' => Module::action('Store', 'AddressesController@destroy', $address->code) ])
          </span>
        </p>
      </div>
      <div class="row"></div>
    </div>
  </div>
</div>

@if (($key+1) % 3 == 0)
  {!! '</div><div class="row">' !!}
@endif
