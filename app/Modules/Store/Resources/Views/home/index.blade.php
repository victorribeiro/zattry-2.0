@extends(config('layout.default'))

@section('content')

<div class="container">
  <div class="row">
    <div class="col-md-3">
      <div class="row">
        <div class="col-md-12">
          <h3>&nbsp;</h3>
        </div>
      </div> <hr />
      @include('store::shared/_navigation_user')
    </div>
    <div class="col-md-9">
      <div class="row">
        <div class="col-md-9">
          <h3>pedidos mais recentes</h3>
        </div>
        <div class="col-md-3 text-right"><br>
          <a href="{{ Module::action('Store', 'OrdersController@index') }}" class="btn btn-primary">
            <i class="fa fa-fw fa-eye"></i> Ver todos pedidos
          </a>
        </div>
      </div>

      <hr />

      <div class="row">
        <div class="col-md-12">
          @if (!$orders->isEmpty())
            @foreach ($orders as $key => $order)
              @include('store::orders/_order', [ 'order' => $order ])
            @endforeach
          @else
            <div class="alert alert-info">
              <b>Atenção!</b> Nenhum pedido foi encontrado
            </div>
          @endif
        </div>
      </div>

      <div class="row">
        <div class="col-md-9">
          <h3>meus endereços</h3>
        </div>
        <div class="col-md-3 text-right"><br>
          <a href="{{ Module::action('Store', 'AddressesController@create') }}" class="btn btn-primary">
            <i class="fa fa-fw fa-plus"></i> Adicionar endereço
          </a>
        </div>
      </div>

      <hr />

      @if (!$addresses->isEmpty())
        <div class="row">
          @foreach ($addresses as $key => $address)
            @include('store::addresses/_address', [ 'address' => $address ])
          @endforeach
        @else
          <div class="alert alert-info">
            <b>Atenção!</b> Nenhum pedido foi encontrado
          </div>
        </div>
      @endif
    </div>
  </div>
</div>

@endsection
