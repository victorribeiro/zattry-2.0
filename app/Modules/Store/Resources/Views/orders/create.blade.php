@extends(config('layout.default'))

@section('content')
<div class="container">
  <h3><span class="fa fa-shopping-cart"></span> Finalizar pedido</h3> <hr />

  @if (session()->get('activation.monthly'))
    @if (Cart::subtotal( $cart_stocks ) < 90)
      <?php $confimation = 'data-confirm="Para validar sua \'Ativação Mensal\' é preciso fazer uma compra de no mínimo R$90,00. Deseja continuar?"'; ?>
      <div class="alert alert-info">
        <strong>Atenção! </strong> Sua "Ativação Mensal" não poderar ser validada, é preciso fazer uma compra de no mínimo R$ 90,00.
      </div>
    @endif
  @endif

  @if (!$cart_stocks->isEmpty())
    {!! Form::model($order, [ 'id' => 'form-store-order', 'url' => Module::action('Store', 'OrdersController@store') ]) !!}
      <input type="hidden" id="checkout-total" value="{{ \Cart::total() }}" />
      <input type="hidden" id="checkout-subtotal" value="{{ Cart::subtotal($cart_stocks) }}" />
      <div class="row">
        <div class="col-md-6">
          @include('store::orders/_resume')
        </div>

        <div class="col-md-6">
          @include('store::orders/_delivery')
        </div>
      </div>

      <div class="row">
        <div class="col-xs-12">
          @include('payments::shared/_payment')
        </div>
      </div>

      <div class="row">
        <div class="col-xs-12">
          <button class="btn btn-lg btn-block btn-primary" {!! $confimation or '' !!}>
            <i class="fa fa-fw fa-credit-card"></i> Pagar com MaxiPago
          </button>
        </div>
      </div>
    {!! Form::close() !!}
  @endif
</div>

@endsection
