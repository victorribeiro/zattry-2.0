@extends(config('layout.default'))

@section('content')

<div class="container">
  <div class="row">
    <div class="col-md-3">
      <h3>&nbsp;</h3> <hr />
      @include('store::shared/_navigation_user')
    </div>
    <div class="col-md-9">
      <h3>&nbsp;</h3> <hr />

      <div class="row">
        <div class="col-md-12">
          @include('store::orders/_order', [ 'order' => $order, 'open' => 'in' ])
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
