<div class="panel panel-default">
  <div class="panel-heading">
    <div class="panel-title">
      <div class="row">
        <div class="col-xs-6">
          <h4 style="margin:0">Endereço de entrega</h4>
        </div>

        <div class="col-xs-6 text-right">
          <a href="{{ Module::action('Store', 'AddressesController@create') }}" class="btn btn-xs btn-primary">
            <i class="fa fa-plus"></i> Adicionar um endereço
          </a>
        </div>
      </div>
    </div>
  </div>
  <div id="panel-order-address" class="panel-body">
    <div class="row">
      <div class="col-md-12">
        {!! Form::radio('address_id', 0, false, [ 'id' => 'address_id_0', 'data-cep' => 0 ]) !!}
        <label for="address_id_0" class="checkbox-inline">
          Retirar no CD
        </label>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        @if ($logged->addresses->isEmpty())
          <div class="alert alert-info">
            <b>Atenção! </b>Você não cadastrou nenhum endereço
          </div>
        @else
          @foreach ($logged->addresses as $key => $address)
            <div class="row">
              <hr />
              <div class="col-md-12">
                {!! Form::radio('address_id', $address->id, false, [ 'id' => 'address_id_' . $address->id, 'data-cep' => $address->cep ]) !!}
                <label for="address_id_{{ $address->id }}" class="checkbox-inline">
                  {{ $address->name }} - {{ $address->cep }}<br />
                  <small>
                    {{ $address->street }}, {{ $address->number }} -
                    {{ $address->neighborhood }} <br />
                    {{ $address->city }} - {{ $address->state }}
                  </small>
                </label>
              </div>
            </div>
          @endforeach
        @endif
      </div>
    </div>
  </div>
</div>
