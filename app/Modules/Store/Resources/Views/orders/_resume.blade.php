<div class="panel panel-default">
  <div class="panel-heading">
    <div class="panel-title">
      <div class="row">
        <div class="col-xs-12">
          <h4 style="margin:0">Resumo do pedido</h4>
        </div>
      </div>
    </div>
  </div>
  <div class="panel-body">
    @foreach ($cart_stocks as $key => $stock)
      <div class="row">
        <div class="col-xs-8">
          {{ $stock->quantity() }} -
          {{ $stock->product_name() }}
        </div>
        <div class="col-xs-4 text-right">
          {{ $stock->formatted_total() }}
        </div>
      </div>
    @endforeach
    <div class="row">
      <div class="col-xs-8">
        Frete
      </div>
      <div class="col-xs-4 text-right">
        <span id="checkout-freight">{{ Cart::formatted_freight() }}</span>
      </div>
    </div>
  </div>
  <div class="panel-footer">
    <div class="row">
      <div class="col-xs-8">
        <b>Total a pagar</b>
      </div>
      <div class="col-xs-4 text-right">
        <b id="checkout-amount-total">{{ Cart::formatted_total( $cart_stocks ) }}</b>
      </div>
    </div>
  </div>
</div>
