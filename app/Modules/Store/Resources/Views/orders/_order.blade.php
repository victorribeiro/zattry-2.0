<div class="row">
  <div class="col-md-12">
    <div class="panel-group">
      <div class="panel panel-default">
        <div class="panel-heading" id="category-group-{{ $order->id }}">
          <h4 class="panel-title">
            <span class="row">
              <span class="col-xs-5">
                <small>Cód. Pedido:</small> <br />
                <b>#{{ $order->code }}</b>
              </span>
              <span class="col-xs-3">
                <span class="pull-right">
                  <small>Valor total:</small> <br />
                  <b>{{ $order->formatted_total() }}</b>
                </span>
              </span>
              <span class="col-xs-4">
                <i style="margin-left:17px" title="Detalhes" class="pull-right">
                  <span data-toggle="collapse" data-target="#collapse-category-{{ $order->id }}" class="fa fa-chevron-down fa-2x"></span>
                </i>
                <span title="{{ $order->created_at_datetime() }}" class="pull-right">
                  <small>Realizado em:</small> <br />
                  <b>{{ $order->created_at_date() }}</b>
                </span>
              </span>
            </span>
          </h4>
        </div>
        <div id="collapse-category-{{ $order->id }}" class="panel-collapse collapse {{ $open or '' }}">
          <ul class="list-group">
            <li class="list-group-item">
              <table class="table" style="margin-bottom:0px;">
              <thead>
                <tr>
                  <th>Nome do Produto</th>
                  <th width="150" class="text-right">Preço</th>
                  <th width="150" class="text-right">Subtotal</th>
                </tr>
              </thead>
              <tbody>
                @if (!$order->items->isEmpty())
                  @foreach ($order->items as $key => $item)
                    <tr>
                      <td>{{ $item->quantity }} - {{ $item->stock->product_name() }}</td>
                      <td class="text-right">{{ $item->formatted_amount() }}</td>
                      <td class="text-right">{{ $item->formatted_total() }}</td>
                    </tr>
                  @endforeach
                @endif
                <tr>
                  <td colspan="2" class="text-right"><b>Subtotal</b></td>
                  <td class="text-right">{{ $order->formatted_amount() }}</td>
                </tr>
                <tr>
                  <td colspan="2" class="text-right"><b>Valor do frete</b></td>
                  <td class="text-right">{{ $order->formatted_freight_amount() }}</td>
                </tr>
                <tr>
                  <td colspan="2" class="text-right"><b>Total</b></td>
                  <td class="text-right"><h4><b>{{ $order->formatted_total() }}</b></h4></td>
                </tr>
              </tbody>
              </table>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
