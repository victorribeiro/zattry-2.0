<div class="thumbnail">
  <img class="img-responsive" src="{{ $product->image() }}" alt="{{ $product->name() }}" />
  <h4>{{ $product->name() }}</h4>
  <h5>{{ $product->category_name() }}</h5>
  <p class="row">
    <span class="col-md-12">{{ $product->formatted_discounted_amount() }}</span>
  </p>
  <a href="{{ $product->url() }}" class="btn btn-xs btn-block btn-default">ver produto</a>
</div>
