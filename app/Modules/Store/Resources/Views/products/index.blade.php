@extends(config('layout.default'))

@section('content')
<div class="container">
  <h3>Produtos</h3>

  <div class="row">
    <div class="col-md-3">
      @if (!$categories->isEmpty())
        @foreach ($categories as $key => $category)
          <h4><a href="?category={{ $category->id }}">{{ $category->name }}</a></h4>
          @foreach ($category->categories as $key => $cat)
            <div class="panel-group">
              <div class="panel panel-default">
                <div class="panel-heading" id="category-group-{{ $cat->id }}">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" href="#collapse-category-{{ $cat->id }}">
                      {{ $cat->name }}
                    </a>
                    <a href="?category={{ $cat->id }}"><i class="fa fa-external-link"></i></a>
                  </h4>
                </div>
                <div id="collapse-category-{{ $cat->id }}" class="panel-collapse collapse in">
                  <ul class="list-group">
                    @foreach ($cat->categories as $key => $c)
                      <li class="list-group-item">
                        <a href="?category={{ $c->id }}">{{ $c->name }}</a>
                      </li>
                    @endforeach
                  </ul>
                </div>
              </div>
            </div>
          @endforeach
        @endforeach
      @endif
    </div>

    <div class="col-md-9">
      @if (!$products->isEmpty())
      <div class="row">
        @foreach ($products as $key => $product)
          <div class="col-md-4 col-md-4">
            @include('store::products/_product', [ 'product' => $product ])
          </div>
          @if (($key+1) % 3 == 0)
            {!! '</div><div class="row">' !!}
          @endif
        @endforeach

        <div class="row">
          <div class="col-md-12 text-right">
            {!! $products->render() !!}
          </div>
        </div>
      </div>
      @else
        <div class="alert alert-info">
          Nenhum produto foi encontrado
        </div>
      @endif
    </div>
  </div>
</div>

@endsection
