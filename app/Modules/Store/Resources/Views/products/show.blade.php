@extends(config('layout.default'))

@section('content')
  <div class="container">
    <h3>{{ $product->name() }}</h3>
    <hr />

    <div class="row">
      <div class="col-md-3">
        <img class="img-responsive" src="{{ $product->image() }}" alt="{{ $product->name() }}" />
      </div>

      <div class="col-md-9">
        {{ $product->description() }}

        <hr />
        {!! Form::open([ 'url' => Module::action('Store', 'CheckoutController@store') ]) !!}
          <div class="row">
            <div class="col-md-4">
              {!! $product->status() !!}
            </div>
            <div class="col-md-4">
              {!! Form::select('size', $sizes, null, [ 'class' => 'form-control input-lg' ]) !!}
            </div>
            <div class="col-md-4">
              {!! Form::select('color', $colors, $product->url(), [ 'id' => 'product_color_code', 'class' => 'form-control input-lg' ]) !!}
            </div>
          </div>
          <hr />
          <div class="row">
            <div class="col-md-6">
              <h3>{{ $product->formatted_discounted_amount() }}</h3>
            </div>
            <div class="col-md-6 text-right">
              {!! Form::submit('Adicionar ao carrinho', [ 'class' => 'btn btn-lg btn-primary' ]) !!}
            </div>
          </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
@endsection
