<div class="row text-center">
  <div class="col-xs-12 text-right">
    <h3 class="pull-right" style="margin:0px">Total <strong id="checkout-total">{{ Cart::formatted_total( $cart_stocks ) }}</strong></h3>
  </div>
</div>
