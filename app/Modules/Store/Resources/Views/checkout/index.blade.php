@extends(config('layout.default'))

@section('content')
<div class="container">
  <h3><span class="fa fa-shopping-cart"></span> Carrinho</h3>

  @if ($cart_stocks->isEmpty())
    <div class="row">
      <div class="col-md-12">
        <div class="alert alert-info">
          Nenhum produto foi adicionado ao carrinho, clique
          <a href="{{ action('HomeController@index') }}" class="text-muted">aqui</a>
          para continuar comprando.
        </div>
      </div>
    </div>
  @else
    <div class="row">
      <div class="col-xs-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <div class="panel-title">
              <div class="row">
                <div class="col-xs-12 text-right">
                  <a href="{{ action('HomeController@index') }}" class="btn btn-primary btn-sm">
                    <i class="fa fa-arrow-left"></i> Continuar comprando
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div class="panel-body">
            @foreach ($cart_stocks as $key => $stock)
              @include('store::checkout/_product', [ 'stock' => $stock ])
            @endforeach

            @include('store::checkout/_subtotal')

            @include('store::checkout/_freight')
          </div>
          <div class="panel-footer">
            @include('store::checkout/_total')
          </div>
        </div>
          <a href="{{ Module::action('Store', 'OrdersController@create') }}" class="btn btn-lg btn-block btn-success">
            Finalizar compra
          </a>
      </div>
    </div>
  @endif
</div>

@endsection
