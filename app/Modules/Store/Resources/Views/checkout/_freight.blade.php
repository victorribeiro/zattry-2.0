<div class="row">
  <div class="col-xs-6">
    <div class="row">
      <div class="col-xs-5">
        <div class="btn-group" data-toggle="buttons">
          <label class="btn btn-default{{ Cart::hasCep() ? '' : ' active' }}">
            <input type="radio" name="freight" value="cd" {{ Cart::hasCep() ? '' : 'checked ' }}/>Retirar no CD
          </label>
          <label class="btn btn-default{{ Cart::hasCep() ? ' active' : '' }}">
            <input type="radio" name="freight" value="pac" {{ Cart::hasCep() ? 'checked ' : '' }}/>PAC
          </label>
        </div>
      </div>

      <div class="col-xs-5">
        <div id="form-freight">
          <div class="input-group">
            <input id="cep" type="text" class="form-control" data-mask="cep" placeholder="54759-195" value="{{ Cart::cep() }}" />
            <span class="input-group-btn">
              <input id="calculate-freight" type="submit" value="Calcular" class="btn btn-primary" />
            </span>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-xs-6 text-right">
    <h4 class="text-right">Frete: <strong id="checkout-freight">{{ Cart::formatted_freight() }}</strong></h4>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <small><em id="checkout-message" class="text-info">
      {{ Cart::formatted_deadline() }}
    </em></small>
  </div>
</div>
