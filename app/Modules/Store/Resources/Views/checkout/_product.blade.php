<div class="row">
  <div class="col-xs-2">
    <img class="img-responsive" src="{{ $stock->product->image() }}" />
  </div>
  <div class="col-xs-4">
    <h4 class="product-name">{{ $stock->product_name() }}</h4>
    <h4><small>{{ $stock->product->description() }}</small></h4>
  </div>
  <div class="col-xs-6">
    <div class="form-group">
      <div class="row">
        <div class="col-xs-offset-7 col-xs-3">
          {!! Form::open([ 'url' => Module::action('Store', 'CheckoutController@update', $stock->id), 'method' => 'PUT' ]) !!}
            <div class="input-group" style="z-index:0;">
              {!! Form::text('quantity', $stock->quantity(), [ 'class' => 'form-control text-center input-sm' ]) !!}
              <span class="input-group-btn" id="basic-addon2">
                <button class="btn btn-success btn-sm">
                  <span class="fa fa-refresh"> </span>
                </button>
              </span>
            </div>
          {!! Form::close() !!}
        </div>
        <div class="col-xs-1 text-right">
          {!! Form::open([ 'url' => Module::action('Store', 'CheckoutController@destroy', $stock->id), 'method' => 'DELETE' ]) !!}
            <button class="btn btn-danger btn-sm">
              <span class="fa fa-trash-o"> </span>
            </button>
          {!! Form::close() !!}
        </div>
      </div>
    </div>
    <div class="form-group">
      <div class="row">
        <div class="col-xs-11 text-right">
          <strong>
            {{ $stock->quantity() }}
            <span class="text-muted">×</span>
            {{ $stock->product->formatted_discounted_amount() }}
            <span class="text-muted">=</span>
            {{ $stock->formatted_total() }}
          </strong>
        </div>
      </div>
    </div>
  </div>
</div>
<hr>
