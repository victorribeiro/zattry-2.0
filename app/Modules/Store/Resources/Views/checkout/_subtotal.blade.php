<div class="row">
  <div class="col-xs-12 text-right">
    {!! Form::hidden(null, Cart::subtotal(), [ 'id' => 'checkout-subtotal' ]) !!}
    <h4 class="text-right">Subtotal: <strong>{{ Cart::formatted_subtotal() }}</strong></h4>
  </div>
</div>
<hr>
