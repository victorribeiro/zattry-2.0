{{-- cart_stocks --}}
@if (isset($cart_stocks))
  @if (!$cart_stocks->isEmpty())
  <li class="dropdown">
    <a href="#" data-toggle="dropdown">
      <i class="label label-info" style="border-radius:1em;">{{ \Cart::count() }}</i>
      <i class="fa fa-shopping-cart"></i>
      <i class="fa fa-angle-down fa-fw"></i>
    </a>
    <ul class="dropdown-menu dropdown-cart">
      @foreach ($cart_stocks as $key => $stock)
        <li>
          <span class="item">
            <span class="item-left">
              <span class="col-xs-3">
                <img height="50" src="{{ $stock->product->image() }}" />
              </span>
              <span class="col-xs-9">
                <span>
                  {{ $stock->quantity() }} -
                  {{ $stock->product_name() }}
                </span> <br />
                <span>{{ $stock->formatted_total() }}</span>
                {!! Form::open([ 'url' => Module::action('Store', 'CheckoutController@destroy', $stock->id), 'method' => 'DELETE' ]) !!}
                <span class="pull-right">
                  <button class="btn btn-xs btn-link">
                    <i class="fa fa-times text-danger"></i>
                  </button>
                </span>
                {!! Form::close() !!}
              </span>
            </span>
          </span>
        </li>
      @endforeach
      <li class="divider"></li>
      <li>
        <a class="text-center" href="{{ Module::action('Store', 'CheckoutController@index') }}">
          Ver carrinho
        </a>
      </li>
    </ul>
  </li>
  @endif
@endif
