<div class="panel-group">
  <div class="panel panel-default">
    <ul class="list-group">
      <li class="list-group-item">
        {{ \Auth::user()->name }}
        <a href="{{ Module::action('Auth', 'SessionsController@logout') }}" class="pull-right">
          <small><i class="fa fa-fw fa-sign-out text-muted"></i>sair</small>
        </a>
      </li>
      <li class="list-group-item">
        <a href="{{ Module::action('Store', 'HomeController@index') }}">
          <i class="fa fa-dashboard fa-fw text-muted"></i> Minha conta
        </a>
      </li>
      <li class="list-group-item">
        <a href="{{ Module::action('Store', 'OrdersController@index') }}">
          <i class="fa fa-shopping-cart fa-fw text-muted"></i> Meus pedidos
        </a>
      </li>
      <li class="list-group-item">
        <a href="{{ Module::action('Store', 'ClientsController@index') }}">
          <i class="fa fa-user fa-fw text-muted"></i> Meus dados cadastrais
        </a>
      </li>
      <li class="list-group-item">
        <a href="{{ Module::action('Auth', 'UsersController@edit') }}">
          <i class="fa fa-key fa-fw text-muted"></i> Meus dados de acesso
        </a>
      </li>
      <li class="list-group-item">
        <a href="{{ Module::action('Store', 'AddressesController@index') }}">
          <i class="fa fa-home fa-fw text-muted"></i> Meus endereços
        </a>
      </li>
      <li class="list-group-item">
        <a href="#">
          <i class="fa fa-credit-card fa-fw text-muted"></i> Meus cartões de crédito
        </a>
      </li>
    </ul>
  </div>
</div>
