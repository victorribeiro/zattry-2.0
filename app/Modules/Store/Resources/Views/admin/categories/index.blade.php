@extends(config('layout.admin'))

@section('content')
  
  @include('store::admin/categories/_page_header', [ 'title' => 'Listagem' ])

  <div class="page-content">
    <div class="panel">
      <div class="panel-body container-fluid">
        @if ($categories->isEmpty())
          <div class="alert alert-info"><b>Atenção!</b> Nenhuma categoria foi encontrada</div>
        @else
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Nome</th>
                <th class="date">Status</th>
                <th class="actions">Ações</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($categories as $key => $category)
                <tr>
                  <td>
                    <h5>{{ $category->name() }}</h5>
                    {{ $category->category_name() }}
                  </td>
                  <td class="date">{!! $category->status() !!}</td>
                  <td class="text-nowrap">
                    <div class="text-right">
                      <a href="{{ Module::action('Store', 'Admin\CategoriesController@edit', $category->id) }}" class="btn btn-sm btn-icon btn-flat btn-default" data-toggle="tooltip" data-original-title="Editar">
                        <i class="icon wb-pencil"></i>
                      </a>

                      <span class="btn btn-sm btn-icon btn-flat btn-default" data-toggle="modal" data-target="#category_destroy_{{ $category->id }}" data-original-title="Apagar">
                        <i class="icon wb-close"></i>
                      </span>
                    </div>

                    @include('admin/shared/_destroy_confirm', [ 'target_modal_id' => 'category_destroy_' . $category->id, 'form_action' => Module::action('Store', 'Admin\CategoriesController@destroy', $category->id) ])
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>

          @include('admin/shared/_pagination', [ 'collection' => $categories ])
        @endif
      </div>
    </div>
  </div>
@endsection
