@extends(config('layout.admin'))

@section('content')
  @include('store::admin/categories/_page_header', [ 'title' => 'Editar # ' . $category->name ])

  <div class="page-content">
    <div class="panel">
      <div class="panel-body container-fluid">
        @include('store::admin/categories/_form')
      </div>
    </div>
  </div>
@endsection
