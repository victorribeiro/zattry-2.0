{!! Form::model($category, [ 'url' => '/admin/categories/' . $category->id, 'method' => empty($category->id) ? 'POST' : 'PUT' ]) !!}
  <div class="form-group row">
    <div class="col-sm-6">
      {!! Form::label('name', 'Nome') !!}
      {!! Form::text('name', null, [ 'class' => 'form-control' ]) !!}
    </div>

    <div class="col-sm-6">
      {!! Form::label('category_id', 'Categoria pai') !!}
      {!! Form::select('category_id', $categories, null, [ 'class' => 'form-control', 'data-plugin' => 'select2', 'data-placeholder' => 'Selecione categoria (opcional)' ]) !!}
    </div>
  </div>
  <div class="form-group">
    {!! Form::submit('Salvar categoria', [ 'class' => 'btn btn-primary btn-block' ]) !!}
  </div>
{!! Form::close() !!}
