<br />
@if ($kit_costumes->isEmpty())
  <div class="text-muted">Este kit não acompanha nenhum produto</div>
@else
  @foreach ($kit_costumes as $key => $kit_costume)
    <label>{{ $kit_costume->costume_name() }}</label>
    @for ($i = 0; $i < $kit_costume->quantity; $i++)
      <div class="form-group">
        <div class="row">
          <div class="col-sm-12">
            <select name="stocks[]" class="form-control">
              @if (!$kit_costume->stocks->isEmpty())
                @foreach ($kit_costume->stocks as $key => $stock)
                  <option value="{{ $stock->id }}">{{ $stock->product_name() }}</option>
                @endforeach
              @endif
            </select>
          </div>
        </div>
      </div>
    @endfor
  @endforeach
@endif
