@extends(config('layout.default'))

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-3">
      <h3>&nbsp;</h3> <hr />
      @include('store::shared/_navigation_user')
    </div>
    <div class="col-md-9">
      <h3>Meus dados cadastrais</h3> <hr />
      {!! Form::model($client, [ 'url' => Module::action('Store', 'ClientsController@store') ]) !!}
        <div class="form-group">
          <div class="row">
            <div class="col-md-6">
              {!! Form::label('client[document]', 'CPF *') !!}
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="fa fa-file-text-o"></i>
                </span>
                {!! Form::text('client[document]', $client->document, [ 'class' => 'form-control', 'data-mask' => 'cpf', 'placeholder' => 'CPF: 000.000.000-00', 'autofocus' => '' ]) !!}
              </div>
            </div>

            <div class="col-md-6">
              {!! Form::label('client[born_in]', 'Data Nascimento *') !!}
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </span>
                {!! Form::text('client[born_in]', $client->born_in_date(), [ 'class' => 'form-control bs-datepicker', 'data-mask' => 'date', 'placeholder' => 'Nascimento: 29/04/1984' ]) !!}
              </div>
            </div>
          </div>
        </div>

        <div class="form-group">
          <div class="row">
            <div class="col-md-6">
              {!! Form::label('client[phone]', 'Telefone de contato *') !!}
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="fa fa-phone"></i>
                </span>
                {!! Form::text('client[phone]', $client->phone, [ 'class' => 'form-control', 'data-mask' => 'phone-br', 'placeholder' => 'Telefone de contato' ]) !!}
              </div>
            </div>

            <div class="col-md-6">
              {!! Form::label('client[sex]', 'Sexo') !!}

              <div class="btn-group input-group" data-toggle="buttons">
                <span class="input-group-addon">
                  <i class="fa fa-venus-mars"></i>
                </span>
                <label class="btn btn-default{{ $client->sex == 'masculino' ? ' active' : null }}">
                  <input type="radio" name="client[sex]" value="masculino"{{ $client->sex == 'masculino' ? ' checked' : null }} />
                  <i class="fa fa-male"></i> Masculino
                </label>
                <label class="btn btn-default{{ $client->sex == 'feminino' ? ' active' : null }}">
                  <input type="radio" name="client[sex]" value="feminino"{{ $client->sex == 'feminino' ? ' checked' : null }} />
                  <i class="fa fa-female"></i> Feminino
                </label>
              </div>
            </div>
          </div>
        </div>

        <div class="form-group">
          <div class="row">
            <div class="col-md-12">
              <input id="client_agree" name="client[agree]" type="checkbox" {{ $client->agree ? 'checked' : null }} >
              <label for="client_agree" class="checkbox-inline">Eu <b>{{ \Auth::user()->name }}</b>, declaro que li e aceito todos os termos de uso constantes no</label>

              <a href="#" data-toggle="modal" data-target="#contract-modal">Contrato de Adesão</a>

              <div class="modal fade" id="contract-modal">
                <div class="modal-dialog modal-lg" style="height:100%">
                  <div class="modal-content" style="height:90%">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">
                        <i class="fa fa-times"></i>
                      </button>
                      <h4 class="modal-title">
                        Contrato de Adesão
                        <small>
                          <a href="{{ asset('downloads/contrato.pdf') }}" target="blank">
                            <i class="fa fa-fw fa-download text-muted"></i>Baixar Contrato de Adesão
                          </a>
                        </small>
                      </h4>
                    </div>
                    <div class="modal-body" style="height:90%">
                      <iframe width="100%" height="100%" border="0" frameborder="0" scrolling="no" allowtransparency="true" src="{{ asset('downloads/contrato.pdf') }}"></iframe>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <hr />

        <button class="btn btn-block btn-primary" type="submit">
          <i class="fa fa-fw fa-client"></i> Atualizar meus dados cadastrais
        </button>
      {!! Form::close() !!}
    </div>
  </div>
</div>
@endsection
