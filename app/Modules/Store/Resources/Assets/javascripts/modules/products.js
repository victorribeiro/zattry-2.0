var Product = {
  init: function () {
    this.redirectToShow();
  },

  redirectToShow: function () {
    $('#product_color_code').on('change', function () {
      location.href = $(this).val();
    })
  }
};

$(document).on('ready', function () { Product.init() })
