var Checkout = {
  init: function () {
    this.chooseFreight();
    this.calculateFreight();
  },

  chooseFreight: function () {
    var inputFreight = $('input[name="freight"]');

    Checkout.showFormFreight( $('input[name="freight"]:checked').val() );

    inputFreight.on('change', function () {
      Checkout.showFormFreight( $(this).val() );
    });
  },

  calculateFreight: function () {
    var cep = $('#cep');
    var button = $('#calculate-freight');

    button.on('click', function () {
      if (cep.val() != '') {
        Application.showLoading();

        $.ajax({
          url: '/correios/freight/' + cep.val(),
          success: function (response) {
            var subtotal = parseFloat( $('#checkout-subtotal').val() );
            if (response.success) {
              $('#checkout-freight').text(  'R$ ' + number_format(response.price, 2, ',', '') );
              $('#checkout-message').text( 'Seu pedido será entregue em até ' + response.deadline + ' após a aprovação do pagamento' );
              total = response.price + subtotal;
            }
            else {
              $('#checkout-freight').text( 'R$ 0,00' );
              $('#checkout-message').text( response.message );
              total = subtotal;
            };

            $('#checkout-total').text( 'R$ ' + number_format(total, 2, ',', '') );
          }
        })
        .always(function () {
          Application.hideLoading()
        });
      };
    });
  },

  showFormFreight: function (freightVal) {
    if (freightVal == 'pac') {
      $('#form-freight').show();
    }
    else {
      $('#form-freight').hide();
    };
  }
};

$(document).on('ready', function () { Checkout.init() })
