var Order = {
  init: function () {
    this.chooseFreight();
    this.chooseInstallment();
    this.submitStoreOrder();

    this.calculateInstallments( $('#checkout-total').val() );
  },

  chooseFreight: function () {
    $('#panel-order-address').find('[name="address_id"]').on('change', function () {
      var cep = $(this).data('cep');

      Application.showLoading();
      $.ajax({
        url: '/correios/freight/' + cep,
        success: function (response) {
          var subtotal = parseFloat( $('#checkout-subtotal').val() );
          if (response.success) {
            $('#checkout-freight').text( 'R$ ' + number_format(response.price, 2, ',', '') );
            total = response.price + subtotal;
          }
          else {
            $('#checkout-freight').text( 'R$ 0,00' );
            total = subtotal;
          };

          $('#checkout-amount-total').text( 'R$ ' + number_format(total, 2, ',', '') );

          Order.calculateInstallments(total);
        }
      })
      .always(function () {
        Application.hideLoading()
      });
    });
  },

  chooseInstallment: function () {
    $('#select-installments').on('change', function () {
      total = parseFloat( $(this).find('option:selected').data('total') );
      $('#checkout-amount-total').text( 'R$ ' + number_format(total, 2, ',', '') );
    });
  },

  calculateInstallments: function (total) {
    if (total > 0) {
      $.ajax({
        url: '/maxipago/installments/' + total,
        success: function (installments) {
          var options = '';
          var selectInstallments = $('#select-installments');
          $.each(installments, function(i, installment) {
            amount = number_format(installment.amount, 2, ',', '');
            text = installment.quantity + ' x ' + amount + ' = R$ ' + number_format(installment.total, 2, ',', '');
            selected = selectInstallments.data('quantity') == installment.quantity ? ' selected' : '';
            options += '<option value="' + installment.quantity + '" data-total="' + installment.total +'"' + selected + '>' + text + '</option>';
          });

          selectInstallments.html(options).trigger('change');
        }
      });
    };
  },

  submitStoreOrder: function () {
    $('#form-store-order').on('submit', function () {
      installment = $('#select-installments').find('option:selected').text();
      return confirm('Sua compra será paga em: ' + installment + "\n" + 'Deseja realmente continuar?')
    });
  }
};

$(document).on('ready', function () { Order.init() })
