<?php

namespace App\Modules\Store\Models;

use App\Models\BaseModel;

class Product extends BaseModel
{
    use Decorators\ProductDecorator;
    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $fillable = [];

    public function costume()
    {
        return $this->belongsTo('App\Modules\Store\Models\Costume');
    }

    public function color()
    {
        return $this->belongsTo('App\Modules\Store\Models\Color');
    }

    public function stocks()
    {
        return $this->hasMany('App\Modules\Store\Models\Stock');
    }

    public function sizes()
    {
        return $this->belongsToMany('App\Modules\Store\Models\Size', 'stocks');
    }

    public function scopeHighlighted($query)
    {
        return $query->where('highlight', 1);
    }
}
