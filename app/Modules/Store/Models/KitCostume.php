<?php

namespace App\Modules\Store\Models;

use App\Models\BaseModel;

class KitCostume extends BaseModel
{
    use Decorators\KitCostumeDecorator;

    public function kit()
    {
        return $this->belongsTo('App\Modules\Store\Models\Kit');
    }

    public function costume()
    {
        return $this->belongsTo('App\Modules\Store\Models\Costume');
    }
}
