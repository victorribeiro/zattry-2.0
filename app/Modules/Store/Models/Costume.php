<?php

namespace App\Modules\Store\Models;

use App\Models\BaseModel;

class Costume extends BaseModel
{
    use Decorators\CostumeDecorator;

    public function category()
    {
        return $this->belongsTo('App\Modules\Store\Models\Category');
    }

    public function sizes()
    {
        return $this->belongsToMany('App\Modules\Store\Models\Size');
    }

    public function colors()
    {
        return $this->belongsToMany('App\Modules\Store\Models\Color');
    }

    public function products()
    {
        return $this->hasMany('App\Modules\Store\Models\Product');
    }
}
