<?php

namespace App\Modules\Store\Models;

use App\Models\BaseModel;

class Address extends BaseModel
{
    use Decorators\AddressDecorator;
    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $fillable = [
        'name', 'receiver', 'cep', 'street', 'number',
        'complement', 'reference', 'neighborhood', 'city', 'state'
    ];
}
