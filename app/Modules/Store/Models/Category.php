<?php

namespace App\Modules\Store\Models;

use App\Models\BaseModel;

class Category extends BaseModel
{
    use Decorators\CategoryDecorator;
    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $fillable = [ 'name', 'category_id' ];

    public function category()
    {
        return $this->belongsTo('App\Modules\Store\Models\Category');
    }

    public function categories()
    {
        return $this->hasMany('App\Modules\Store\Models\Category');
    }
}
