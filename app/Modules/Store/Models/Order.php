<?php

namespace App\Modules\Store\Models;

use App\Models\BaseModel;

class Order extends BaseModel
{
    use Decorators\OrderDecorator;
    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $fillable = [ 'name', 'category_id' ];
    protected $casts = [ 'raised_points' => 'bool', 'raised_moves' => 'bool' ];

    public function items()
    {
        return $this->hasMany('App\Modules\Store\Models\Item');
    }

    public function payment()
    {
        return $this->belongsTo('App\Modules\Payments\Models\Payment');
    }
}
