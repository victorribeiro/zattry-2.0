<?php

namespace App\Modules\Store\Models;

use App\Models\BaseModel;

class Client extends BaseModel
{
    use Decorators\ClientDecorator;
    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $dates = [ 'born_in' ];
    protected $fillable = [ 'document', 'born_in', 'phone', 'agree', 'sex' ];
}
