<?php

namespace App\Modules\Store\Models\Decorators;

trait StockDecorator
{
    public function product_name()
    {
        if (!is_null($this->product)) {
            $size = is_null($this->size) ? null : ' - ' . $this->size->name;
            return $this->product->name() . $size;
        }
    }

    public function quantity()
    {
        return session()->get('cart.stocks.' . $this->id);
    }

    public function total()
    {
        return $this->quantity() * $this->product->discounted_amount();
    }

    public function formatted_total()
    {
        return \Number::format($this->total());
    }
}
