<?php

namespace App\Modules\Store\Models\Decorators;

trait ClientDecorator
{
    use \App\Models\Decorators\BaseDecorator;

    public function born_in_date()
    {
        return $this->to_date('born_in');
    }
}
