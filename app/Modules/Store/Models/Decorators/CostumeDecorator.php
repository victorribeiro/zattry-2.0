<?php

namespace App\Modules\Store\Models\Decorators;

trait CostumeDecorator
{
    public function name()
    {
        return $this->name;
    }

    public function category_name()
    {
        return is_null($this->category) ? null : $this->category->name;
    }
}
