<?php

namespace App\Modules\Store\Models\Decorators;

trait AddressDecorator
{
    public function formatted_number()
    {
        return empty($this->number) ? 'S\N' : $this->number;
    }

    public function freight()
    {
        return $this->get_freight();
    }

    public function formatted_freight()
    {
        return \Number::format( $this->get_freight() );
    }

    protected function get_freight()
    {
        if (is_null($this->freight_price)) {
            $freight = \Correios::freight($this->cep);
            if ($freight->success) {
                $this->freight_price = $freight->price;
            }
        }
        return $this->freight_price;
    }
}
