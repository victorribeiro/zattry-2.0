<?php

namespace App\Modules\Store\Models\Decorators;

use App\Modules\Payments\Services\MaxiPagoService;

trait OrderDecorator
{
    public function maxipago_total()
    {
        return MaxiPagoService::calculateInstallment($this->total(), $this->installments) * $this->installments;
    }

    public function handle_type()
    {
        return $this->amount >= 90 && session()->get('activation.monthly') ? 'ativacao_mensal' : 'venda_direta';
    }

    public function total()
    {
        return $this->amount + $this->freight_amount;
    }

    public function formatted_total()
    {
        return \Number::format($this->total());
    }

    public function formatted_amount()
    {
        return \Number::format($this->amount);
    }

    public function formatted_freight_amount()
    {
        return \Number::format($this->freight_amount);
    }
}
