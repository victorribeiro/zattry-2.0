<?php

namespace App\Modules\Store\Models\Decorators;

trait ProductDecorator
{
    public function name()
    {
        $name = is_null($this->costume) ? $this->name : $this->costume->name;
        $color = is_null($this->color) ? '' : ' - ' . $this->color->name;

        return $name . $color;
    }

    public function status()
    {
        $total = $this->stocks()->sum('quantity');

        if ($total > 0) {
            return '<div class="text-success">
              <i class="fa fa-check"></i> EM ESTOQUE
            </div>';
        }
        else {
            return '<div class="text-danger">
              <i class="fa fa-times"></i> INDISPONÍVEL
            </div>';
        }
    }

    public function description()
    {
        return is_null($this->costume) ? $this->description : $this->costume->description;
    }

    public function sluged_name()
    {
        return \Illuminate\Support\Str::slug( $this->name() );
    }

    public function url()
    {
        return \Module::action('Store', 'ProductsController@show', [ $this->code, $this->sluged_name() ]);
    }

    public function image()
    {
        $folder = '/uploads/products/';

        if (empty($this->image)) {
            return 'http://placehold.it/253x277&text=sem+imagem';
        }
        else {
            return $folder . $this->image;
        }
    }

    public function amount()
    {
        return $this->amount;
    }

    public function formatted_amount()
    {
        return \Number::format($this->amount());
    }

    public function discounted_amount()
    {
        return $this->amount() - $this->discount_amount();
    }

    public function formatted_discounted_amount()
    {
        return \Number::format($this->discounted_amount());
    }

    public function discount_amount()
    {
        $discount = 0;

        if (\Auth::check()) {
            $profile_id = \Auth::user()->profile_id;

            if ($profile_id == 2) {
                $discount = $this->amount * $this->discount / 100;
            };
        };

        return $discount;
    }

    public function category_name()
    {
        return is_null($this->costume) ? null : $this->costume->category_name();
    }
}
