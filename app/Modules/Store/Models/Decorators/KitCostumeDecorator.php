<?php

namespace App\Modules\Store\Models\Decorators;

trait KitCostumeDecorator
{
    use \App\Models\Decorators\BaseDecorator;

    public function costume_name()
    {
        return is_null($this->costume) ? null : $this->costume->name;
    }
}
