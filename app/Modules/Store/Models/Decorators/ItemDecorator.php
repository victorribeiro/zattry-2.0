<?php

namespace App\Modules\Store\Models\Decorators;

trait ItemDecorator
{
    public function total()
    {
        return $this->quantity * $this->amount;
    }

    public function formatted_total()
    {
        return \Number::format($this->total());
    }

    public function formatted_amount()
    {
        return \Number::format($this->amount);
    }

    public function formatted_freight_amount()
    {
        return \Number::format($this->freight_amount);
    }
}
