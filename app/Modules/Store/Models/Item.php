<?php

namespace App\Modules\Store\Models;

use App\Models\BaseModel;

class Item extends BaseModel
{
    use Decorators\ItemDecorator;
    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $fillable = [ 'stock_id', 'quantity', 'amount' ];

    public function stock()
    {
        return $this->belongsTo('App\Modules\Store\Models\Stock');
    }
}
