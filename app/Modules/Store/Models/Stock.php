<?php

namespace App\Modules\Store\Models;

use App\Models\BaseModel;

class Stock extends BaseModel
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use Decorators\StockDecorator;

    public function size()
    {
        return $this->belongsTo('App\Modules\Store\Models\Size');
    }

    public function product()
    {
        return $this->belongsTo('App\Modules\Store\Models\Product');
    }

    public function office()
    {
        return $this->belongsTo('App\Modules\Store\Models\Office');
    }

    public function scopeAvailable($query)
    {
        return $query->where('quantity', '>', 0);
    }
}
