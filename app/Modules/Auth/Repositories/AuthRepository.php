<?php

namespace App\Modules\Auth\Repositories;

use App\Repositories\BaseRepository;

use App\User;

class AuthRepository
# extends BaseRepository
{
    public $name = 'conta';
    public $gender = 'a';

    const SUCCESS_MESSAGE = 'Sua conta foi criado com sucesso, e um email foi enviado para o email cadastrado. Confira seu email e clique no link de confirmação';

    public function __construct(User $eloquent)
    {
        $this->eloquent = $eloquent;
    }

    public function authenticate($credentials)
    {
        $user = $this->eloquent->byLogin($credentials['login'])
            ->whereNotNull('confirmabled_at')->first();

        if ($user) {
            if (\Hash::check($credentials['password'], $user->password)) {
                \Auth::loginUsingId( $user->id );
                return true;
            }
        };
        return false;
    }
}
