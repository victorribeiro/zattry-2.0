<?php

namespace App\Modules\Auth\Repositories;

use App\Repositories\BaseRepository;

use App\User;

class RegistrationRepository extends BaseRepository
{
    public $name = 'conta';
    public $gender = 'a';

    const SUCCESS_MESSAGE = 'Sua conta foi criado com sucesso, e um email foi enviado para o email cadastrado. Confira seu email e clique no link de confirmação';

    public function __construct(User $eloquent)
    {
        $this->eloquent = $eloquent;
    }

    public function beforeStore($attributes)
    {
        $this->eloquent->profile_id = 5;
        $this->eloquent->confirmable_token = str_random(51);

        return $attributes;
    }

    public function afterStore($attributes)
    {
        $user = $this->eloquent->find( $this->eloquent->id );

        return \Mail::send('auth::emails/registrations', [ 'user' => $user ], function ($message) use ($user) {
            $message->to($user->email, $user->name)->subject('Confirmação de cadastro :: Zattry');
        });
    }

    public function byConfirmableToken($token)
    {
        return $this->eloquent->where('confirmable_token', $token)->first();
    }
}
