<?php

namespace App\Modules\Auth\Repositories;

use App\Repositories\BaseRepository;

use App\User;

class UserRepository extends BaseRepository
{
    public $name = 'conta';
    public $gender = 'a';

    const SUCCESS_MESSAGE = 'Sua conta foi criado com sucesso, e um email foi enviado para o email cadastrado. Confira seu email e clique no link de confirmação';

    public function __construct(User $eloquent)
    {
        $this->eloquent = $eloquent;
    }

    public function updateWithPassword($password, $attributes)
    {
        $current_password = sha1($password);

        if (\Hash::check($current_password, \Auth::user()->password)) {
            return parent::update( \Auth::id(), $attributes );
        }
        else {
            $this->message(false, 'A senha atual está incorreta');
        }

        return $this;
    }

    public function beforeUpdate($attributes)
    {
        $user = $attributes['user'];
        if (!empty($attributes['password']['new'])) $user['password'] = bcrypt(sha1($attributes['password']['new']));

        return $user;
    }

    public function afterUpdate($attributes)
    {
        \Auth::loginUsingId( $this->eloquent->id );
    }
}
