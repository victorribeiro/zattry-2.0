<?php

namespace App\Modules\Auth\Repositories;

use App\Repositories\BaseRepository;

use App\User;

class PasswordRepository extends BaseRepository
{
    public $name = 'senha';
    public $gender = 'a';

    public function __construct(User $eloquent)
    {
        $this->eloquent = $eloquent;
    }

    public function updatePassoword($token, $password)
    {
        $user = $this->byConfirmableToken($token);


        if (is_null($user)) {
            $this->message(false, 'O token expirou ou não existe, tente logar ou solicite o reenvio de confirmação no link "Reenviar email de confirmação"');
        }
        else {
            $this->eloquent->confirmable_token = '';
            $this->eloquent->confirmabled_at = date('Y-m-d H:i:s');

            $updated = $this->update($user->id, [ 'password' => bcrypt( sha1($password) ) ]);

            if ($updated->success()) {
                $this->message(true, 'Um email foi enviado para você');
            }
            else {
                $this->message(false, 'Não foi possivel enviar email');
            };
        }

        return $updated;
    }

    public function byConfirmableToken($token)
    {
        return $this->eloquent->where('confirmable_token', $token)->first();
    }

    public function byLogin($login)
    {
        return $this->eloquent->byLogin($login)->first();
    }

    public function resetEmail($user)
    {
        $this->eloquent->confirmable_token = str_random(51);

        $updated = $this->update($user->id, []);

        if ($updated->success()) {
            \Mail::send('auth::emails/reset', [ 'user' => $user ], function ($message) use ($user) {
                $message->to($user->email, $user->name)->subject('Alterar minha senha :: Zattry');
            });

            $this->message(true, 'Um email foi enviado para você');
        }
        else {
            $this->message(false, 'Não foi possivel enviar email');
        };

        return $updated;
    }
}
