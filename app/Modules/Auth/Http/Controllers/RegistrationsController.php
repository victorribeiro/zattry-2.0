<?php

namespace App\Modules\Auth\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Auth\AuthController;

# Requests
use App\Modules\Auth\Http\Requests\RegistrationUserRequest;
use App\Modules\Auth\Repositories\RegistrationRepository;

class RegistrationsController extends AuthController
{
    public function __construct(RegistrationRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    public function create()
    {
        return view('auth::registrations/create');
    }

    public function store(RegistrationUserRequest $request)
    {
        $stored = $this->repository->store( $request->get('user') );

        if ($stored->success()) {
            session()->put('user_id', $this->repository->eloquent->id);
            return \Module::redirectAction('Auth', 'RegistrationsController@success')->with('success', 'Sua conta foi criada com sucesso');
        }
        else {
            return view('auth::registrations/create')->withInput()->withErrors($stored->errors);
        }
    }

    public function success()
    {
        $user_id = session()->get('user_id');

        $user = $this->repository->find($user_id);

        if (is_null($user)) {
            return redirect(action('HomeController@index'));
        }
        else {
            return view('auth::registrations/success', [ 'user' => $user ]);
        }
    }
}
