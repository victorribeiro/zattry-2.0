<?php

namespace App\Modules\Auth\Http\Controllers;

use App\Http\Controllers\BaseController;

# Requests
use App\Modules\Auth\Http\Requests\UserUpdateRequest;
// use App\Modules\Auth\Http\Requests\UserPasswordRequest;
use App\Modules\Auth\Repositories\UserRepository;

class UsersController extends BaseController
{
    public function __construct(UserRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    public function edit()
    {
        $user = \Auth::user();
        return view('auth::users/edit', [ 'user' => $user ]);
    }

    public function update(UserUpdateRequest $request)
    {
        $updated = $this->repository->updateWithPassword(  $request->get('current_password'), $request->only('user', 'password') );

        if ($updated->success()) {
            session()->put('user_id', $this->repository->eloquent->id);
            return \Module::redirectAction('Auth', 'UsersController@edit')
                ->with('success', $updated->message);
        }
        else {
            return \Module::redirectAction('Auth', 'UsersController@edit')
                ->withInput()->with('error', $updated->message);
        }
    }
}
