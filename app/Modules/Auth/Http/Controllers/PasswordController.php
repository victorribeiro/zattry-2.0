<?php

namespace App\Modules\Auth\Http\Controllers;

use App\Http\Controllers\Auth\PasswordController as AppPasswordController;

use App\Modules\Auth\Http\Requests\EmailRequest;
use App\Modules\Auth\Http\Requests\PasswordRequest;
use App\Modules\Auth\Repositories\PasswordRepository;

class PasswordController extends AppPasswordController
{
    public function __construct(PasswordRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    public function create($token)
    {
        $user = $this->repository->byConfirmableToken($token);

        if (is_null($user)) {
            return \Module::redirectAction('Auth', 'SessionsController@login')->withErrors('O token expirou ou não existe, tente logar ou solicite o reenvio de confirmação no link "Reenviar email de confirmação"');
        }
        else {
            return view('auth::password/create', [ 'user' => $user, 'token' => $token ]);
        }
    }

    public function update($token, PasswordRequest $request)
    {
        $updated = $this->repository->updatePassoword( $token, $request->input('user.password') );

        if ($updated->success()) {
            \Auth::loginUsingId($updated->eloquent->id);
            return \Module::redirectIntendedAction('Store', 'HomeController@index')->with('success', $updated->message);
        }
        else {
            return \Module::redirectAction('Auth', 'PasswordController@create', $token)->withInput()->withErrors($updated->message);
        }
    }

    public function email()
    {
        return view('auth::password/email');
    }

    public function send_mail(EmailRequest $request)
    {
        $user = $this->repository->byLogin( $request->input('user.login') );

        if (is_null($user)) {
            return \Module::redirectAction('Auth', 'PasswordController@email')
                ->withErrors('Não foi encontrado nenhum usuário para o login "' . $request->input('user.login') . '"');
        }

        $updated = $this->repository->resetEmail( $user );
        if ($updated->success()) {
            return redirect(action('HomeController@index'))->with('success', $updated->message);
        }
        else {
            \Module::redirectAction('Auth', 'PasswordController@email')->withInput()->withErrors($updated->message);
        };
    }
}
