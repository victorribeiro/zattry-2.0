<?php

namespace App\Modules\Auth\Http\Controllers;

# Controllers
use App\Http\Controllers\Controller;
use App\Http\Controllers\Auth\AuthController;

# Repositories
use App\Modules\Auth\Repositories\AuthRepository;

# Models
use App\User;

# Requests
use App\Modules\Auth\Http\Requests\LoginUserRequest;

class SessionsController extends AuthController
{
    public function __construct(AuthRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    public function login()
    {
        return view('auth::sessions/login');
    }

    public function authenticate(LoginUserRequest $request)
    {
        $credentials = [
            'login' => $request->get('login'),
            'password' => sha1( $request->get('password') )
        ];

        if ( $this->repository->authenticate($credentials) ) {
            if (is_null(\Auth::user()->distributor)) {
                return redirect()->intended(\Module::action('Store', 'HomeController@index'));
            }
            else {
                return redirect()->intended(\Module::action('Backoffice', 'HomeController@index'));
            }
        };
        return view('auth::sessions/login')->withErrors('Login e/ou senha estão inválidos ou sua contra não foi confirmada ou está inativa');
    }

    public function logout()
    {
        \Auth::logout();
        return redirect(action('HomeController@index'));
    }
}
