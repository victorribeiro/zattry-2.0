<?php
namespace App\Modules\Auth\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $user_id = \Auth::id();
		return [
			'user.name' => [ 'required' ],
            'user.username' => [ 'required', 'unique:users,username,' . $user_id . ',id' ],
            'user.email' => [ 'required', 'email', 'unique:users,email,' . $user_id . ',id,deleted_at,NULL' ],
            'password.new' => [ 'confirmed', 'min:6' ],
            'current_password' => [ 'required' ],
		];
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}
}
