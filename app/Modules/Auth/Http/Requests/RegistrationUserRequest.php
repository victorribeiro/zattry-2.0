<?php
namespace App\Modules\Auth\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegistrationUserRequest extends FormRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'user.name' => [ 'required' ],
            'user.username' => [ 'required', 'unique:users,username' ],
            'user.email' => [ 'required', 'email', 'unique:users,email,NULL,deleted_at' ],
		];
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}
}
