<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group([ 'prefix' => 'auth' ], function() {
    // Sessions
    Route::get('login', 'SessionsController@login');
    Route::post('login', 'SessionsController@authenticate');
    Route::get('logout', 'SessionsController@logout');

    // Registrations
    Route::get('register', 'RegistrationsController@create');
    Route::post('register', 'RegistrationsController@store');
    Route::get('success', 'RegistrationsController@success');
});

Route::group([ 'prefix' => 'password' ], function() {
    // Password send link request
    Route::get('email', 'PasswordController@email');
    Route::post('email', 'PasswordController@send_mail');

    // Password reset link request
    Route::get('reset/{token}', 'PasswordController@create');
    Route::put('reset/{token}', 'PasswordController@update');
});

Route::group([ 'prefix' => 'users', 'middleware' => 'auth' ], function() {
    Route::get('edit', 'UsersController@edit');
    Route::put('/', 'UsersController@update');
});
