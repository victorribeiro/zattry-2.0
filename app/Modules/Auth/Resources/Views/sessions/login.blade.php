@extends(config('layout.default'))

@section('content')
<div class="container">
  <h2 class="form-signin-heading">Criar conta ou login</h2>
  <div class="row">
    <div class="col-md-5">
      <h3>Novos clientes</h3>
      <p>Ao criar uma conta na nossa loja, você será capaz de se mover através do
        processo de compra mais rápida, armazenar múltiplos endereços de envio,
        ver e rastrear seus pedidos em sua conta e muito mais.</p>
      <hr />
      <a href="{{ Module::action('Auth', 'RegistrationsController@create') }}" class="btn btn-primary btn-lg btn-block">
        <i class="fa fa-user-plus fa-fw"></i>
        Quero criar uma conta
      </a>
    </div>

    <div class="col-md-offset-2 col-md-5">
      {!! Form::open([ 'url' => Module::action('Auth', 'SessionsController@authenticate') ]) !!}
        <h3>Os clientes registrados</h3>
        <div class="form-group">
          <div class="row">
            <div class="col-md-12">
              {!! Form::label('login', 'Login') !!}
              {!! Form::text('login', null, [ 'class' => 'form-control input-lg', 'placeholder' => 'Email ou login', 'required' => '', 'autofocus' => '' ]) !!}
            </div>
          </div>
        </div>

        <div class="form-group">
          <div class="row">
            <div class="col-md-12">
              {!! Form::label('password', 'Senha') !!}
              <div class="input-group">
                {!! Form::password('password', [ 'class' => 'form-control input-lg', 'placeholder' => 'Senha', 'required' => '' ]) !!}
                <span class="input-group-btn">
                  <button class="btn btn-lg btn-primary" type="submit">Entrar</button>
                </span>
              </div>
            </div>
          </div>
        </div>

        @include('auth::shared/_links')
      {!! Form::close() !!}
    </div>
  </div>
</div>
@endsection
