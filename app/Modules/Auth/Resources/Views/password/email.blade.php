@extends(config('layout.default'))

@section('content')
<div class="container">
  <h2>Esqueci a senha</h2>

  <div class="row">
    <div class="col-md-12">
      {!! Form::open([ 'url' => Module::action('Auth', 'PasswordController@send_mail') ]) !!}
        <div class="form-group">
          <div class="row">
            <div class="col-md-12">
              {!! Form::label('user[login]', 'Digite seu email ou nome de usuário') !!}
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="fa fa-at"></i>
                </span>
                {!! Form::text('user[login]', null, [ 'class' => 'form-control input-lg', 'placeholder' => 'Digite seu email ou nome de usuário' ]) !!}
                <span class="input-group-btn">
                  <button class="btn btn-lg btn-primary">Resetar minha senha</button>
                </span>
              </div>
            </div>
          </div>
        </div>

        @include('auth::shared/_links')
      {!! Form::close() !!}
    </div>
  </div>
</div>
@endsection
