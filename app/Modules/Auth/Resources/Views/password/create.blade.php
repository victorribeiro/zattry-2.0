@extends(config('layout.default'))

@section('content')
<div class="container">
  <h2>
    Criar senha <br />
    <small>Olá {{ $user->name }}, crie uma senha para poder ter acesso ao nosso sistema</small>
  </h2>
  <div class="row">
    <div class="col-md-12">
      {!! Form::open([ 'url' => Module::action('Auth', 'PasswordController@update', $token), 'method' => 'put' ]) !!}
        <div class="form-group">
          <div class="row">
            <div class="col-md-6">
              {!! Form::label('user[password]', 'Senha') !!}
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="fa fa-key"></i>
                </span>
                {!! Form::password('user[password]', [ 'class' => 'form-control input-lg', 'placeholder' => 'Senha' ]) !!}
              </div>
            </div>

            <div class="col-md-6">
              {!! Form::label('user[password_confirmation]', 'Confirmação de senha') !!}
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="fa fa-key"></i>
                </span>
                {!! Form::password('user[password_confirmation]', [ 'class' => 'form-control input-lg', 'placeholder' => 'Confirmação de senha' ]) !!}
                <span class="input-group-btn">
                  <button class="btn btn-lg btn-primary" type="submit">Criar conta</button>
                </span>
              </div>
            </div>
          </div>
        </div>

        @include('auth::shared/_links')
      {!! Form::close() !!}
    </div>
  </div>
</div>
@endsection
