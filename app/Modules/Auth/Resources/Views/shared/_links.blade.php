<hr />

<div class="row">
  <div class="col-md-12">
    <ul>
      @if (\Request::path() == 'auth/login')
        <li>
          <i class="fa fa-user-plus fa-fw"></i>
          <a href="{{ Module::action('Auth', 'RegistrationsController@create') }}">Quero criar uma conta</a>
        </li>
      @else
        <li>
          <i class="fa fa-sign-in fa-fw"></i>
          <a href="{{ Module::action('Auth', 'SessionsController@login') }}">Já sou cadastrado</a>
        </li>
      @endif
      <li>
        <i class="fa fa-key fa-fw"></i>
        <a href="{{ url('/password/email') }}">Sou cadastrado, mas esqueci a senha</a>
      </li>
      {{-- <li>
        <i class="fa fa-info fa-fw"></i>
        <a href="{{ url('#resend-email') }}">Reenviar email de confirmação</a>
      </li> --}}
    </ul>
  </div>
</div>
