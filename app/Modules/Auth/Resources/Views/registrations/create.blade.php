@extends(config('layout.default'))

@section('content')
<div class="container">
  <h2 class="form-signin-heading">Formulário de cadastro</h2>
  <div class="row">
    <div class="col-md-12">
      {!! Form::open([ 'url' => Module::action('Auth', 'RegistrationsController@store') ]) !!}
        <div class="form-group">
          <div class="row">
            <div class="col-md-12">
              {!! Form::label('user[name]', 'Nome') !!}
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="fa fa-user"></i>
                </span>
                {!! Form::text('user[name]', null, [ 'class' => 'form-control input-lg', 'placeholder' => 'Nome', 'autofocus' => '' ]) !!}
              </div>
            </div>
          </div>
        </div>

        <div class="form-group">
          <div class="row">
            <div class="col-md-6">
              {!! Form::label('user[username]', 'Nome de usuário') !!}
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="fa fa-at"></i>
                </span>
                {!! Form::text('user[username]', null, [ 'class' => 'form-control input-lg', 'placeholder' => 'Nome de usuário' ]) !!}
              </div>
            </div>

            <div class="col-md-6">
              {!! Form::label('user[email]', 'Email') !!}
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="fa fa-envelope"></i>
                </span>
                {!! Form::text('user[email]', null, [ 'class' => 'form-control input-lg', 'placeholder' => 'Email' ]) !!}
                <span class="input-group-btn">
                  <button class="btn btn-lg btn-primary" type="submit">Criar conta</button>
                </span>
              </div>
            </div>
          </div>
        </div>

        @include('auth::shared/_links')
      {!! Form::close() !!}
    </div>
  </div>
</div>
@endsection
