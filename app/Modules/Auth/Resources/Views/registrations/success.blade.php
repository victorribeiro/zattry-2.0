@extends(config('layout.default'))

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h2>Seu cadastro foi feito com sucesso</h2>

      <h3>Falta pouco... confirme seu email!</h3>

      <h4>Olá {{ $user->name }},</h4>
      <p>Para concluir seu cadastro siga os passos abaixo:</p>

      <h4>1º) Cheque seu email</h4>
      <p>* Enviamos um email para "<b>{{ $user->email }}</b>" com o assunto "<b>Confirmação de cadastro :: Zattry</b>"</p>

      <h4>2º) Confirme seu email</h4>
      <p>* Clique no botão "<b>Confirmar e-mail</b>" ou copie o link que nós o enviamos</p>

      <h4>3º) Crie sua senha</h4>
      <p>* Após o 2º passo, você será redirecionado para uma tela onde você poderar <b>Criar uma senha</b></p>

      <h4>Pronto sua conta estará pronta!</h4>
    </div>
  </div>
</div>
@endsection
