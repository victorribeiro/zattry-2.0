@extends(config('layout.default'))

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-3">
      {{-- <div class="row">
        <div class="col-md-12">
        </div>
      </div> --}}
      <h3>&nbsp;</h3> <hr />
      @include('store::shared/_navigation_user')
    </div>
    <div class="col-md-9">
      <h3>Meus dados de acesso</h3> <hr />
      {!! Form::model($user, [ 'url' => Module::action('Auth', 'UsersController@update'), 'method' => 'PUT' ]) !!}
        <div class="form-group">
          <div class="row">
            <div class="col-md-12">
              {!! Form::label('user[name]', 'Nome') !!}
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="fa fa-user"></i>
                </span>
                {!! Form::text('user[name]', $user->name, [ 'class' => 'form-control', 'placeholder' => 'Nome', 'autofocus' => '' ]) !!}
              </div>
            </div>
          </div>
        </div>

        <div class="form-group">
          <div class="row">
            <div class="col-md-6">
              {!! Form::label('user[username]', 'Nome de usuário') !!}
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="fa fa-at"></i>
                </span>
                {!! Form::text('user[username]', $user->username, [ 'class' => 'form-control', 'placeholder' => 'Nome de usuário' ]) !!}
              </div>
            </div>

            <div class="col-md-6">
              {!! Form::label('user[email]', 'Email') !!}
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="fa fa-envelope"></i>
                </span>
                {!! Form::text('user[email]', $user->email, [ 'class' => 'form-control', 'placeholder' => 'Email' ]) !!}
              </div>
            </div>
          </div>
        </div>

        <hr />

        <div class="form-group">
          <div class="row">
            <div class="col-md-6">
              {!! Form::label('password[new]', 'Nova senha') !!}
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="fa fa-key"></i>
                </span>
                {!! Form::password('password[new]', [ 'class' => 'form-control', 'placeholder' => 'Nova senha' ]) !!}
              </div>
            </div>

            <div class="col-md-6">
              {!! Form::label('password[new_confirmation]', 'Confirmar da nova senha') !!}
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="fa fa-key"></i>
                </span>
                {!! Form::password('password[new_confirmation]', [ 'class' => 'form-control', 'placeholder' => 'Confirmar da nova senha' ]) !!}
              </div>
            </div>
            <div class="help-block col-md-12"><em class="text-info">Deixe em branco se você não quiser alterá-la</em></div>
          </div>
        </div>

        <hr />

        <div class="form-group">
          <div class="row">
            <div class="col-md-12">
              {!! Form::label('current_password', 'Sua senha atual') !!}
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="fa fa-key"></i>
                </span>
                {!! Form::password('current_password', [ 'class' => 'form-control', 'placeholder' => 'Nova senha' ]) !!}
              </div>
            </div>
            <div class="help-block col-md-12"><em class="text-info">Nós precisamos de sua senha atual para confirmar as alterações</em></div>
          </div>
        </div>

        <hr />

        <button class="btn btn-block btn-primary" type="submit">
          <i class="fa fa-fw fa-user"></i> Atualizar meus dados
        </button>
      {!! Form::close() !!}
    </div>
  </div>
</div>
@endsection
