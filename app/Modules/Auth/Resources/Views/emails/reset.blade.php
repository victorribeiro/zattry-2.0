@extends(config('layout.mail'))

@section('content')
<table width="540" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidthinner">
  <tbody>
    <tr>
      <td class="title">
        Olá {{ $user->name }}
      </td>
    </tr>
    <tr>
      <td width="100%" height="10"></td>
    </tr>
    <tr>
      <td class="paragraph">
        Alguém pediu um link para mudar sua senha. Você pode fazer isso através do link abaixo.
      </td>
    </tr>
    <tr>
      <td width="100%" height="25"></td>
    </tr>
    <tr>
      <td>
        <table height="36" align="center" valign="middle" border="0" cellpadding="0" cellspacing="0" class="tablet-button" st-button="edit">
          <tbody>
            <tr>
              <td width="auto" align="center" valign="middle" height="36" style="background-color:{{ config('templates.color.primary') }}; border-top-left-radius:4px; border-bottom-left-radius:4px;border-top-right-radius:4px; border-bottom-right-radius:4px; background-clip: padding-box;font-size:13px; font-family:Helvetica, arial, sans-serif; text-align:center;  color:#ffffff; font-weight: 300;">
                <span style="color:#ffffff;font-weight:300;">
                  <a href="{{ $user->confirmation_url() }}" style="color:#ffffff;text-align:center;text-decoration:none;display:block;padding-left:25px;padding-right:25px;background-clip:padding-box;height:36px;line-height:36px">Alterar minha senha</a>
                </span>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr>
      <td width="100%" height="10"></td>
    </tr>
    <tr>
      <td class="paragraph">
        Caso não consiga copie e cole no seu navegador o link abaixo:
      </td>
    </tr>
    <tr>
      <td class="paragraph">
        <small>{{ $user->confirmation_url() }}</small>
      </td>
    </tr>
    <tr>
      <td width="100%" height="25"></td>
    </tr>
    <tr>
      <td class="paragraph">
        Se você não pediu isso, por favor, ignore este e-mail.
        Sua senha não será alterada até que você acessar o link acima e criar um novo.
      </td>
    </tr>
    <tr>
      <td width="100%" height="25"></td>
    </tr>
  </tbody>
</table>
@endsection
