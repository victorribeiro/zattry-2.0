<?php
namespace App\Modules\Backoffice\Repositories;

use App\Repositories\BaseRepository;
use App\Modules\Backoffice\Models\Kit;

class KitRepository extends BaseRepository
{
    use Finders\KitFinder;
    public $name = 'kit';
    public $gender = 'o';

    public function __construct(Kit $eloquent)
    {
        $this->eloquent = $eloquent;
    }
}
