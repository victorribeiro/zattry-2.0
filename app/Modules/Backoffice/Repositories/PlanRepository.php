<?php
namespace App\Modules\Backoffice\Repositories;

use App\Repositories\BaseRepository;
use App\Modules\Backoffice\Models\Plan;

class PlanRepository extends BaseRepository
{
    use Finders\PlanFinder;
    public $name = 'plan';
    public $gender = 'o';

    public function __construct(Plan $eloquent)
    {
        $this->eloquent = $eloquent;
    }
    
}
