<?php
namespace App\Modules\Backoffice\Repositories;

use App\Repositories\BaseRepository;
use App\Modules\Backoffice\Models\Distributor;
use App\Modules\Backoffice\Models\DistributorConfiguration as Configuration;
use App\User;

class DistributorRepository extends BaseRepository
{
    use Finders\DistributorFinder;
    public $name = 'distributor';
    public $gender = 'o';

    public function __construct(Distributor $eloquent)
    {
        $this->eloquent = $eloquent;
    }

    public function afterStore($attributes)
    {
        if (is_null($this->eloquent->sponsor)) {
            $this->message(false, self::TRY_AGAIN_MESSAGE);
        }
        else {
            $configuration = new Configuration;
            $configuration->distributor_id = $this->eloquent->id;

            if ($this->eloquent->sponsor->configuration->preference_binary == 'balance') {
                $configuration->network_side = $this->eloquent->sponsor->configuration->balance_side == 'left' ? 'right' : 'left';
            }
            else {
                $configuration->network_side = $this->eloquent->sponsor->configuration->preference_binary;
            }

            if (!$configuration->save()) {
                $this->message(false, self::TRY_AGAIN_MESSAGE);
            }

            $user = User::create($attributes);
            
            if ($user) {
                \Mail::send('auth::emails/registrations', [ 'user' => $user ], function ($message) use ($user) {
                    $message->to($user->email, $user->name)->subject('Confirmação de cadastro de Distribuidor :: Zattry');
                });
            }
            
        }
    }
}
