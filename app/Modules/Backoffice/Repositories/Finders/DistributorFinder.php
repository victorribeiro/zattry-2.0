<?php

namespace App\Modules\Backoffice\Repositories\Finders;

trait DistributorFinder
{
    protected function defaultScope()
    {
        return $this->eloquent;
    }

}
