<?php

namespace App\Modules\Backoffice\Repositories\Finders;

trait CareerFinder
{
    protected function defaultScope()
    {
        return $this->eloquent;
    }

}
