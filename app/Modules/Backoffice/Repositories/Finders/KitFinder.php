<?php

namespace App\Modules\Backoffice\Repositories\Finders;

trait KitFinder
{
    protected function defaultScope()
    {
        return $this->eloquent;
    }

}
