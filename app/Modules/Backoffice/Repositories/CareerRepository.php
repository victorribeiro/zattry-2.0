<?php
namespace App\Modules\Backoffice\Repositories;

use App\Repositories\BaseRepository;
use App\Modules\Backoffice\Models\Career;

class CareerRepository extends BaseRepository
{
    use Finders\CareerFinder;
    public $name = 'career';
    public $gender = 'a';

    public function __construct(Career $eloquent)
    {
        $this->eloquent = $eloquent;
    }

    public function beforeSave($attributes)
    {
        $attributes['points'] = \Number::strToInt($attributes['points']);
        $attributes['gain_limit'] = \Number::strToFloat($attributes['gain_limit']);

        return $attributes;
    }
}
