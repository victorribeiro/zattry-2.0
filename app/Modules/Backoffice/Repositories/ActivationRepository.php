<?php
namespace App\Modules\Backoffice\Repositories;

use App\Repositories\BaseRepository;

use App\Modules\Backoffice\Models\Kit;
use App\Modules\Store\Models\Order;
use App\Modules\Store\Models\Stock;
use App\Modules\Store\Models\Item;
use App\Modules\Payments\Models\PaymentStatus;
use App\Modules\Payments\Models\Payment;

use App\Modules\Payments\Services\MaxiPagoService;

class ActivationRepository extends BaseRepository
{
    use Finders\CareerFinder;
    public $name = 'Ativação';
    public $gender = 'a';

    public function __construct(Order $eloquent)
    {
        $this->eloquent = $eloquent;
    }

    public function beforeStore($attributes)
    {
        $this->kit = Kit::find($attributes['order']['kit_id']);

        $this->eloquent->address_id = empty($attributes['order']['address_id']) || $attributes['order']['address_id'] == 0 ? null : $attributes['order']['address_id'];
        $this->eloquent->user_id = \Auth::id();
        $this->eloquent->code = str_random(13);
        $this->eloquent->amount = $this->kit->amount;
        $this->eloquent->type = 'compra_ativacao';
        $this->eloquent->freight_type = \Cart::freight() > 0 ? 'cd' : 'pac';
        $this->eloquent->freight_amount = \Cart::freight();

        return $attributes;
    }

    public function afterStore($attributes)
    {
        $stocks = Stock::whereIn('id', $attributes['stocks'])->get();
        $quantities = array_count_values($attributes['stocks']);

        $items = [];
        foreach ($stocks as $key => $stock) {
            $items[] = new Item([
                'stock_id' => $stock->id,
                'amount' => $stock->product->discounted_amount(),
                'quantity' => $quantities[ $stock->id ],
            ]);
        };

        if ($this->eloquent->items()->saveMany( $items )) {
            $this->maxipago($attributes);
        }
        else {
            $this->message(false, self::TRY_AGAIN_MESSAGE);
        };
    }

    private function maxipago($attributes)
    {
        $this->maxipago = new MaxiPagoService;

        $installments = $attributes['card']['installments'];
        $total = MaxiPagoService::total( $this->kit->amount + \Cart::freight(), $installments);

        $pay_attributes = [
            'processorID' => env('MAXIPAGO_PROCESS_ID'), // REQUIRED - Use '1' for testing. Contact our team for production values //
            'referenceNum' => $this->eloquent->code, // REQUIRED - Merchant internal order number //
            'chargeTotal' => $total, // REQUIRED - Transaction amount in US format //
            'numberOfInstallments' => $installments, // Optional - Number of installments ("parcelas") //
            'number' => str_replace(' ', '', $attributes['card']['card']), // REQUIRED - Full credit card number //
            'expMonth' => $attributes['card']['month'], // REQUIRED - Credit card expiration month //
            'expYear' => $attributes['card']['year'], // REQUIRED - Credit card expiration year //
            'cvvNumber' => $attributes['card']['code'], // RECOMMENDED - Credit card verification code //
            'bname' => $attributes['card']['name'], //RECOMMENDED - Customer name //
            'chargeInterest' => 'N', // Optional - Charge interest flag (Y/N), used with installments ("com" ou "sem" juros) //
            'fraudCheck' => 'N', // Optional - Trigger fraud analysis for the transaction //
        ];

        $this->maxipago->pay($pay_attributes);

        if ($this->maxipago->success()) {
            $status = PaymentStatus::where('gateway', 'maxipago')->where('code', 0)->first();

            $payment = Payment::firstOrNew([ 'order_id' => $this->eloquent->id ]);
            $payment->payment_status_id = $status->id;
            $payment->gateway = 'maxipago';
            $payment->gateway_order_id = $this->maxipago->getOrderID();
            $payment->authorization_code = $this->maxipago->getAuthCode();
            $payment->total = $total;
            $payment->card = $attributes['card']['form'];
            $payment->parcel = MaxiPagoService::calculateInstallment( \Cart::total(), $installments);
            $payment->installments = $installments;

            if ($payment->save()) {
                // TODO: Subi pontos depois da ativação
            };
        };

        $this->message($this->maxipago->success(), $this->maxipago->message());
    }
}
