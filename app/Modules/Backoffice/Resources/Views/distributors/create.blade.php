@extends(config('layout.admin'))

@section('content')
  @include('backoffice::admin/distributors/_page_header', [ 'title' => 'Adicionar' ])

  <div class="page-content">
    <div class="panel">
      <div class="panel-body container-fluid">
        @include('backoffice::admin/distributors/_form', [ 'url' => Module::action('Backoffice', 'DistributorsController@store') ])
      </div>
    </div>
  </div>
@endsection
