@extends(config('layout.admin'))

@section('content')
  <div class="page-content">
    {{-- <div class="panel">
      <div class="panel-body container-fluid">
        <div class="row">
          <div class="col-xs-6 col-sm-3">
            <h3 class="text-right" style="margin:0px;">
              <b><span class="text-info">R$ 0,00</span>
              <small>Saldo</small></b>
            </h3>
            <div class="text-right"><small>atual</small></div>
          </div>

          <div class="col-xs-6 col-sm-3">
            <h3 class="text-right" style="margin:0px;">
              <b><span class="text-info">R$ 0,00</span>
              <small>GANHOS</small></b>
            </h3>
            <div class="text-right"><small>até hoje</small></div>
          </div>

          <div class="col-xs-6 col-sm-2">
            <h3 class="text-right">
              <b><span class="text-info">0</span>
              <small>Direto</small></b>
            </h3>
          </div>

          <div class="col-xs-6 col-sm-2">
            <h3 class="text-right">
              <b><span class="text-info">0</span>
              <small>Rede</small></b>
            </h3>
          </div>
        </div>
      </div>
    </div> --}}

    <div class="panel">
      <div class="panel-body container-fluid">
        <div class="row">
          <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <a href="{{ Module::action('Backoffice', 'ActivationController@monthly') }}" class="btn btn-block btn-primary" target="blank" style="padding:30px 0px">
              <i class="fa fa-4x fa-check"></i> <br />
              Ativação mensal
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
