@extends(config('layout.admin'))

@section('content')
  
  @include('backoffice::admin/careers/_page_header', [ 'title' => 'Listagem' ])

  <div class="page-content">
    <div class="panel">
      <div class="panel-body container-fluid">
        @if ($careers->isEmpty())
          <div class="alert alert-info"><b>Atenção!</b> Nenhuma carreira foi encontrado</div>
        @else
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Nome</th>
                <th>Pontos</th>
                <th>Limite de Ganho</th>
                <th>Volume Pessoal (%)</th>
                <th class="actions">Ações</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($careers as $key => $career)
                <tr>
                  <td>
                    <h5>{{ $career->name() }}</h5>
                  </td>
                  <td>
                    <h5>{{ $career->points }}</h5>
                  </td>
                  <td>
                    <h5>{{ $career->gain_limit() }}</h5>
                  </td>
                  <td class="text-center">
                    <h5>{{ $career->volume }}</h5>
                  </td>
                  <td class="date"></td>
                  <td class="text-nowrap">
                    <div class="text-right">
                      <a href="{{ Module::action('Backoffice', 'Admin\CareersController@edit', $career->id) }}" class="btn btn-sm btn-icon btn-flat btn-default" data-toggle="tooltip" data-original-title="Editar">
                        <i class="icon wb-pencil"></i>
                      </a>

                      <span class="btn btn-sm btn-icon btn-flat btn-default" data-toggle="modal" data-target="#career_destroy_{{ $career->id }}" data-original-title="Apagar">
                        <i class="icon wb-close"></i>
                      </span>
                    </div>

                    @include('admin/shared/_destroy_confirm', [ 'target_modal_id' => 'career_destroy_' . $career->id, 'form_action' => Module::action('Backoffice', 'Admin\CareersController@destroy', $career->id) ])
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>

          @include('admin/shared/_pagination', [ 'collection' => $careers ])
        @endif
      </div>
    </div>
  </div>
@endsection
