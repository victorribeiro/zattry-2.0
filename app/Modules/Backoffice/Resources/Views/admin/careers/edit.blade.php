@extends(config('layout.admin'))

@section('content')
  @include('backoffice::admin/careers/_page_header', [ 'title' => 'Editar # ' . $career->name ])

  <div class="page-content">
    <div class="panel">
      <div class="panel-body container-fluid">
        @include('backoffice::admin/careers/_form')
      </div>
    </div>
  </div>
@endsection
