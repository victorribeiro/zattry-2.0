<div class="panel">
	<div class="panel-body container-fluid">
        {!! Form::model($career, [ 'url' => '/admin/careers/' . $career->id, 'method' => empty($career->id) ? 'POST' : 'PUT' ]) !!}
        <div class="form-group row">
            <div class="col-sm-12">
                {!! Form::label('name', 'Nome') !!}
                {!! Form::text('name', null, [ 'class' => 'form-control' ]) !!}
            </div>
        </div>
        
        <div class="form-group row">
            <div class="col-sm-4">
                {!! Form::label('points', 'Pontos') !!}
                {!! Form::text('points', null, [ 'class' => 'form-control', 'data-mask' => 'integer' ]) !!}
            </div>
        
            <div class="col-sm-4">
                {!! Form::label('gain_limit', 'Limite de Ganho') !!}
                {!! Form::text('gain_limit', null, [ 'class' => 'form-control', 'data-mask' => 'decimal' ]) !!}
            </div>
            
            <div class="col-sm-4">
                {!! Form::label('volume', 'Volume Pessoal') !!}
                {!! Form::text('volume', null, [ 'class' => 'form-control', 'data-mask' => 'percent' ]) !!}
            </div>
        </div> 
            
        <div class="form-group row">
            <div class="col-sm-12">
                {!! Form::label('prize', 'Premiação') !!}
                {!! Form::text('prize', null, [ 'class' => 'form-control' ]) !!}
            </div>
        </div>
        
        <div class="form-group">
            {!! Form::submit('Salvar carreira', [ 'class' => 'btn btn-primary btn-block' ]) !!}
        </div>
        {!! Form::close() !!}
    </div>
</div>