{!! Form::model($kit, [ 'url' => '/admin/kits/' . $kit->id, 'method' => empty($kit->id) ? 'POST' : 'PUT' ]) !!}
  <div class="form-group row">
    <div class="col-sm-6">
      {!! Form::label('name', 'Nome') !!}
      {!! Form::text('name', null, [ 'class' => 'form-control' ]) !!}
    </div>
    
    <div class="col-sm-3">
      {!! Form::label('amount', 'Valor') !!}
      {!! Form::text('amount', null, [ 'class' => 'form-control' ]) !!}
    </div>
    
    <div class="col-sm-3">
      {!! Form::label('weight', 'Peso') !!}
      {!! Form::text('weight', null, [ 'class' => 'form-control' ]) !!}
    </div>

    <div class="col-sm-3">
      {!! Form::label('percent_indication', 'Percentual de Indicação') !!}
      {!! Form::text('percent_indication', null, [ 'class' => 'form-control' ]) !!}
    </div>
    
    <div class="col-sm-3">
      {!! Form::label('percent_formation_team', 'Formação de Equipe (%)') !!}
      {!! Form::text('percent_formation_team', null, [ 'class' => 'form-control' ]) !!}
    </div>
    
    <div class="col-sm-3">
      {!! Form::label('amount_fast_start', 'Valor Início Rápido') !!}
      {!! Form::text('amount_fast_start', null, [ 'class' => 'form-control' ]) !!}
    </div>
    
    <div class="col-sm-3">
      {!! Form::label('points', 'Pontos') !!}
      {!! Form::text('points', null, [ 'class' => 'form-control' ]) !!}
    </div>
    
    <div class="col-sm-3">
      {!! Form::label('discount', 'Desconto (%)') !!}
      {!! Form::text('discount', null, [ 'class' => 'form-control' ]) !!}
    </div>
    
    <div class="col-sm-3">
      {!! Form::label('level', 'Nível RAM') !!}
      {!! Form::text('level', null, [ 'class' => 'form-control' ]) !!}
    </div>
    
  </div>
  <div class="form-group">
    {!! Form::submit('Salvar kit', [ 'class' => 'btn btn-primary btn-block' ]) !!}
  </div>
{!! Form::close() !!}
