<div class="panel">
	<div class="panel-body container-fluid">
		{!! Form::model($distributor, [ 'url' => $url, 'method' => empty($distributor->id) ? 'POST' : 'PUT' ]) !!}
		<div class="form-group row">
			<div class="input-group col-sm-3" style="padding-left: 15px;">
<!-- 			    {!! Form::label('distributor[sponsor_id]', 'Patrocinador') !!}  -->
			    {!! Form::text('distributor[sponsor_id]', null, [ 'class' => 'form-control', 'placeholder' => 'Patrocinador' ]) !!}
			    <span class="input-group-btn">
                    <button type="button" class="btn btn-primary" id="seek-sponsor" ><i class="icon wb-search" aria-hidden="true"></i></button>
                </span>
			</div>
		</div>	
		
		<div class="form-group row">
			<div class="col-sm-6">
                {!! Form::label('distributor[sponsor_name]', 'Nome do Patrocinador') !!} 
                {!! Form::text('distributor[sponsor_name]', null, [ 'class' => 'form-control' ]) !!}
			</div>
		</div>

		<div class="form-group row">
			<div class="col-sm-3">
                {!! Form::label('distributor[person_type]', 'Tipo de Pessoa') !!} 
                {!! Form::select('distributor[person_type]', [ '' => 'Selecione', 'PF' => 'Pessoa Física', 'PJ' => 'Pessoa Jurídica' ], null, [ 'class' => 'form-control' ]) !!}
			</div>
			<div class="col-sm-9">
                {!! Form::label('distributor[name]', 'Nome') !!} 
                {!! Form::text('distributor[name]', null, [ 'class' => 'form-control' ]) !!}
			</div>
		</div>

		<div class="form-group row">
            <div class="col-sm-6">
                {!! Form::label('distributor[username]', 'Login') !!} 
                {!! Form::text('distributor[username]', null, [ 'class' => 'form-control' ]) !!}
			</div>
			<div class="col-sm-6">
                {!! Form::label('distributor[email]', 'Email') !!} 
                {!! Form::text('distributor[email]', null, [ 'class' => 'form-control' ]) !!}
			</div>
		</div>

		<div class="form-group row">
			<div class="col-sm-6">
                {!! Form::label('distributor[mother_name]', 'Nome da Mãe') !!} 
                {!! Form::text('distributor[mother_name]', null, [ 'class' => 'form-control' ]) !!}
			</div>
			<div class="col-sm-6">
                {!! Form::label('distributor[document]', 'CPF') !!} 
                {!! Form::text('distributor[document]', null, [ 'class' => 'form-control' ]) !!}
			</div>
		</div>
		
		<div class="form-group row">
			<div class="col-sm-4">
                {!! Form::label('distributor[born_in]', 'Data de nascimento') !!} 
                {!! Form::text('distributor[born_in]', null, [ 'class' => 'form-control' ]) !!}
			</div>
			<div class="col-sm-4">
                {!! Form::label('distributor[sex]', 'Sexo') !!} 
                {!! Form::select('distributor[sex]', [ '' => 'Selecione', 'M' => 'Masculino', 'F' => 'Feminino' ], null, [ 'class' => 'form-control' ]) !!}
			</div>
		</div>
		
		<div class="form-group row">
			<div class="col-sm-4">
                {!! Form::label('distributor[phone]', 'Telefone') !!} 
                {!! Form::text('distributor[phone]', null, [ 'class' => 'form-control' ]) !!}
			</div>
			<div class="col-sm-4">
                {!! Form::label('distributor[cell_phone]', 'Celular') !!} 
                {!! Form::text('distributor[cell_phone]', null, [ 'class' => 'form-control' ]) !!}
			</div>
		</div>
		
		<div class="form-group row">
			<div class="col-sm-4">
                {!! Form::label('distributor[kit_id]', 'Plano') !!} 
                {!! Form::select('distributor[kit_id]', $kits, null, [ 'class' => 'form-control' ]) !!}
			</div>
		</div>
		
		<div class='form-group row'></div>


		<div class="form-group">
            {!! Form::submit('Salvar distribuidor', [ 'class' => 'btn btn-primary btn-block' ]) !!}
        </div>
		{!! Form::close() !!}
	</div>
</div>


