@extends(config('layout.admin'))

@section('content')

  @include('backoffice::admin/distributor/_page_header', [ 'title' => 'Adicionar' ])

  <div class="page-content">
    <div class="panel">
      <div class="panel-body container-fluid">
        @include('backoffice::admin/distributor/_form')
      </div>
    </div>
  </div>
@endsection
