@extends(config('layout.admin'))

@section('content')
  
  @include('backoffice::admin/distributor/_page_header', [ 'title' => 'Listagem' ])

  <div class="page-content">
    <div class="panel">
      <div class="panel-body container-fluid">
        @if ($kits->isEmpty())
          <div class="alert alert-info"><b>Atenção!</b> Nenhum Distribudor foi encontrado</div>
        @else
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Nome</th>
                <th class="date">Status</th>
                <th class="actions">Ações</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($entities as $key => $entity)
                <tr>
                  <td>
                    <h5>{{ $entity->name() }}</h5>
                  </td>
                  <td class="date"></td>
                  <td class="text-nowrap">
                    <div class="text-right">
                      <a href="{{ Module::action('Backoffice', 'Admin\DistributorController@edit', $entity->id) }}" class="btn btn-sm btn-icon btn-flat btn-default" data-toggle="tooltip" data-original-title="Editar">
                        <i class="icon wb-pencil"></i>
                      </a>

                      <span class="btn btn-sm btn-icon btn-flat btn-default" data-toggle="modal" data-target="#distributor_destroy_{{ $distributor->id }}" data-original-title="Apagar">
                        <i class="icon wb-close"></i>
                      </span>
                    </div>

                    @include('admin/shared/_destroy_confirm', [ 'target_modal_id' => 'kit_destroy_' . $kit->id, 'form_action' => Module::action('Backoffice', 'Admin\KitsController@destroy', $kit->id) ])
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>

          @include('admin/shared/_pagination', [ 'collection' => $entities ])
        @endif
      </div>
    </div>
  </div>
@endsection
