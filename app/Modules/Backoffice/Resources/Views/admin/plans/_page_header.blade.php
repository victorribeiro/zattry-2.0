<div class="page-header">
  <h1 class="page-title">Planos de Carreira</h1>
  <ol class="breadcrumb">
    <li><a href="{{ action('Admin\HomeController@index') }}">Home</a></li>
    <li><a href="{{ Module::action('Backoffice', 'Admin\PlansController@index') }}">Planos</a></li>
    <li class="active">{{ $title or '' }}</li>
  </ol>
  <div class="page-header-actions">
    <a href="{{ Module::action('Backoffice', 'Admin\PlansController@index') }}" class="btn btn-sm btn-icon btn-default btn-outline btn-round" data-toggle="tooltip" data-original-title="Voltar">
      <i class="icon wb-arrow-left"></i>
    </a>
    <a href="{{ Module::action('Backoffice', 'Admin\PlansController@create') }}" class="btn btn-sm btn-icon btn-default btn-outline btn-round" data-toggle="tooltip" data-original-title="Adicionar">
      <i class="icon wb-plus"></i>
    </a>
  </div>
</div>
