@extends(config('layout.admin'))

@section('content')
  @include('backoffice::admin/plans/_page_header', [ 'title' => 'Editar # ' . $plan->career->name ])

  <div class="page-content">
    <div class="panel">
      <div class="panel-body container-fluid">
        @include('backoffice::admin/plans/_form')
      </div>
    </div>
  </div>
@endsection
