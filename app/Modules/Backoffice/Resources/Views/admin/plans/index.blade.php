@extends(config('layout.admin'))

@section('content')
  
  @include('backoffice::admin/plans/_page_header', [ 'title' => 'Listagem' ])

  <div class="page-content">
    <div class="panel">
      <div class="panel-body container-fluid">
        @if ($plans->isEmpty())
          <div class="alert alert-info"><b>Atenção!</b> Nenhum plano foi encontrado</div>
        @else
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Carreira</th>
                <th>Plano</th>
                <th>Percentual</th>
                <th class="actions">Ações</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($plans as $key => $plan)
                <tr>
                  <td>
                    <h5>{{ $plan->parentCareer->name }}</h5>
                  </td>
                  <td>
                    <h5>{{ $plan->career->name }}</h5>
                  </td>
                  <td>
                    <h5>{{ $plan->percent }}</h5>
                  </td>
                  <td class="date"></td>
                  <td class="text-nowrap">
                    <div class="text-right">
                      <a href="{{ Module::action('Backoffice', 'Admin\PlansController@edit', $plan->id) }}" class="btn btn-sm btn-icon btn-flat btn-default" data-toggle="tooltip" data-original-title="Editar">
                        <i class="icon wb-pencil"></i>
                      </a>

                      <span class="btn btn-sm btn-icon btn-flat btn-default" data-toggle="modal" data-target="#plan_destroy_{{ $plan->id }}" data-original-title="Apagar">
                        <i class="icon wb-close"></i>
                      </span>
                    </div>

                    @include('admin/shared/_destroy_confirm', [ 'target_modal_id' => 'plan_destroy_' . $plan->id, 'form_action' => Module::action('Backoffice', 'Admin\PlansController@destroy', $plan->id) ])
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>

          @include('admin/shared/_pagination', [ 'collection' => $plans ])
        @endif
      </div>
    </div>
  </div>
@endsection
