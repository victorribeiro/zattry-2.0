<div class="panel">
	<div class="panel-body container-fluid">
        {!! Form::model($plan, [ 'url' => '/admin/plans/' . $plan->id, 'method' => empty($plan->id) ? 'POST' : 'PUT' ]) !!}
        <div class="form-group row">
            <div class="col-sm-4">
                {!! Form::label('parent_id', 'Carreira') !!}
                {!! Form::select('parent_id', $careers, null, [ 'class' => 'form-control' ]) !!}
            </div>
        </div>
        
        <div class="form-group row">
            <div class="col-sm-4">
                {!! Form::label('career_id', 'Plano') !!}
                {!! Form::select('career_id', $careers, null, [ 'class' => 'form-control' ]) !!}
            </div>
        </div>
        
        <div class="form-group row">
            <div class="col-sm-4">
                {!! Form::label('percent', 'Percentual') !!}
                {!! Form::text('percent', null, [ 'class' => 'form-control', 'data-mask' => 'percent' ]) !!}
            </div>
        </div> 
            
        
        <div class="form-group">
            {!! Form::submit('Salvar carreira', [ 'class' => 'btn btn-primary btn-block' ]) !!}
        </div>
        {!! Form::close() !!}
    </div>
</div>