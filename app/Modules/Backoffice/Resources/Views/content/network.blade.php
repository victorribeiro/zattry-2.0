<!-- <div class="tree">
	<ul>
		<li>
			<a href="#">Nó pai</a>
			<ul>
				<li>
					<a href="#">Nó Filho esquerda</a>
					<ul>
						<li>
							<a href="#">Nó Filho esquerda</a>
						</li>
						<li>
							<a href="#">No filho direita</a>
						</li>
					</ul>
				</li>
				<li>
					<a href="#">No filho direita</a>
					<ul>
						<li>
							<a href="#">Nó Filho esquerda</a>
						</li>
						<li>
							<a href="#">No filho direita</a>
						</li>
					</ul>
				</li>
			</ul>
		</li>
	</ul>
</div> -->
<div class="tree">
<?php
use App\Helpers\BaseHelper;

class TreeView{


	public $tree = array();
	public $leftChild;
	public $rightChild;
	public $id;


	public function __construct($id){

		$this->id = $id;
	}

	public function tree_view($nivel=6) {


		$this->rightChild = false;
		$this->leftChild = false;
		
		if($nivel > 0){


			$this->leftChild	= BaseHelper::getPerna($this->id,'E');
			$this->rightChild	= BaseHelper::getPerna($this->id,'D');

			// echo BaseHelper::getUser($this->leftChild->distributor_id)->name;
			// echo BaseHelper::getUser($this->rightChild->distributor_id)->name;
			
			if (is_object($this->leftChild)) {

				$this->tree[] = array(

		    		'nivel' => $nivel,
		    		'side' => $this->leftChild->side,
		    		'sponsor_id' => $this->leftChild->below,
		    		'distributor_id' => $this->leftChild->distributor_id,
		    		'name' => BaseHelper::getUser($this->leftChild->distributor_id)->name
	    		);
			}

			if (is_object($this->rightChild)){

		    	$this->tree[] =  array(

		    		'nivel' => $nivel,
		    		'side' => $this->rightChild->side,
		    		'sponsor_id' => $this->rightChild->below,
		    		'distributor_id' => $this->rightChild->distributor_id,
		    		'name' => BaseHelper::getUser($this->rightChild->distributor_id)->name
	    		);
	    	}

		    if (is_object($this->leftChild)) {

		    	$nivel--;
		    	$this->id = $this->leftChild->distributor_id;
		        $this->tree_view($nivel);
		    }
		    
		    if (is_object($this->rightChild)){

		    	$nivel--;
		    	$this->id = $this->rightChild->distributor_id;
		        $this->tree_view($nivel);
		    }

	    	if((!$this->leftChild) && (!$this->rightChild)){

				return $this->tree;
			}
		}
	}
}

$level = 6;
$tree = new TreeView(2); // Passa o ID do patrocinador e a quantidade de níveis
$ntws = $tree->tree_view();

function display_tree($arvore,$id,$nivel=6){

	$indices_no_nivel = array_keys(array_column($arvore, 'sponsor_id'), $id);

	// echo "NO NIVEL $i<br>";
	// print_r($indices_no_nivel);

	if(count($indices_no_nivel) > 0){

		$array = array();

		foreach($indices_no_nivel as $indice){

			if($arvore[$indice]['side'] == 'E'){

				$array['E'] =  $arvore[$indice];
			}

			if($arvore[$indice]['side'] == 'D'){

				$array['D'] =  $arvore[$indice];
			}
		}

		echo "<li><a href='#'>" . (isset($array['E']) ? $array['E']['name'] : '(Vazio)') . "</a>";

		if (isset($array['E'])){
			
			$indices_no_nivel = array_keys(array_column($arvore, 'sponsor_id'), $array['E']['distributor_id']);

			if(count($indices_no_nivel) > 0){

				echo "<ul>";
			}

			else{

				echo "</li>";
			}

			if(count($indices_no_nivel) > 0){

				display_tree($arvore,$array['E']['distributor_id']);
			}
		}

		else{

			echo "<li><a href='#'>(Vazio)</a></li>";
		}

		echo "<li><a href='#'>" . (isset($array['D']) ? $array['D']['name'] : '(Vazio)') . "</a></li>";

		if (isset($array['D'])){

			$indices_no_nivel = array_keys(array_column($arvore, 'sponsor_id'), $array['D']['distributor_id']);

			if(count($indices_no_nivel) > 0){

				echo "<ul>";
				
			}
			else{

				echo "</li>";


			}
			if(count($indices_no_nivel) > 0){
			
				display_tree($arvore,$array['E']['distributor_id']);
			}
		}

		echo "</ul>";
	}
}

?>

<div class="tree">
	<ul>
		<li>
			<a href="#">Você</a>

				<?php

				if(count($ntws) > 0){

					echo "<ul>";
					display_tree($ntws,2);
					echo "</ul>";
				}
				?>
		</li>
	</ul>
</div>


</div>