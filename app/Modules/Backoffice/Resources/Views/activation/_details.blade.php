<div class="panel panel-default">
  <div class="panel-heading">
    <div class="panel-title">
      <div class="row">
        <div class="col-xs-6">
          <h4 style="margin-bottom:0">Detalhes da compra</h4>
        </div>
      </div>
    </div>
  </div>

  <div class="panel-body">
    <div class="row">
      <div class="col-md-12"> <br />
        <div class="row">
          <div class="col-md-6">
            <h4 id="kit-details" data-amount="0" class="text-left">
              <span class="name"></span>
              <strong>R$ <span class="amount">0,00</span></strong>
            </h4>
          </div>

          <div class="col-md-6">
            <h4 id="freight-details" data-amount="0" class="text-right">
              <span class="name"></span>
              <strong>R$ <span class="amount">0,00</span></strong>
            </h4>
          </div>
        </div> <hr />
        <h3 class="text-right" style="margin:0px">
          Total <strong>R$ <span id="total-amount" data-amount="0">0,00</span></strong>
        </h3>
      </div>
    </div>

    <br />
    <div class="row">
      <div class="col-md-12">
        <button class="btn btn-lg btn-block btn-primary">
          Pagar minha ativação
        </button>
      </div>
    </div>
  </div>
</div>
