<div class="panel panel-default">
  <div class="panel-heading">
    <div class="panel-title">
      <div class="row">
        <div class="col-xs-6">
          <h4 style="margin-bottom:0">Escolha o Kit</h4>
        </div>
      </div>
    </div>
  </div>

  <div class="panel-body">
    @if (!$kits->isEmpty())
      @foreach ($kits as $key => $kit)
        <div class="row">
          {!! $key != 0 ? '<hr />' : '<br />' !!}
          <div class="col-md-12">
            {!! Form::radio('order[kit_id]', $kit->id, false, [ 'id' => 'kit_id_' . $kit->id, 'data-amount' => $kit->amount, 'data-name' => $kit->name . ':' ]) !!}

            <label for="kit_id_{{ $kit->id }}" class="checkbox-inline">
              {{ $kit->name }} - {{ $kit->amount() }}<br />
              <small>
                Nível: {{ $kit->level }} - Pontos: {{ $kit->points }}
              </small>
            </label>
          </div>
        </div>
      @endforeach
    @endif
  </div>
</div>
