<div class="panel panel-default">
  <div class="panel-heading">
    <div class="panel-title">
      <div class="row">
        <div class="col-xs-6">
          <h4 style="margin-bottom:0">Escolha os Produtos do Kit</h4>
        </div>
      </div>
    </div>
  </div>

  <div class="panel-body">
    <div class="row">
      <div id="kit-costumes" class="col-md-12"> <br />
        <div class="text-muted"><em>Escolha um kit para carregar os produtos</em></div>
      </div>
    </div>
  </div>
</div>
