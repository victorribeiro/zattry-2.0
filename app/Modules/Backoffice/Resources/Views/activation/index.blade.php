@extends(config('layout.admin'))

@section('content')
  <div class="page-header">
    <h1 class="page-title">Ativação <small>pagamento</small></h1> <br>
    <ol class="breadcrumb">
      <li><a href="{{ action('Admin\HomeController@index') }}">Home</a></li>
      <li class="active">Ativação <small>(pagamento)</small></li>
    </ol>
  </div>

  <div class="container-fluid">
    {!! Form::open([ 'url' => Module::action('Backoffice', 'ActivationController@store') ]) !!}
      {!! Form::hidden('card[type]', 0, [ 'id' => 'card-type' ]) !!}

      <div class="row">
        <div class="col-md-5">
          @include('backoffice::activation/_kits')

          @include('backoffice::activation/_addresses')
        </div>

        <div class="col-md-7">
          @include('backoffice::activation/_costumes')

          @include('payments::shared/_payment')

          @include('backoffice::activation/_details')
        </div>
      </div>
    {!! Form::close() !!}
  </div>
@endsection
