var Activation = {
  init: function () {
    // Selectors
    this.total = $('#total-amount');
    this.kitDetails = $('#kit-details');
    this.freightDetails = $('#freight-details');

    // Methods
    this.chooseKit();
    this.chooseForm();
    this.chooseAddress();
    this.chooseInstallment();
    $('#activation-loading').hide();
  },

  setDetails: function (element, amount, text) {
    element.data('amount', amount);
    element.find('.name').text( text );
    element.find('.amount').text( number_format(amount, 2, ',', '') );
  },

  calculateTotal: function (total) {
    if (total == undefined) {
      amount = parseFloat( this.kitDetails.data('amount') );
      freight = parseFloat( this.freightDetails.data('amount') );
      total = amount + freight;
    }

    this.total.data('amount', total).text(number_format(total, 2, ',', ''));
  },

  calculateInstallments: function () {
    total = this.total.data('amount');

    if (total > 0) {
      $.ajax({
        url: '/maxipago/installments/' + total,
        success: function (installments) {
          var options = '';
          $.each(installments, function(i, installment) {
            amount = number_format(installment.amount, 2, ',', '');
            text = installment.quantity + ' x ' + amount + ' = R$ ' + number_format(installment.total, 2, ',', '');
            options += '<option value="' + installment.quantity + '" data-total="' + installment.total +'">' + text + '</option>';
          });
          $('#select-installments').html(options);
        }
      });
    };
  },

  chooseKit: function () {
    if ($('input[name="order[kit_id]"]:checked').length > 0) {
      Activation.setKitAttributes( $('input[name="order[kit_id]"]:checked') );
    };

    $('input[name="order[kit_id]"]').on('change', function () {
      Activation.setKitAttributes( $(this) );
    });
  },

  setKitAttributes: function (kit) {
    amount = kit.data('amount');

    this.setKitDetails(amount, kit.data('name') );

    Activation.getCostumes( kit.val() );

    Activation.calculateTotal();
    Activation.calculateInstallments();
  },

  setKitDetails: function (amount, text) {
    this.setDetails(this.kitDetails, amount, text);
  },

  chooseAddress: function () {
    if ($('input[name="order[address_id]"]:checked').length > 0) {
      Activation.setAddressAttributes( $('input[name="order[address_id]"]:checked') );
    };

    $('input[name="order[address_id]"]').on('change', function () {
      Activation.setAddressAttributes( $(this) );
    });
  },

  setAddressAttributes: function (address) {
    Activation.calculateFreight( address );
    Activation.calculateTotal();
    Activation.calculateInstallments();
  },

  calculateFreight: function (address) {
    var cep = address.data('cep');

    if (cep == '') {
      Activation.setFreightDetails(0, 'Retirar no CD:');
      Activation.calculateTotal();
      Activation.calculateInstallments();
    }
    else {
      Application.showLoading();

      $.ajax({
        url: '/correios/freight/' + cep,
        success: function (response) {
          var freight = response.success ? response.price : 0;
          var name = freight > 0 ? 'Frete PAC:' : 'Retirar no CD:';

          Activation.setFreightDetails(freight, name);
          Activation.calculateTotal();
          Activation.calculateInstallments();
        }
      })
      .always(function () {
        Application.hideLoading()
      });
    };
  },

  setFreightDetails: function (amount, text) {
    this.setDetails(this.freightDetails, amount, text);
  },

  chooseInstallment: function () {
    $('#select-installments').on('change', function () {
      total = parseFloat( $(this).find('option:selected').data('total') );
      Activation.calculateTotal(total);
    });
  },

  getCostumes: function (kit_id) {
    if (kit_id != '') {
      $('#activation-loading').fadeIn();
      $.ajax({
        url: '/costumes/' + kit_id + '/activation',
        success: function (html) {
          $('#kit-costumes').html(html);
        }
      })
      .always( function () {
        $('#activation-loading').fadeOut();
      });
    };
  },

  chooseForm: function () {
    Activation.cardType( $('input[name="card[form]"]:checked').val() );
    Activation.showCardForm( $('input[name="card[form]"]:checked').val() );

    $('input[name="card[form]"]').on('change', function () {
      Activation.showCardForm( $(this).val() );
      Activation.cardType( $(this).val() );
    });
  },

  isCard: function (paymentForm) {
    return paymentForm == 'paypal' || paymentForm == 'boleto' || paymentForm == undefined;
  },

  cardType: function (paymentForm) {
    if (Activation.isCard(paymentForm)) {
      $('#card-type').val(0);
    }
    else {
      $('#card-type').val(1);
    };
  },

  showCardForm: function (paymentForm) {
    if (Activation.isCard(paymentForm)) {
      $('#activation-form-card').slideUp();
    }
    else {
      $('#activation-form-card').slideDown();
    };
  },
};

$(document).on('ready', function() { Activation.init() });
