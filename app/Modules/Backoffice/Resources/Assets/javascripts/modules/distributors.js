var Distributors = {
	init : function() {
		this.seekSponsor()
	},

	seekSponsor : function() {
		var sponsor = $('input[name="distributor[sponsor_id]"]');
		var button = $('#seek-sponsor');
		
		button.on('click', function () {
			if (sponsor.val() != '') {
				Application.showLoading();
				
				$.ajax({
					url: '/backoffice/distributors/seeksponsor/' + sponsor.val(),
					success: function (response) {
						
				    if (response.success) {
				    	$('input[name="distributor[sponsor_name]"]').val(  response.name );
				    } else {
				    	alert(response.message);
				    	$('input[name="distributor[sponsor_id]"]').val("");
				    };
				}
		    })
		    .always(function () {
	    		Application.hideLoading()
			});
		};
		});
	}
};

$(document).on('ready', function() {
	Distributors.init()
});
