<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Node extends Model
{
    protected $fillable = [
    	'level',
    	'distributor_id',
    	'sponsor',
    	'below',
    	'side',
    ];

    public function distributor()
    {
        return $this->belongsTo('App\Distributor');
    }

}
