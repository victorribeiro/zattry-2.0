<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameBackupDistribuidorDocumentoStatusesTable extends Migration
{
    public function up()
    {
        Schema::rename('distribuidor_documento_statuses', 'z_distribuidor_documento_statuses');
    }

    public function down()
    {
        Schema::rename('z_distribuidor_documento_statuses', 'distribuidor_documento_statuses');
    }
}
