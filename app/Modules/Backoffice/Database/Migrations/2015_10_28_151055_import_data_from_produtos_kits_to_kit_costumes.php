<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImportDataFromProdutosKitsToKitCostumes extends Migration
{
    public function up()
    {
        $pks = \DB::table('produtos_kits')->get();

        $values = [];
        foreach ($pks as $key => $pk) {
            $values[] = "('{$pk->produtos_id}', '{$pk->kits_id}', NOW())";
        }

        if (is_array($values) && !empty($values)) {
            $sql = "INSERT INTO `kit_costumes` (`costume_id`, `kit_id`, `created_at`) VALUES " . implode(", ", $values) . ";";
            \DB::statement( $sql );
        }
    }

    public function down()
    {
        \DB::table('kit_costumes')->truncate();
    }
}
