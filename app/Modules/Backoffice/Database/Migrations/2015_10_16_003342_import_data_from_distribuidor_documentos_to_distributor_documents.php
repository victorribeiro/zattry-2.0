<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImportDataFromDistribuidorDocumentosToDistributorDocuments extends Migration
{
    public function up()
    {
        $documentos = \DB::table('distribuidor_documentos')->get();

        $values = [];
        foreach ($documentos as $key => $documento) {
            $values[] = "('"
                . $documento->id . "','"
                . $documento->distribuidor_id . "','"
                . $documento->status_id . "','"
                . $documento->tipo_id . "','"
                . $documento->arquivo . "','"
                . $documento->criado_em . "','"
                . $documento->atualizado_em . "','"
                . $documento->descricao . "')";
        }

        if (is_array($values)) {
            $sql = "INSERT INTO `distributor_documents` (
                `id`,
                `distributor_id`,
                `distributor_document_status_id`,
                `distributor_document_type_id`,
                `filename`,
                `created_at`,
                `updated_at`,
                `name`
            ) VALUES " . implode(", ", $values) . ";";
            \DB::statement( $sql );
        }
    }

    public function down()
    {
        \DB::table('distributor_documents')->truncate();
    }
}
