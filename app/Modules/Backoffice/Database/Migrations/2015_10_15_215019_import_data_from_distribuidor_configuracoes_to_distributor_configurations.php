<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImportDataFromDistribuidorConfiguracoesToDistributorConfigurations extends Migration
{
    public function up()
    {
        $configuracaos = \DB::table('distribuidor_configuracoes')->get();

        $values = [];
        foreach ($configuracaos as $key => $configuracao) {
            $lado_rede = $preferencia_binario = "'right'";
            $lado_balance = 'NULL';

            if ($configuracao->lado_rede == 'D') {
                $lado_rede = "'right'";
            }
            else if ($configuracao->lado_rede == 'E') {
                $lado_rede = "'left'";
            }

            if ($configuracao->preferencia_binario == 'D') {
                $preferencia_binario = "'right'";
            }
            else if ($configuracao->preferencia_binario == 'E') {
                $preferencia_binario = "'left'";
            }
            else if ($configuracao->preferencia_binario == 'B') {
                $preferencia_binario = "'balance'";
            }

            if ($configuracao->lado_balance == 'D') {
                $lado_balance = "'right'";
            }
            else if ($configuracao->lado_balance == 'E') {
                $lado_balance = "'left'";
            }

            $values[] = "('"
                . $configuracao->distribuidor_id . "',"
                . (empty($configuracao->carreira_id) ? 'NULL' : "'{$configuracao->carreira_id}'"). ",'"
                . $configuracao->lado_esquerdo . "','"
                . $configuracao->lado_direito . "',"
                . $preferencia_binario . ","
                . $lado_rede . ","
                . $lado_balance . ",'"
                . $configuracao->qualificacao . "','"
                . $configuracao->ativacao . "','"
                . $configuracao->cadastro_esquerda . "','"
                . $configuracao->cadastro_direito . "','"
                . round($configuracao->vencimento) . "','"
                . $configuracao->updated_at . "', NOW())";
        }

        if (is_array($values)) {
            $sql = "INSERT INTO `distributor_configurations` (
                `distributor_id`,
                `career_id`,
                `left_side`,
                `right_side`,
                `preference_binary`,
                `network_side`,
                `balance_side`,
                `qualification`,
                `ativation`,
                `left_register`,
                `right_register`,
                `expiration_day`,
                `updated_at`,
                `created_at`
            ) VALUES " . implode(", ", $values) . ";";
            \DB::statement( $sql );
        }
    }

    public function down()
    {
        \DB::table('distributor_configurations')->truncate();
    }
}
