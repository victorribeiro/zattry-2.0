<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameBackupPontuacaoTable extends Migration
{
    public function up()
    {
        Schema::rename('pontuacao', 'z_pontuacao');
    }

    public function down()
    {
        Schema::rename('z_pontuacao', 'pontuacao');
    }
}
