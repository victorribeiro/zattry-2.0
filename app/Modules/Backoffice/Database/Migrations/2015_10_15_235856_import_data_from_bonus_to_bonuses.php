<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImportDataFromBonusToBonuses extends Migration
{
	public function up()
	{
        $bonuses = \DB::table('bonus')->get();

        $values = [];
        foreach ($bonuses as $key => $bonus) {
            $values[] = "('"
                . $bonus->id . "','"
                . $bonus->descricao . "', NOW(),NOW())";
        }

        if (is_array($values)) {
            $sql = "INSERT INTO `bonuses` (
                `id`,
                `name`,
                `created_at`,
                `updated_at`
            ) VALUES " . implode(", ", $values) . ";";
            \DB::statement( $sql );
        }
    }

    public function down()
    {
        \DB::table('bonuses')->truncate();
    }
}
