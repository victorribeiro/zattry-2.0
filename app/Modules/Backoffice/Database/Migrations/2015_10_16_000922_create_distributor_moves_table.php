<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistributorMovesTable extends Migration
{
    public function up()
    {
        Schema::create('distributor_moves', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('distributor_id')->unsigned()->index();
            $table->foreign('distributor_id')->references('id')->on('distributors');
            $table->integer('bonus_id')->unsigned()->index();
            $table->foreign('bonus_id')->references('id')->on('bonuses');
            $table->decimal('amount', 17, 2);
            $table->enum('type', ['in','out'])->default('in');
            $table->boolean('confirm_withdraw')->nullable();
            $table->date('moved_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('distributor_moves');
    }
}
