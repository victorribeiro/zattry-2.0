<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameBackupPlanosCarreirasTable extends Migration
{
    public function up()
    {
        Schema::rename('planos_carreiras', 'z_planos_carreiras');
    }

    public function down()
    {
        Schema::rename('z_planos_carreiras', 'planos_carreiras');
    }
}
