<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImportDataFromDistribuidorRedeToDistributorNetworks extends Migration
{
    public function up()
    {
        $networks = \DB::table('distribuidor_rede')->get();

        $values = [];
        foreach ($networks as $key => $network) {
            $values[] = "('"
                . $network->distribuidor_id . "','"
                . $network->sponsor . "','"
                . $network->below . "','"
                . $network->side . "','"
                . $network->created_at . "','"
                . date('Y-m-d H:i:s') . "'"
            . ")";
        }

        if (is_array($values)) {
            $sql = "INSERT INTO `distributor_networks` (
                `distributor_id`,
                `sponsor`,
                `below`,
                `side`,
                `created_at`,
                `updated_at`
            ) VALUES " . implode(", ", $values) . ";";
            \DB::statement( $sql );
        }
    }

    public function down()
    {
        \DB::table('distributor_networks')->truncate();
    }
}
