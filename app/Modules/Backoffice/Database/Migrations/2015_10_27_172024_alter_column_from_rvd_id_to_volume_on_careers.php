<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterColumnFromRvdIdToVolumeOnCareers extends Migration
{
    public function up()
    {
        Schema::table('careers', function($table)
        {
            $table->renameColumn('rvd_id', 'volume')->default(1);
        });
    }

    public function down()
    {
        Schema::table('careers', function($table)
        {
            $table->renameColumn('volume', 'rvd_id');
        });
    }
}
