<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameBackupDistribuidorRedeTable extends Migration
{
    public function up()
    {
        Schema::rename('distribuidor_rede', 'z_distribuidor_rede');
    }

    public function down()
    {
        Schema::rename('z_distribuidor_rede', 'distribuidor_rede');
    }
}
