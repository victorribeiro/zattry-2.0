<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameBackupDistribuidorDocumentoTiposTable extends Migration
{
    public function up()
    {
        Schema::rename('distribuidor_documento_tipos', 'z_distribuidor_documento_tipos');
    }

    public function down()
    {
        Schema::rename('z_distribuidor_documento_tipos', 'distribuidor_documento_tipos');
    }
}
