<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistributorDocumentsTable extends Migration
{
    public function up()
    {
        Schema::create('distributor_documents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('distributor_id')->unsigned()->index();
            $table->foreign('distributor_id')->references('id')->on('distributors');
            $table->integer('distributor_document_status_id')->unsigned()->index();
            $table->foreign('distributor_document_status_id')->references('id')->on('distributor_document_statuses');
            $table->integer('distributor_document_type_id')->unsigned()->index();
            $table->foreign('distributor_document_type_id')->references('id')->on('distributor_document_types');
            $table->string('name')->nullable();
            $table->string('filename');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('distributor_documents');
    }
}
