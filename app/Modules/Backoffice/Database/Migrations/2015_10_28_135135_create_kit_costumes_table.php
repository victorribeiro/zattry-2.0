<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKitCostumesTable extends Migration
{
    public function up()
    {
        Schema::create('kit_costumes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kit_id')->unsigned()->index();
            $table->integer('costume_id')->unsigned()->index();
            $table->integer('quantity')->default(1);
            $table->timestamp('created_at');
        });
    }

    public function down()
    {
        Schema::dropIfExists('kit_costumes');
    }
}
