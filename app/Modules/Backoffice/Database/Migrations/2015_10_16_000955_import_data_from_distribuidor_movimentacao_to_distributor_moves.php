<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImportDataFromDistribuidorMovimentacaoToDistributorMoves extends Migration
{
    public function up()
    {
        $moves = \DB::table('distribuidor_movimentacao')->get();

        $values = [];
        foreach ($moves as $key => $move) {
            $values[] = "('"
                . $move->id . "','"
                . $move->distribuidor_id . "','"
                . $move->bonus_id . "','"
                . $move->valor . "','"
                . ($move->tipo == 'E' ? 'in' : 'out') . "',"
                . ($move->data == '0000-00-00'  ? 'NULL' : "'{$move->data}'") . ","
                . (empty($move->confirm_sacking) ? 'NULL' : "'{$move->confirm_sacking}'") . ", NOW(),NOW())";
        }

        if (is_array($values)) {
            $sql = "INSERT INTO `distributor_moves` (
                `id`,
                `distributor_id`,
                `bonus_id`,
                `amount`,
                `type`,
                `confirm_withdraw`,
                `moved_at`,
                `created_at`,
                `updated_at`
            ) VALUES " . implode(", ", $values) . ";";
            \DB::statement( $sql );
        }
    }

    public function down()
    {
        \DB::table('distributor_moves')->truncate();
    }
}
