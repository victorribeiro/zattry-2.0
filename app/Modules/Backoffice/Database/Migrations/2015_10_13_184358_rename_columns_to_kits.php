<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnsToKits extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('kits', function($table)
        {
            $table->renameColumn('nome', 'name');
            $table->renameColumn('valor', 'amount');
            $table->renameColumn('peso', 'weight');
            $table->renameColumn('percentual_indicacao', 'percent_indication');
            $table->renameColumn('percentual_formacao_equipe', 'percent_formation_team');
            $table->renameColumn('valor_inicio_rapido', 'amount_fast_start');
            $table->renameColumn('pontos', 'points');
            $table->renameColumn('desconto_compra', 'discount');
            $table->renameColumn('ram_nivel', 'level');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('kits', function($table)
        {
            $table->renameColumn('name', 'nome');
            $table->renameColumn('amount', 'valor');
            $table->renameColumn('weight', 'peso');
            $table->renameColumn('percent_indication', 'percentual_indicacao');
            $table->renameColumn('percent_formation_team', 'percentual_formacao_equipe');
            $table->renameColumn('amount_fast_start', 'valor_inicio_rapido');
            $table->renameColumn('points', 'pontos');
            $table->renameColumn('discount', 'desconto_compra');
            $table->renameColumn('level', 'ram_nivel');
        });
	}
}