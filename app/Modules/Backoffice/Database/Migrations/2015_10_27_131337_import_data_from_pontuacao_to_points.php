<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImportDataFromPontuacaoToPoints extends Migration
{
    public function up()
    {
        $pontuacao = \DB::table('pontuacao')->get();

        $values = [];
        foreach ($pontuacao as $key => $ponto) {
            $lado = $ponto->lado == 'D' ? 'right' : 'left';
            $values[] = "({$ponto->id}, '{$ponto->distribuidor_id}', '{$ponto->pontos}', '{$lado}', '{$ponto->data}', '{$ponto->data}', NULL)";
        }

        if (is_array($values)) {
            $sql = "INSERT INTO `points` (`id`, `distributor_id`, `quantity`, `side`, `created_at`, `updated_at`, `deleted_at`) VALUES " . implode(", ", $values) . ";";
            \DB::statement( $sql );
        }
    }

    public function down()
    {
        \DB::table('points')->truncate();
    }
}
