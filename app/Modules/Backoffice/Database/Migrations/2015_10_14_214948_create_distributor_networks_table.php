<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistributorNetworksTable extends Migration
{
    public function up()
    {
        Schema::dropIfExists('distributor_networks');
        Schema::create('distributor_networks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('distributor_id')->references('id')->on('distributors');
            $table->integer('sponsor')->references('id')->on('distributors')->nullable();;
            $table->integer('below')->references('id')->on('distributors')->nullable();;
            $table->enum('side', array('D', 'E'))->default('E');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    public function down()
    {
        Schema::dropIfExists('distributor_networks');
    }
}
