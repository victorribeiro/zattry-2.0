<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImportDataFromDistribuidorDocumentoStatusesToDistributorDocumentStatuses extends Migration
{
    public function up()
    {
        $statuses = \DB::table('distribuidor_documento_statuses')->get();

        $values = [];
        foreach ($statuses as $key => $status) {
            $values[] = "('"
                . $status->id . "','"
                . $status->criado_em . "','"
                . $status->atualizado_em . "','"
                . $status->nome . "')";
        }

        if (is_array($values)) {
            $sql = "INSERT INTO `distributor_document_statuses` (
                `id`,
                `created_at`,
                `updated_at`,
                `name`
            ) VALUES " . implode(", ", $values) . ";";
            \DB::statement( $sql );
        }
    }

    public function down()
    {
        \DB::table('distributor_document_statuses')->truncate();
    }
}
