<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImportDataFromPlanosCarreirasToCareers extends Migration
{
    public function up()
    {
        $carreiras = \DB::table('planos_carreiras')->get();

        $values = [];
        foreach ($carreiras as $key => $carreira) {
            $values[] = "('"
                . $carreira->id . "','"
                . $carreira->nome . "','"
                . $carreira->pontos . "','"
                . $carreira->premio . "','"
                . $carreira->rvd_id . "','"
                . $carreira->limite_ganho . "', NOW(),NOW())";
        }

        if (is_array($values)) {
            $sql = "INSERT INTO `careers` (
                `id`,
                `name`,
                `points`,
                `prize`,
                `rvd_id`,
                `gain_limit`,
                `created_at`,
                `updated_at`
            ) VALUES " . implode(", ", $values) . ";";
            \DB::statement( $sql );
        }
    }

    public function down()
    {
        \DB::table('careers')->truncate();
    }
}
