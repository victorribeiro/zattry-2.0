<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameBackupDistribuidorMovimentacaoTable extends Migration
{
    public function up()
    {
        Schema::rename('distribuidor_movimentacao', 'z_distribuidor_movimentacao');
    }

    public function down()
    {
        Schema::rename('z_distribuidor_movimentacao', 'distribuidor_movimentacao');
    }
}
