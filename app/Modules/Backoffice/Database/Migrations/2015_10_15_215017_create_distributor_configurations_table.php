<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistributorConfigurationsTable extends Migration
{
    public function up()
    {
        Schema::create('distributor_configurations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('distributor_id')->unsigned()->index();
            $table->foreign('distributor_id')->references('id')->on('distributors');
            $table->integer('career_id')->unsigned()->index()->nullable();
            $table->foreign('career_id')->references('id')->on('careers');
            $table->integer('left_side')->default(0);
            $table->integer('right_side')->default(0);
            $table->enum('preference_binary', ['right','left','balance'])->default('right');
            $table->enum('network_side', ['right','left'])->default('right');
            $table->enum('balance_side', ['right','left'])->nullable();
            $table->boolean('qualification')->default(0);
            $table->boolean('ativation')->default(0);
            $table->integer('left_register')->default(0);
            $table->integer('right_register')->default(0);
            $table->integer('expiration_day')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('distributor_configurations');
    }
}
