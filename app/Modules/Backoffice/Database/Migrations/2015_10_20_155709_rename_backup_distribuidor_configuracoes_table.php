<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameBackupDistribuidorConfiguracoesTable extends Migration
{
    public function up()
    {
        Schema::rename('distribuidor_configuracoes', 'z_distribuidor_configuracoes');
    }

    public function down()
    {
        Schema::rename('z_distribuidor_configuracoes', 'distribuidor_configuracoes');
    }
}
