<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePointsTable extends Migration
{
    public function up()
    {
        Schema::create('points', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('distributor_id')->unsigned()->index();
            $table->foreign('distributor_id')->references('id')->on('distributors');
            $table->integer('quantity');
            $table->enum('side', ['right','left'])->default('right');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('points');
    }
}
