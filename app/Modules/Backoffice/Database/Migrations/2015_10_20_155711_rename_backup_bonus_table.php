<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameBackupBonusTable extends Migration
{
    public function up()
    {
        Schema::rename('bonus', 'z_bonus');
    }

    public function down()
    {
        Schema::rename('z_bonus', 'bonus');
    }
}
