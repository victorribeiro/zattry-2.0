<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImportDataFromDistribuidorDocumentoTiposToDistributorDocumentTypes extends Migration
{
    public function up()
    {
        $types = \DB::table('distribuidor_documento_tipos')->get();

        $values = [];
        foreach ($types as $key => $type) {
            $values[] = "('"
                . $type->id . "','"
                . $type->criado_em . "','"
                . $type->atualizado_em . "','"
                . $type->nome . "')";
        }

        if (is_array($values)) {
            $sql = "INSERT INTO `distributor_document_types` (
                `id`,
                `created_at`,
                `updated_at`,
                `name`
            ) VALUES " . implode(", ", $values) . ";";
            \DB::statement( $sql );
        }
    }

    public function down()
    {
        \DB::table('distributor_document_types')->truncate();
    }
}
