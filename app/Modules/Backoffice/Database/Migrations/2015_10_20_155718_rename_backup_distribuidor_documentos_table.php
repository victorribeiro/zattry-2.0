<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameBackupDistribuidorDocumentosTable extends Migration
{
    public function up()
    {
        Schema::rename('distribuidor_documentos', 'z_distribuidor_documentos');
    }

    public function down()
    {
        Schema::rename('z_distribuidor_documentos', 'distribuidor_documentos');
    }
}
