<?php
namespace App\Modules\Backoffice\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class BackofficeServiceProvider extends ServiceProvider
{
	/**
	 * Register the Backoffice module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\Backoffice\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the Backoffice module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('backoffice', realpath(__DIR__.'/../Resources/Lang'));

		View::addNamespace('backoffice', realpath(__DIR__.'/../Resources/Views'));
	}
}
