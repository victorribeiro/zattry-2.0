<?php

namespace App\Modules\Backoffice\Models;
use App\Models\BaseModel;
// use Eloquent;

class Network extends BaseModel {

    protected $table = 'distributor_networks';

	public function distributors()
    {
        return $this->hasMany('App\Modules\Backoffice\Models\Distributor');
    }

}