<?php

namespace App\Modules\Backoffice\Models;

use App\Models\BaseModel;

class Point extends BaseModel
{
    use Decorators\PointDecorator;
    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $fillable = [ 'quantity', 'side' ];

    public function distributor()
    {
        return $this->belongsTo('App\Modules\Backoffice\Models\Distributor');
    }
}
