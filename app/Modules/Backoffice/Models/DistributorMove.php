<?php

namespace App\Modules\Backoffice\Models;

use App\Models\BaseModel;

class DistributorMove extends BaseModel
{
    // use Decorators\DistributorDecorator;
    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $fillable = [];

    public function distributor()
    {
        return $this->belongsTo('App\Modules\Backoffice\Models\Distributor');
    }
}
