<?php

namespace App\Modules\Backoffice\Models;

use App\Models\BaseModel;

class DistributorConfiguration extends BaseModel
{
    public function distributor()
    {
        return $this->belongsTo('App\Modules\Backoffice\Models\Distributor');
    }
}
