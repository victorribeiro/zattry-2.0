<?php

namespace App\Modules\Backoffice\Models;

use App\Models\BaseModel;

class Kit extends BaseModel
{
    use Decorators\KitDecorator;
    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $fillable = [
        'name', 'amount', 'weight', 'percent_indication',
        'percent_formation_team', 'amount_fast_start',
        'points', 'discount', 'level'
    ];

    public function costumes()
    {
        return $this->hasMany('App\Modules\Store\Models\KitCostume');
    }
}
