<?php

namespace App\Modules\Backoffice\Models;

use App\Models\BaseModel;

class Career extends BaseModel
{
    use Decorators\CareerDecorator;
    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $fillable = [ 'name', 
                            'points',
                            'prize',
                            'gain_limit',
                            'volume'];
    
}
