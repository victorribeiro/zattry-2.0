<?php

namespace App\Modules\Backoffice\Models;

use App\Models\BaseModel;

class Distributor extends BaseModel
{
    use Decorators\DistributorDecorator;
    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $fillable = [
        'kit_id', 'sponsor_id', 'name', 'email',
        'mother_name', 'person_type', 'document',
        'born_in', 'sex', 'phone', 'cell_phone', 'marital_status'
    ];

    public function configuration()
    {
        return $this->hasOne('App\Modules\Backoffice\Models\DistributorConfiguration');
    }

    public function sponsor()
    {
        return $this->belongsTo('App\Modules\Backoffice\Models\Distributor', 'sponsor_id');
    }

    public function distributors()
    {
        return $this->hasMany('App\Modules\Backoffice\Models\Distributor', 'sponsor_id');
    }

    public function moves()
    {
        return $this->hasMany('App\Modules\Backoffice\Models\DistributorMove');
    }

    public function user()
    {
        return $this->hasOne('App\User');
    }
}
