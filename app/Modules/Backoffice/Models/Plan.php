<?php

namespace App\Modules\Backoffice\Models;

use App\Models\BaseModel;

class Plan extends BaseModel
{
    use Decorators\PlanDecorator;
    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $fillable = ['parent_id', 'career_id', 'percent'];
    
    public function career()
    {
        return $this->belongsTo('App\Modules\Backoffice\Models\Career');
    }

    public function parentCareer()
    {
        return $this->belongsTo('App\Modules\Backoffice\Models\Career', 'parent_id');
    }
}
