<?php

namespace App\Modules\Backoffice\Models\Decorators;

trait KitDecorator
{
    public function name()
    {
        return $this->name;
    }

    public function amount()
    {
        return \Number::format($this->amount);
    }
}
