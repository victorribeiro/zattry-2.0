<?php

namespace App\Modules\Backoffice\Models\Decorators;

use App\Modules\Backoffice\Models\Point;

trait PointDecorator
{
    public function lower_side()
    {
        $right = Point::where('distributor_id', $this->distributor_id)
            ->where('side', 'right')
        ->sum('quantity');

        $left = Point::where('distributor_id', $this->distributor_id)
            ->where('side', 'left')
        ->sum('quantity');

        return $right > $left ? 'left' : 'right';
    }
}
