<?php

namespace App\Modules\Backoffice\Models\Decorators;

trait CareerDecorator
{
    public function name()
    {
        return $this->name;
    }
    
    public function gain_limit()
    {
        return \Number::format($this->gain_limit);
    }
}
