<?php

namespace App\Modules\Backoffice\Models\Decorators;

trait DistributorDecorator
{
    public function name()
    {
        return $this->name;
    }
}
