<?php

namespace App\Modules\Backoffice\Models\Decorators;

trait PlanDecorator
{

    public function career_name()
    {
        return is_null($this->parentCareer) ? null : $this->parentCareer->name;
    }
    
    
    
}
