<?php
namespace App\Modules\Backoffice\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ActivationRequest extends FormRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
    public function rules()
    {
        return [
            'order.kit_id' => [ 'required' ],
            'order.address_id' => [ 'required' ],
            'stocks' => [ 'required' ],
            'card.form' => [ 'required' ],
            'card.name' => [ 'required', /*'required_if:card.type,1'*/ ],
            'card.cpf' => [ 'required', /*'required_if:card.type,1'*/ ],
            'card.card' => [ 'required', /*'required_if:card.type,1'*/ ],
            'card.code' => [ 'required', /*'required_if:card.type,1'*/ ],
            'card.year' => [ 'required', /*'required_if:card.type,1'*/ ],
            'card.month' => [ 'required', /*'required_if:card.type,1'*/ ],
            'card.installments' => [ 'required', /*'required_if:card.type,1'*/ ],
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'stocks.required' => 'Produtos do kit é obrigatório',
            'card.name.required_if' => 'O campo "Nome do titular do cartão" é obrigatório quando a "Forma de pagamento" não for PayPal ou Boleto',
            'card.cpf.required_if' => 'O campo "CPF do titular do cartão" é obrigatório quando a "Forma de pagamento" não for PayPal ou Boleto',
            'card.card.required_if' => 'O campo "Número do cartão" é obrigatório quando a "Forma de pagamento" não for PayPal ou Boleto',
            'card.code.required_if' => 'O campo "Cód. Segurança" é obrigatório quando a "Forma de pagamento" não for PayPal ou Boleto',
            'card.year.required_if' => 'O campo "Ano" é obrigatório quando a "Forma de pagamento" não for PayPal ou Boleto',
            'card.month.required_if' => 'O campo "Mês" é obrigatório quando a "Forma de pagamento" não for PayPal ou Boleto',
            'card.installments.required_if' => 'O campo "Parcelas" é obrigatório quando a "Forma de pagamento" não for PayPal ou Boleto',
        ];
    }

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}
}
