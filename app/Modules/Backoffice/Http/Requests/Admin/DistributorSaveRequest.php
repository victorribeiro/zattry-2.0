<?php
namespace App\Modules\Backoffice\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class DistributorSaveRequest extends FormRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            'distributor.sponsor_id' => [ 'required' ],
            'distributor.sponsor_name' => [ 'required' ],
            'distributor.person_type' => [ 'required' ],
            'distributor.name' => [ 'required' ],
            'distributor.email' => [ 'required', 'email', 'confirmed' ],
            'distributor.username' => [ 'required' ],
            'distributor.document' => [ 'required' ],
		    'distributor.cell_phone' => [ 'required' ],
		    'distributor.password' => [ 'required', 'password', 'confirmed' ],
		];
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}
}
