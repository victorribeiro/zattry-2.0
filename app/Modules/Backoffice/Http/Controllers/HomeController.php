<?php

namespace App\Modules\Backoffice\Http\Controllers;

use App\Http\Controllers\Controller;

class HomeController extends Controller
{
	public function index()
    {
		return view('backoffice::home/index');
	}
}
