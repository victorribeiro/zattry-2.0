<?php namespace App\Modules\Backoffice\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Backoffice\Models\Network;
use DB;
use App\Helpers\BaseHelper;

class NetworkController extends Controller{

	public function index(){

		$network = Network::all();

		$userID = 10;

		$rede = array();

		$j = 0;

		for($i=0;$i<3;$i++){

			if($i==0){

				$pernas = DB::table('distributor_networks')->where('sponsor',$userID)->get();

				if(count($pernas) > 0){

					foreach($pernas as $user){

						// $rede[$i][] = array();

						$rede[$i][$j]['id'] = $user->id;
						$rede[$i][$j]['distributor_id'] = $user->distributor_id;
						$rede[$i][$j]['sponsor'] = $user->sponsor;
						$rede[$i][$j]['below'] = $user->below;
						$rede[$i][$j]['side'] = $user->side;

						$j++;
					}
				}


			}

			else{

				// print_r($rede[$i-1]);
				if(isset($rede[$i-1]) && count($rede[$i-1]) > 0){
					
					$pernas = DB::table('distributor_networks')->whereIn('sponsor',$rede[$i-1])->get();

					foreach($pernas as $user){

						// $rede[$i] = array();

						$rede[$i][$j]['id'] = $user->id;
						$rede[$i][$j]['distributor_id'] = $user->distributor_id;
						$rede[$i][$j]['sponsor'] = $user->sponsor;
						$rede[$i][$j]['below'] = $user->below;
						$rede[$i][$j]['side'] = $user->side;

						$j++;
					}
				}

				
			}
			
		}

		$data = array(
				
			'styles' => array('stylesheets/pages/backoffice/network.css'),
 			'scripts' => array('javascripts/pages/backoffice/network.js'),
			'content' => 'backoffice::content/network',
			'network' => $rede
		);

		return view('backoffice::default',$data);
		//->with('categories', $categories)
	}

	/*
	public function index(){

		$distributor = new Distributor();

		$distributor->name = "Maria Ferraz";
		$distributor->email = "maria@hotmail.com";
		$distributor->pay = 1;
		$distributor->sideprefered = "E";

		$distributor->save();



		$node = new Node();
		$node->level = 3;
		$node->distributor_id = $distributor->id;
		$node->sponsor = 2; //root
		$node->below = 2;

		$this->createNode($node);


	}
	*/

	public function percorrer(){
		

		
	}

	public function createNode(Node $node, $current=null, $side=null){

		// Get node root

		if($side==null){

			$side = Distributor::where('id',$node->below)->first()->sideprefered;
		}

		// Verify exists node childrens

		if($current==null){

			$current = Node::where('below', $node->below)->where('side',$side)->first();
		}

		$children = Node::where('below', $current->distributor_id)->where('side',$side)->first();

		// Verify if exists children in side prefered of sponsor

		if(count($children) > 0){

			$this->createNode($node,$children,$side);
		}

		else{

			$node->below = $current->distributor_id;
			$node->side = $side;
			$node->save();
			// echo "NÓ INSERIDO DO LADO $side ATRAVÉS DO DISTRIBUIDOR $current->distributor_id";
		}

	}

}