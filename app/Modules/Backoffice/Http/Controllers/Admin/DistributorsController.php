<?php

namespace App\Modules\Backoffice\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Modules\Backoffice\Repositories\DistributorRepository;
use App\Modules\Backoffice\Http\Requests\Admin\DistributorSaveRequest;

class DistributorsController extends Controller
{

    public function __construct(DistributorRepository $repository)
    {
        $this->repository = $repository;
    }
    
    public function index()
    {
        $entity = $this->repository->all();
        return view('backoffice::admin/distributor/index')->with('entities', $entity);
    }
    
    
    
    public function edit($id)
    {
        $kit = $this->repository->find($id);
        if (is_null($kit)) return redirect('admin/distributor')->with('error', 'Distribuidor não encontrado');
    
        return view('backoffice::admin/distributor/edit')->with('kit', $kit);
    }
    
    public function update($id, DistributorSaveRequest $request)
    {
        $updated = $this->repository->update( $id, $request->except('_token', '_method') );
    
        if ($updated->success()) {
            return redirect('admin/distributor')->with('success', $updated->message);
        }
        else {
            return redirect('admin/distributor/' . $id . '/edit')->withInput()->withErrors($updated->message);
        }
    }
    
    public function destroy($id)
    {
        $destroyed = $this->repository->destroy( $id );
    
        if ($destroyed->success()) {
            return redirect('admin/distributor')->with('success', $destroyed->message);
        }
        else {
            return redirect('admin/distributor')->with('error', $destroyed->message);
        }
    }


}