<?php

namespace App\Modules\Backoffice\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Modules\Backoffice\Repositories\PlanRepository;
use App\Modules\Backoffice\Http\Requests\Admin\PlanSaveRequest;
use App\Modules\Backoffice\Models\Career;

class PlansController extends Controller
{
    public function __construct(PlanRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $plans = $this->repository->all();
        return view('backoffice::admin/plans/index')->with('plans', $plans);
    }

    public function create()
    {
        $careers = Career::lists('name', 'id');
        
        $plan = $this->repository->init();
        return view('backoffice::admin/plans/create')->with( [ 'plan' => $plan, 'careers' => $careers ]);
    }

    public function store(PlanSaveRequest $request)
    {
        $stored = $this->repository->store( $request->except('_token', '_method') );

        if ($stored->success()) {
            return redirect('admin/plans')->with('success', $stored->message);
        }
        else {
            return redirect('admin/plans/create')->withInput()->withErrors($stored->errors);
        }
    }

    public function edit($id)
    {
        $careers = Career::lists('name', 'id');
        
        $plan = $this->repository->find($id);
        if (is_null($plan)) return redirect('admin/plans')->with('error', 'Plano não encontrado');

        return view('backoffice::admin/plans/edit')->with([ 'plan' => $plan, 'careers' => $careers ]);
    }

    public function update($id, PlanSaveRequest $request)
    {
        $updated = $this->repository->update( $id, $request->except('_token', '_method') );

        if ($updated->success()) {
            return redirect('admin/plans')->with('success', $updated->message);
        }
        else {
            return redirect('admin/plans/' . $id . '/edit')->withInput()->withErrors($updated->message);
        }
    }

    public function destroy($id)
    {
        $destroyed = $this->repository->destroy( $id );

        if ($destroyed->success()) {
            return redirect('admin/plans')->with('success', $destroyed->message);
        }
        else {
            return redirect('admin/plans')->with('error', $destroyed->message);
        } # endif;
    }
}
