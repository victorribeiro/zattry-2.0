<?php

namespace App\Modules\Backoffice\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Modules\Backoffice\Repositories\CareerRepository;
use App\Modules\Backoffice\Http\Requests\Admin\CareerSaveRequest;

class CareersController extends Controller
{
    public function __construct(CareerRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $careers = $this->repository->all();
        return view('backoffice::admin/careers/index')->with('careers', $careers);
    }

    public function create()
    {
        $career = $this->repository->init();
        return view('backoffice::admin/careers/create')->with('career', $career);
    }

    public function store(CareerSaveRequest $request)
    {
        $stored = $this->repository->store( $request->except('_token', '_method') );

        if ($stored->success()) {
            return redirect('admin/careers')->with('success', $stored->message);
        }
        else {
            return redirect('admin/careers/create')->withInput()->withErrors($stored->errors);
        }
    }

    public function edit($id)
    {
        $career = $this->repository->find($id);
        if (is_null($career)) return redirect('admin/careers')->with('error', 'Carreira não encontrada');

        return view('backoffice::admin/careers/edit')->with('career', $career);
    }

    public function update($id, CareerSaveRequest $request)
    {
        $updated = $this->repository->update( $id, $request->except('_token', '_method') );

        if ($updated->success()) {
            return redirect('admin/careers')->with('success', $updated->message);
        }
        else {
            return redirect('admin/careers/' . $id . '/edit')->withInput()->withErrors($updated->message);
        }
    }

    public function destroy($id)
    {
        $destroyed = $this->repository->destroy( $id );

        if ($destroyed->success()) {
            return redirect('admin/careers')->with('success', $destroyed->message);
        }
        else {
            return redirect('admin/careers')->with('error', $destroyed->message);
        } # endif;
    }
}
