<?php

namespace App\Modules\Backoffice\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Modules\Backoffice\Repositories\KitRepository;
use App\Modules\Backoffice\Http\Requests\Admin\KitSaveRequest;

class KitsController extends Controller
{
    public function __construct(KitRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $kits = $this->repository->all();
        return view('backoffice::admin/kits/index')->with('kits', $kits);
    }

    public function create()
    {
        $kit = $this->repository->init();
        return view('backoffice::admin/kits/create')->with('kit', $kit);
    }

    public function store(KitSaveRequest $request)
    {
        $stored = $this->repository->store( $request->except('_token', '_method') );

        if ($stored->success()) {
            return redirect('admin/kits')->with('success', $stored->message);
        }
        else {
            return redirect('admin/kits/create')->withInput()->withErrors($stored->errors);
        }
    }

    public function edit($id)
    {
        $kit = $this->repository->find($id);
        if (is_null($kit)) return redirect('admin/kits')->with('error', 'Kit não encontrado');

        return view('backoffice::admin/kits/edit')->with('kit', $kit);
    }

    public function update($id, KitSaveRequest $request)
    {
        $updated = $this->repository->update( $id, $request->except('_token', '_method') );

        if ($updated->success()) {
            return redirect('admin/kits')->with('success', $updated->message);
        }
        else {
            return redirect('admin/kits/' . $id . '/edit')->withInput()->withErrors($updated->message);
        }
    }

    public function destroy($id)
    {
        $destroyed = $this->repository->destroy( $id );

        if ($destroyed->success()) {
            return redirect('admin/kits')->with('success', $destroyed->message);
        }
        else {
            return redirect('admin/kits')->with('error', $destroyed->message);
        } # endif;
    }
}
