<?php

namespace App\Modules\Backoffice\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Modules\Backoffice\Repositories\DistributorRepository;
use App\Modules\Backoffice\Http\Requests\Admin\DistributorSaveRequest;

use App\Modules\Backoffice\Models\Kit;

class DistributorsController extends Controller
{

    public function __construct(DistributorRepository $repository)
    {
        $this->repository = $repository;
    }

    public function create()
    {
        $kits = Kit::lists('name', 'id');

        $distributor = $this->repository->init();
        return view('backoffice::distributors/create')->with( [ 'distributor' => $distributor, 'kits' => $kits ]);
    }

    public function store(DistributorSaveRequest $request)
    {
        $stored = $this->repository->store( $request->get('distributor') );

        if ($stored->success()) {
            return \Module::redirectAction('Backoffice','HomeController@index')->with('success', $stored->message);
        }
        else {
            return \Module::redirectAction('Backoffice','DistributorsController@create')->with('error', $stored->message);
        }
    }
    
    public function seeksponsor($id)
    {
        if ($id <= 3)
        {
            return  (array) (object) [ 'success' => false, 'message' => "Distribuidor não pode ser selecionado" ];
        }
        else 
        {
            $distributor = $this->repository->find($id);
            if (is_null($distributor))
                return  (array) (object) [ 'success' => false, 'message' => "Distribuidor não encontrado" ];
            else
                return  (array) (object) [ 'success' => true, 'name' => $distributor['name'] ];
        }
        
        
    }

}
