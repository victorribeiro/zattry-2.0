<?php

namespace App\Modules\Backoffice\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Http\Controllers\BaseController;
use App\Modules\Backoffice\Models\Kit;
use App\Modules\Backoffice\Http\Requests\ActivationRequest;
use App\Modules\Backoffice\Repositories\ActivationRepository;

class ActivationController extends BaseController
{
    public function __construct(ActivationRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    public function index()
    {
        $kits = Kit::all();

        return view('backoffice::activation/index', [ 'kits' => $kits ]);
    }

    public function store(ActivationRequest $request)
    {
        $stored = $this->repository->store($request->except('_token', '_method'));

        if ($stored->success()) {
            return \Module::redirectAction('Backoffice','HomeController@index')->with('success', $stored->message);
        }
        else {
            return \Module::redirectAction('Backoffice','ActivationController@index')->withInput()->with('error', $stored->message);
        }
    }

    public function monthly()
    {
        session()->put('activation.monthly', true);
        return redirect(action('HomeController@index'))->with('success', 'O modo "Ativação Mensal" foi ligado. Faça um compra de no mínimo R$ 90,00 para validar sua ativação');
    }
}
