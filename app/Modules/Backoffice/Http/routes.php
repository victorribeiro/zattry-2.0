<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group([ 'prefix' => 'backoffice', 'middleware' => 'auth' ], function() {
	Route::get('/', 'HomeController@index');
	Route::resource('distributors', 'DistributorsController', [ 'only' => [ 'create', 'store' ] ]);
	Route::get('distributors/seeksponsor/{id}', 'DistributorsController@seeksponsor');

    Route::get('rede', 'NetworkController@index');

    Route::get('activation', 'ActivationController@index');
    Route::post('activation', 'ActivationController@store');
    Route::get('activation/monthly', 'ActivationController@monthly');
});

Route::group([ 'prefix' => 'admin', 'middleware' => 'auth' ], function() {
    Route::resource('kits', 'Admin\KitsController');
    Route::resource('distributors', 'Admin\DistributorsController');
    Route::resource('careers', 'Admin\CareersController');
    Route::resource('plans', 'Admin\PlansController');
});
