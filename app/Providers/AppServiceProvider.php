<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // $modules = \Module::all();

        // foreach($modules as $key => $module) {
        //     $public = app_path('Modules/' . $module['name'] . '/Public');
        //     $this->loadTranslationsFrom($public, $module['slug']);
        //     $this->publishes([ $public => public_path($module['slug']) ], 'public');
        // }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
