<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Artesaos\Defender\Traits\HasDefender;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use App\Modules\Backoffice\Models\Distributor;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, HasDefender;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name', 'username', 'email', 'password' ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token', 'confirmabled_token'];

    /**
     * Relationships
     */
    public function addresses()
    {
        return $this->hasMany('App\Modules\Store\Models\Address', 'owner_id')->where('owner_type', 'user');
    }

    public function distributor()
    {
        return $this->belongsTo('App\Modules\Backoffice\Models\Distributor');
    }

    public function client()
    {
        return $this->hasOne('App\Modules\Store\Models\Client');
    }

    /**
     * Scopes
     */
    public function scopeByLogin($query, $login)
    {
        return $query->where(function ($query) use ($login) {
            return $query->orWhere('email', $login)->orWhere('username', $login);
        });
    }

    /**
     * Decorators
     */
    public function confirmation_url()
    {
        return \Module::action('Auth', 'PasswordController@create', $this->confirmable_token);
    }

    public function distributor_moves()
    {
        $distributor = $this->distributor;

        $sponsor_ids[1] = (object) [
            'distributor_id' => $distributor->sponsor_id,
            'amount' => 5
        ];
        for ($i = 2; $i <= 10; $i++) {
            $distributor = Distributor::select('distributors.id', 'distributors.sponsor_id')->find( $distributor->sponsor_id );

            if (is_null($distributor) || $distributor->sponsor_id == 0) break;

            $sponsor_ids[ $i ] = (object) [
                'distributor_id' => $distributor->sponsor_id,
                'amount' => $i > 4 ? 4 : 5
            ];
        };

        return $sponsor_ids;
    }
}
