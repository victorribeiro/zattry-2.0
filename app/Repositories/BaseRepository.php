<?php

namespace App\Repositories;

use App\Models\BaseModel;

class BaseRepository
{
    use Finders\BaseFinder;

    /**
     * Static messages
     */
    const SUCCESS_MESSAGE = '{:name} foi salv{:gender} com sucesso';
    const CREATED_MESSAGE = '{:name} foi criad{:gender} com sucesso';
    const UPDATED_MESSAGE = '{:name} foi alterad{:gender} com sucesso';
    const REMOVED_MESSAGE = '{:name} foi removid{:gender} com sucesso';
    const TRY_AGAIN_MESSAGE = '{:name} não pode ser salv{:gender}, tente novamente';
    const NOT_FOUND_MESSAGE = '{:name} não foi encontrad{:gender}';

    /**
     * String $name
     */
    public $name = 'registro';

    /**
     * String $gender
     */
    public $gender = 'o';

    /**
     * Integer $perpage
     */
    public $perpage = 30;

    /**
     * String $message
     */
    public $message = null;

    /**
     * Boolean $success
     */
    private $success = false;

    public function __construct(BaseModel $eloquent)
    {
        $this->eloquent = $eloquent;
    }

    public function store($attributes)
    {
        $this->eloquent->fill( $this->beforeStore($attributes) );

        \DB::beginTransaction();
        if ($this->eloquent->save()) {
            $this->message(true, self::CREATED_MESSAGE);

            $this->afterStore($attributes);
        }
        else {
            $this->message(false, self::TRY_AGAIN_MESSAGE);
        }

        $this->success() ? \DB::commit() : \DB::rollback();

        return $this;
    }

    public function update($id, $attributes)
    {
        $this->eloquent = $this->eloquent->find($id);

        \DB::beginTransaction();
        if ($this->eloquent->update( $this->beforeUpdate($attributes) )) {
            $this->message(true, self::UPDATED_MESSAGE);

            $this->afterUpdate($attributes);
        }
        else {
            $this->message(false, self::TRY_AGAIN_MESSAGE);
        }

        $this->success() ? \DB::commit() : \DB::rollback();

        return $this;
    }

    public function destroy($id)
    {
        $this->eloquent = $this->find($id);

        if (empty($this->eloquent)) {
            $this->message(false, self::NOT_FOUND_MESSAGE);
        }
        else {
            $this->beforeDestroy();
            \DB::beginTransaction();
            if ($this->eloquent->delete()) {
                $this->afterDestroy();
                $this->message(true, self::REMOVED_MESSAGE);
            }
            else {
                $this->message(false, self::TRY_AGAIN_MESSAGE);
            }
        }

        $this->success() ? \DB::commit() : \DB::rollback();

        return $this;
    }

    public function message($success, $message)
    {
        $this->success = $success;
        $message = str_replace('{:gender}', $this->gender, $message);
        $message = str_replace('{:name}', $this->name, $message);
        $this->message = ucfirst($message);
    }

    public function success()
    {
        return $this->success;
    }

    protected function beforeSave($attributes)
    {
        return $attributes;
    }

    protected function afterSave($attributes)
    {
        //
    }

    protected function beforeUpdate($attributes)
    {
        return $this->beforeSave($attributes);
    }

    protected function afterUpdate($attributes)
    {
        $this->afterSave($attributes);
    }

    protected function beforeStore($attributes)
    {
        return $this->beforeSave($attributes);
    }

    protected function afterStore($attributes)
    {
        $this->afterSave($attributes);
    }

    protected function beforeDestroy()
    {
        //
    }

    protected function afterDestroy()
    {
        //
    }
}
