<?php

namespace App\Models\Decorators;

trait BaseDecorator
{
    public function status()
    {
        if ($this->trashed()) {
            return '<span class="label label-danger">inativo</span>';
        }
        else {
            return '<span class="label label-success">ativo</span>';
        };
    }

    public function format_date($column, $format)
    {
        if (!empty($this->{$column})) {
            $date = $this->{$column};
            return $date->format('Y') == '-0001' ? '' : $date->format($format);
        }
    }

    public function time_ago($column = 'created_at')
    {
        if (!empty($this->{$column})) {
            return $this->{$column}->diffForHumans();
        }
    }

    public function to_datetime($column)
    {
        return $this->format_date($column, 'd/m/Y H:i:s');
    }

    public function to_date($column)
    {
        return $this->format_date($column, 'd/m/Y');
    }

    public function to_time($column)
    {
        return $this->format_date($column, 'H:i:s');
    }

    public function updated_at_datetime()
    {
        return $this->to_datetime('updated_at');
    }

    public function updated_at_date()
    {
        return $this->to_date('updated_at');
    }

    public function updated_at_time()
    {
        return $this->to_time('updated_at');
    }

    public function created_at_datetime()
    {
        return $this->to_datetime('created_at');
    }

    public function created_at_date()
    {
        return $this->to_date('created_at');
    }

    public function created_at_time()
    {
        return $this->to_time('created_at');
    }
}
