<?php

namespace App\Helpers;

class NumberHelper extends BaseHelper
{
    public static function format($number, $decimals = 2, $decimal_separator = ',', $prefix = 'R$ ', $thousands_separator = '')
    {
        return $prefix . number_format($number, $decimals, $decimal_separator, $thousands_separator);
    }

    public static function strToInt($string)
    {
        return (int) str_replace('.', '', str_replace(',', '', str_replace('R$', '', $string)));
    }

    public static function strToFloat($string)
    {
        return (float) str_replace(',', '.', str_replace('.', '', str_replace('R$', '', $string)));
    }
}
