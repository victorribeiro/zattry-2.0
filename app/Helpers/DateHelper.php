<?php

namespace App\Helpers;

class DateHelper extends BaseHelper
{
    public static function format($date, $format = 'd/m/Y')
    {
        if (strpos($date, '0000-00-00') !== false || empty($date)) {
            return '';
        }

        if ($format === 'Y-m-d') {
            return date($format, strtotime(str_replace('/', '-', $date)));
        }

        return date($format, strtotime($date));
    }
}
