<?php

namespace App\Helpers;

use DB;

class BaseHelper
{
    public static function getRede($userID){

    	return DB::table('distributor_networks')->where('distributor_id',$userID)->get();

    }
    public static function getPernas($userID){

    	return DB::table('distributor_networks')->whereIn('bellow',$userID)->get();
    }
    public static function getPerna($id,$side){

    	return DB::table('distributor_networks')->where('below',$id)->where('side',$side)->first();
    }
    public static function getSubrede($userID){

    	return DB::table('distributor_networks')->where('sponsor',$userID)->get();

    }
    public static function getUser($userID){

    	return DB::table('distributors')->where('id',$userID)->first();

    }
}
