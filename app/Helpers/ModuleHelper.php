<?php

namespace App\Helpers;

// use Caffeinated\Modules\Facades\Module as CaffeinatedModule;

class ModuleHelper extends BaseHelper
{
    public static function action($module, $action, $parameters = [])
    {
        $namespace = '\App\Modules\\' . $module . '\Http\Controllers\\';
        return action($namespace . $action, $parameters);
    }

    public static function redirectAction($module, $action, $parameters = [])
    {
        return redirect(self::action($module, $action, $parameters));
    }

    public static function redirectIntendedAction($module, $action, $parameters = [])
    {
        return redirect()->intended(self::action($module, $action, $parameters));
    }
}
