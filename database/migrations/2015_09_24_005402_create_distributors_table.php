<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistributorsTable extends Migration
{
    public function up()
    {
        Schema::create('distributors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kit_id')->nullable();
            $table->integer('sponsor_id')->nullable();
            $table->string('name');
            $table->string('email');
            $table->string('mother_name')->nullable();
            $table->enum('person_type', [ 'PF', 'PJ' ])->nullable();
            $table->string('document')->nullable();
            $table->date('born_in')->nullable();
            $table->enum('sex', [ 'M', 'F' ])->nullable();
            $table->string('phone')->nullable();
            $table->string('cell_phone')->nullable();
            $table->string('marital_status')->nullable();
            $table->string('salt');
            $table->string('activation_key');
            $table->integer('change_pass')->default(0);
            $table->string('activation_pass')->nullable();
            $table->string('security')->nullable();
            $table->integer('recovery_security')->default(0);
            $table->integer('update_user_id')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('distributors');
    }
}
