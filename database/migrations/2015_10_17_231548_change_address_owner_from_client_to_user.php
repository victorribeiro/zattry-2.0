<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeAddressOwnerFromClientToUser extends Migration
{
    public function up()
    {
        $addresses = \DB::table('addresses')->select(
                'addresses.id',
                'addresses.owner_id',
                'addresses.cep',
                'users.id as user_id',
                'addresses.cep'
            )
            ->where('addresses.owner_type','client')
            ->join('clients','clients.id','=','addresses.owner_id')
            ->join('users','users.client_id','=','clients.id')
            ->orderBy('addresses.id')
        ->get();

        if (is_array($addresses)) {
            foreach ($addresses as $key => $address) {
                \DB::table('addresses')->where('id', $address->id)
                    ->update([ 'owner_id' => $address->user_id, 'owner_type' => 'user' ]);
            }
        }
    }

    public function down()
    {
        //
    }
}
