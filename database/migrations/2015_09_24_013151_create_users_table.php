<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profile_id')->unsigned()->index();
            $table->foreign('profile_id')->references('id')->on('profiles');
            $table->integer('distributor_id')->unsigned()->index()->nullable();
            $table->foreign('distributor_id')->references('id')->on('distributors');
            $table->integer('client_id')->unsigned()->index()->nullable();
            $table->foreign('client_id')->references('id')->on('clients');
            $table->string('name');
            $table->string('username')->unique();
            $table->string('email');
            $table->string('password', 60);
            $table->string('confirmable_token')->nullable();
            $table->datetime('confirmabled_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('users');
    }
}
