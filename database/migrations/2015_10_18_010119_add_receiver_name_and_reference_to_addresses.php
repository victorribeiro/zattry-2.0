<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReceiverNameAndReferenceToAddresses extends Migration
{
    public function up()
    {
        Schema::table('addresses', function (Blueprint $table) {
            $table->string('receiver')->after('owner_type')->nullable();
            $table->string('name')->after('receiver')->nullable();
            $table->text('reference')->after('complement')->nullable();
        });
    }

    public function down()
    {
        Schema::table('addresses', function (Blueprint $table) {
            $table->dropColumn(['receiver', 'name', 'reference']);
        });
    }
}
