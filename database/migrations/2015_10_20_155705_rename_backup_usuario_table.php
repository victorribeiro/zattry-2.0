<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameBackupUsuarioTable extends Migration
{
    public function up()
    {
        Schema::rename('usuario', 'z_usuario');
    }

    public function down()
    {
        Schema::rename('z_usuario', 'usuario');
    }
}
