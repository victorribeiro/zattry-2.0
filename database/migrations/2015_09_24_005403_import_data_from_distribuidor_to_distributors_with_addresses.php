<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImportDataFromDistribuidorToDistributorsWithAddresses extends Migration
{
    public function up()
    {
        $distribuidors = \DB::table('distribuidor')->get();

        $values_distributors = $values_addresses = [];

        foreach ($distribuidors as $key => $distribuidor) {
            $deleted_at = $distribuidor->ativo == 0 ? 'NOW()' : 'NULL';
            $values_distributors[] = "(
                '$distribuidor->id',
                '$distribuidor->kit_id',
                '" . round($distribuidor->patrocinador_id) . "',
                '" . str_replace("'", "\\'", $distribuidor->nome) . "',
                '" . str_replace("'", "\\'", $distribuidor->email) . "',
                '" . str_replace("'", "\\'", $distribuidor->nome_mae) . "',
                '" . str_replace("'", "\\'", $distribuidor->tipo_pessoa) . "',
                '" . str_replace("'", "\\'", $distribuidor->cpf_cnpj) . "',
                '" . (empty($distribuidor->data_nascimento) ? '0000-00-00' : $distribuidor->data_nascimento) . "',
                '" . str_replace("'", "\\'", $distribuidor->sexo) . "',
                '" . str_replace("'", "\\'", $distribuidor->telefone) . "',
                '" . str_replace("'", "\\'", $distribuidor->celular) . "',
                '" . str_replace("'", "\\'", $distribuidor->estado_civil) . "',
                '" . str_replace("'", "\\'", $distribuidor->salt) . "',
                '" . str_replace("'", "\\'", $distribuidor->activation_key) . "',
                '" . str_replace("'", "\\'", $distribuidor->mudar_senha) . "',
                '" . str_replace("'", "\\'", $distribuidor->activation_senha) . "',
                '" . str_replace("'", "\\'", $distribuidor->security) . "',
                '" . str_replace("'", "\\'", $distribuidor->recovery_security) . "',
                '$distribuidor->update_user_id',
                '$distribuidor->data_cadastro',
                '$distribuidor->data_update',
                {$deleted_at}
            )";

            $values_addresses[] = "(
                NULL,
                '$distribuidor->id',
                'distribuidor',
                '" . str_replace("'", "\\'", $distribuidor->cep) . "',
                '" . str_replace("'", "\\'", $distribuidor->logradouro) . "',
                '" . str_replace("'", "\\'", $distribuidor->numero) . "',
                '" . str_replace("'", "\\'", $distribuidor->bairro) . "',
                '" . str_replace("'", "\\'", $distribuidor->complemento) . "',
                '" . str_replace("'", "\\'", $distribuidor->cidade) . "',
                '" . str_replace("'", "\\'", $distribuidor->uf) . "',
                'Brasil',
                NOW(), NOW(), NULL
            )";
        }

        if (is_array($values_distributors)) {
            $sql = "INSERT INTO `distributors` (
                `id`,
                `kit_id`,
                `sponsor_id`,
                `name`,
                `email`,
                `mother_name`,
                `person_type`,
                `document`,
                `born_in`,
                `sex`,
                `phone`,
                `cell_phone`,
                `marital_status`,
                `salt`,
                `activation_key`,
                `change_pass`,
                `activation_pass`,
                `security`,
                `recovery_security`,
                `update_user_id`,
                `created_at`,
                `updated_at`,
                `deleted_at`
            ) VALUES " . implode(", ", $values_distributors) . ";";
            \DB::statement( $sql );
        }

        if (is_array($values_addresses)) {
            $sql = "INSERT INTO `addresses` (
                `id`,
                `owner_id`,
                `owner_type`,
                `cep`,
                `street`,
                `number`,
                `neighborhood`,
                `complement`,
                `city`,
                `state`,
                `country`,
                `created_at`,
                `updated_at`,
                `deleted_at`
            ) VALUES " . implode(", ", $values_addresses) . ";";
            \DB::statement( $sql );
        }
    }

    public function down()
    {
        \DB::table('distributors')->truncate();
        \DB::table('addresses')->truncate();
    }
}
