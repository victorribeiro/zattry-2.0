<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameBackupDistribuidorTable extends Migration
{
    public function up()
    {
        Schema::rename('distribuidor', 'z_distribuidor');
    }

    public function down()
    {
        Schema::rename('z_distribuidor', 'distribuidor');
    }
}
