<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImportDataFromClientesToClientsWithAddresses extends Migration
{
    public function up()
    {
        $clients = \DB::table('clientes')->get();

        $values_clients = $values_addresses = [];

        foreach ($clients as $key => $client) {
            $values_clients[] = "(
                '$client->id',
                '$client->nome',
                '$client->cpf',
                '$client->nascido_em',
                '$client->sexo',
                '$client->fone',
                '$client->adesao',
                '$client->salt',
                '$client->activation_key',
                '$client->created_at',
                '$client->updated_at',
                NULL
            )";

            $values_addresses[] = "(
                NULL,
                '$client->id',
                'client',
                '" . str_replace("'", "\\'", $client->cep) . "',
                '" . str_replace("'", "\\'", $client->endereco) . "',
                '" . str_replace("'", "\\'", $client->numero) . "',
                '" . str_replace("'", "\\'", $client->bairro) . "',
                '',
                '" . str_replace("'", "\\'", $client->cidade) . "',
                '" . str_replace("'", "\\'", $client->uf) . "',
                'Brasil',
                NOW(), NOW(), NULL
            )";
        }

        if (is_array($values_clients)) {
            $sql = "INSERT INTO `clients` (
                `id`,
                `name`,
                `document`,
                `born_in`,
                `sex`,
                `phone`,
                `agree`,
                `salt`,
                `activation_key`,
                `created_at`,
                `updated_at`,
                `deleted_at`
            ) VALUES " . implode(", ", $values_clients) . ";";
            \DB::statement( $sql );
        }

        if (is_array($values_addresses)) {
            $sql = "INSERT INTO `addresses` (
                `id`,
                `owner_id`,
                `owner_type`,
                `cep`,
                `street`,
                `number`,
                `neighborhood`,
                `complement`,
                `city`,
                `state`,
                `country`,
                `created_at`,
                `updated_at`,
                `deleted_at`
            ) VALUES " . implode(", ", $values_addresses) . ";";
            \DB::statement( $sql );
        }
    }

    public function down()
    {
        \DB::table('distributors')->truncate();
        \DB::table('addresses')->truncate();
    }
}
