<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameBackupClientesTable extends Migration
{
    public function up()
    {
        Schema::rename('clientes', 'z_clientes');
    }

    public function down()
    {
        Schema::rename('z_clientes', 'clientes');
    }
}
