<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImportDataFromPerfilToProfiles extends Migration
{
    public function up()
    {
        $perfis = \DB::table('perfil')->get();

        $values = [];
        foreach ($perfis as $key => $perfil) {
            $deleted_at = $perfil->ativo == 0 ? 'NOW()' : 'NULL';
            $values[] = "({$perfil->id}, '{$perfil->nomePerfil}', NOW(), NOW(), {$deleted_at})";
        }

        if (is_array($values)) {
            $sql = "INSERT INTO `profiles` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES " . implode(", ", $values) . ";";
            \DB::statement( $sql );
        }
    }

    public function down()
    {
        \DB::table('profiles')->truncate();
    }
}
