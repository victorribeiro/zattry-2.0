<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImportDataFromUsuarioToUsers extends Migration
{
    public function up()
    {
        $usuarios = \DB::table('usuario')->get();

        $values = [];
        foreach ($usuarios as $key => $usuario) {
            $deleted_at = $usuario->ativo == 0 ? 'NOW()' : 'NULL';
            $values[] = "(
                '{$usuario->id}',
                '{$usuario->idPerfil}',
                " . (empty($usuario->distribuidor_id) ? 'NULL' : $usuario->distribuidor_id) . ",
                " . (empty($usuario->cliente_id) ? 'NULL' : $usuario->cliente_id) . ",
                '{$usuario->nome}',
                '{$usuario->login}',
                '" . (empty($usuario->email) ? "{$usuario->login}@zattry.com.br" : $usuario->email) . "',
                '" . bcrypt($usuario->senha) . "',
                NOW(), NOW(), {$deleted_at})";
        }

        if (is_array($values)) {
            $sql = "INSERT INTO `users` (
                `id`,
                `profile_id`,
                `distributor_id`,
                `client_id`,
                `name`,
                `username`,
                `email`,
                `password`,
                `created_at`, `updated_at`, `deleted_at`) VALUES " . implode(", ", $values) . ";";
            \DB::statement( $sql );
        }
    }

    public function down()
    {
        \DB::table('users')->truncate();
    }
}
