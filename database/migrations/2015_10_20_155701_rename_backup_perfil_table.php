<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameBackupPerfilTable extends Migration
{
    public function up()
    {
        Schema::rename('perfil', 'z_perfil');
    }

    public function down()
    {
        Schema::rename('z_perfil', 'perfil');
    }
}
