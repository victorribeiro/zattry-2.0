var Product = {
  init: function () {
    this.redirectToShow();
  },

  redirectToShow: function () {
    $('#product_color_code').on('change', function () {
      location.href = $(this).val();
    })
  }
};

$(document).on('ready', function () { Product.init() })

var Checkout = {
  init: function () {
    this.chooseFreight();
    this.calculateFreight();
  },

  chooseFreight: function () {
    var inputFreight = $('input[name="freight"]');

    Checkout.showFormFreight( $('input[name="freight"]:checked').val() );

    inputFreight.on('change', function () {
      Checkout.showFormFreight( $(this).val() );
    });
  },

  calculateFreight: function () {
    var cep = $('#cep');
    var button = $('#calculate-freight');

    button.on('click', function () {
      if (cep.val() != '') {
        Application.showLoading();

        $.ajax({
          url: '/correios/freight/' + cep.val(),
          success: function (response) {
            var subtotal = parseFloat( $('#checkout-subtotal').val() );
            if (response.success) {
              $('#checkout-freight').text(  'R$ ' + number_format(response.price, 2, ',', '') );
              $('#checkout-message').text( 'Seu pedido será entregue em até ' + response.deadline + ' após a aprovação do pagamento' );
              total = response.price + subtotal;
            }
            else {
              $('#checkout-freight').text( 'R$ 0,00' );
              $('#checkout-message').text( response.message );
              total = subtotal;
            };

            $('#checkout-total').text( 'R$ ' + number_format(total, 2, ',', '') );
          }
        })
        .always(function () {
          Application.hideLoading()
        });
      };
    });
  },

  showFormFreight: function (freightVal) {
    if (freightVal == 'pac') {
      $('#form-freight').show();
    }
    else {
      $('#form-freight').hide();
    };
  }
};

$(document).on('ready', function () { Checkout.init() })

var Order = {
  init: function () {
    this.chooseFreight();
  },

  chooseFreight: function () {
    $('#panel-order-address').find('[name="address_id"]').on('change', function () {
      var cep = $(this).data('cep');

      Application.showLoading();
      $.ajax({
        url: '/correios/freight/' + cep,
        success: function (response) {
          var subtotal = parseFloat( $('#checkout-subtotal').val() );
          if (response.success) {
            $('#checkout-freight').text( 'R$ ' + number_format(response.price, 2, ',', '') );
            total = response.price + subtotal;
          }
          else {
            $('#checkout-freight').text( 'R$ 0,00' );
            total = subtotal;
          };

          $('#checkout-total').text( 'R$ ' + number_format(total, 2, ',', '') );
        }
      })
      .always(function () {
        Application.hideLoading()
      });
    });
  }
};

$(document).on('ready', function () { Order.init() })

var Correios = {
  init: function () {
    this.address();
  },

  address: function () {
    $('#address-cep').on('blur', function () {
      var field = $(this);
      var prefix = field.data('prefix');
      if (field.val() != '') {
        Application.showLoading();

        $.ajax({
          url: '/correios/address/' + field.val(),
          success: function (response) {
            if (response.success) {
              $('#' + prefix + '-street').val( response.address.street );
              $('#' + prefix + '-neighborhood').val( response.address.neighborhood );
              $('#' + prefix + '-city').val( response.address.city );
              $('#' + prefix + '-state').val( response.address.state );
              $('#correios-message').text( '' );
            }
            else {
              $('#correios-message').text( response.message );
            };
          }
        })
        .always(function () {
          Application.hideLoading()
        });
      };
    })
  }
};

$(document).on('ready', function () { Correios.init() })

//# sourceMappingURL=modules.js.map
