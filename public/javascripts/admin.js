var Activation = {
  init: function () {
    // Selectors
    this.total = $('#total-amount');
    this.kitDetails = $('#kit-details');
    this.freightDetails = $('#freight-details');

    // Methods
    this.chooseKit();
    this.chooseForm();
    this.chooseAddress();
    this.chooseInstallment();
    $('#activation-loading').hide();
  },

  setDetails: function (element, amount, text) {
    element.data('amount', amount);
    element.find('.name').text( text );
    element.find('.amount').text( number_format(amount, 2, ',', '') );
  },

  calculateTotal: function (total) {
    if (total == undefined) {
      amount = parseFloat( this.kitDetails.data('amount') );
      freight = parseFloat( this.freightDetails.data('amount') );
      total = amount + freight;
    }

    this.total.data('amount', total).text(number_format(total, 2, ',', ''));
  },

  calculateInstallments: function () {
    total = this.total.data('amount');

    if (total > 0) {
      $.ajax({
        url: '/maxipago/installments/' + total,
        success: function (installments) {
          var options = '';
          $.each(installments, function(i, installment) {
            amount = number_format(installment.amount, 2, ',', '');
            text = installment.quantity + ' x ' + amount + ' = R$ ' + number_format(installment.total, 2, ',', '');
            options += '<option value="' + installment.quantity + '" data-total="' + installment.total +'">' + text + '</option>';
          });
          $('#select-installments').html(options);
        }
      });
    };
  },

  chooseKit: function () {
    if ($('input[name="order[kit_id]"]:checked').length > 0) {
      Activation.setKitAttributes( $('input[name="order[kit_id]"]:checked') );
    };

    $('input[name="order[kit_id]"]').on('change', function () {
      Activation.setKitAttributes( $(this) );
    });
  },

  setKitAttributes: function (kit) {
    amount = kit.data('amount');

    this.setKitDetails(amount, kit.data('name') );

    Activation.getCostumes( kit.val() );

    Activation.calculateTotal();
    Activation.calculateInstallments();
  },

  setKitDetails: function (amount, text) {
    this.setDetails(this.kitDetails, amount, text);
  },

  chooseAddress: function () {
    if ($('input[name="order[address_id]"]:checked').length > 0) {
      Activation.setAddressAttributes( $('input[name="order[address_id]"]:checked') );
    };

    $('input[name="order[address_id]"]').on('change', function () {
      Activation.setAddressAttributes( $(this) );
    });
  },

  setAddressAttributes: function (address) {
    Activation.calculateFreight( address );
    Activation.calculateTotal();
    Activation.calculateInstallments();
  },

  calculateFreight: function (address) {
    var cep = address.data('cep');

    if (cep == '') {
      Activation.setFreightDetails(0, 'Retirar no CD:');
      Activation.calculateTotal();
      Activation.calculateInstallments();
    }
    else {
      Application.showLoading();

      $.ajax({
        url: '/correios/freight/' + cep,
        success: function (response) {
          var freight = response.success ? response.price : 0;
          var name = freight > 0 ? 'Frete PAC:' : 'Retirar no CD:';

          Activation.setFreightDetails(freight, name);
          Activation.calculateTotal();
          Activation.calculateInstallments();
        }
      })
      .always(function () {
        Application.hideLoading()
      });
    };
  },

  setFreightDetails: function (amount, text) {
    this.setDetails(this.freightDetails, amount, text);
  },

  chooseInstallment: function () {
    $('#select-installments').on('change', function () {
      total = parseFloat( $(this).find('option:selected').data('total') );
      Activation.calculateTotal(total);
    });
  },

  getCostumes: function (kit_id) {
    if (kit_id != '') {
      $('#activation-loading').fadeIn();
      $.ajax({
        url: '/costumes/' + kit_id + '/activation',
        success: function (html) {
          $('#kit-costumes').html(html);
        }
      })
      .always( function () {
        $('#activation-loading').fadeOut();
      });
    };
  },

  chooseForm: function () {
    Activation.cardType( $('input[name="card[form]"]:checked').val() );
    Activation.showCardForm( $('input[name="card[form]"]:checked').val() );

    $('input[name="card[form]"]').on('change', function () {
      Activation.showCardForm( $(this).val() );
      Activation.cardType( $(this).val() );
    });
  },

  isCard: function (paymentForm) {
    return paymentForm == 'paypal' || paymentForm == 'boleto' || paymentForm == undefined;
  },

  cardType: function (paymentForm) {
    if (Activation.isCard(paymentForm)) {
      $('#card-type').val(0);
    }
    else {
      $('#card-type').val(1);
    };
  },

  showCardForm: function (paymentForm) {
    if (Activation.isCard(paymentForm)) {
      $('#activation-form-card').slideUp();
    }
    else {
      $('#activation-form-card').slideDown();
    };
  },
};

$(document).on('ready', function() { Activation.init() });

var Distributors = {
	init : function() {
		this.seekSponsor()
	},

	seekSponsor : function() {
		var sponsor = $('input[name="distributor[sponsor_id]"]');
		var button = $('#seek-sponsor');
		
		button.on('click', function () {
			if (sponsor.val() != '') {
				Application.showLoading();
				
				$.ajax({
					url: '/backoffice/distributors/seeksponsor/' + sponsor.val(),
					success: function (response) {
						
				    if (response.success) {
				    	$('input[name="distributor[sponsor_name]"]').val(  response.name );
				    } else {
				    	alert(response.message);
				    	$('input[name="distributor[sponsor_id]"]').val("");
				    };
				}
		    })
		    .always(function () {
	    		Application.hideLoading()
			});
		};
		});
	}
};

$(document).on('ready', function() {
	Distributors.init()
});

var Application = {
  init: function () {
    this.confirmation();
  },

  hideLoading: function () {
    $('#ajax-loading').fadeOut('slow');
  },

  showLoading: function () {
    $('#ajax-loading').fadeIn('fast');
  },

  confirmation: function () {
    $('[data-confirm]').click(function (event) {
      console.log($(this).data('confirm'))
      return confirmation = confirm( $(this).data('confirm') );
    });
  }
};

$(document).on('ready', function () { Application.init(); });

// number_format;
function number_format(number, decimals, dec_point, thousands_sep) {
  //  discuss at: http://phpjs.org/functions/number_format/
  // original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
  // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // improved by: davook
  // improved by: Brett Zamir (http://brett-zamir.me)
  // improved by: Brett Zamir (http://brett-zamir.me)
  // improved by: Theriault
  // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // bugfixed by: Michael White (http://getsprink.com)
  // bugfixed by: Benjamin Lupton
  // bugfixed by: Allan Jensen (http://www.winternet.no)
  // bugfixed by: Howard Yeend
  // bugfixed by: Diogo Resende
  // bugfixed by: Rival
  // bugfixed by: Brett Zamir (http://brett-zamir.me)
  //  revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
  //  revised by: Luke Smith (http://lucassmith.name)
  //    input by: Kheang Hok Chin (http://www.distantia.ca/)
  //    input by: Jay Klehr
  //    input by: Amir Habibi (http://www.residence-mixte.com/)
  //    input by: Amirouche
  //   example 1: number_format(1234.56);
  //   returns 1: '1,235'
  //   example 2: number_format(1234.56, 2, ',', ' ');
  //   returns 2: '1 234,56'
  //   example 3: number_format(1234.5678, 2, '.', '');
  //   returns 3: '1234.57'
  //   example 4: number_format(67, 2, ',', '.');
  //   returns 4: '67,00'
  //   example 5: number_format(1000);
  //   returns 5: '1,000'
  //   example 6: number_format(67.311, 2);
  //   returns 6: '67.31'
  //   example 7: number_format(1000.55, 1);
  //   returns 7: '1,000.6'
  //   example 8: number_format(67000, 5, ',', '.');
  //   returns 8: '67.000,00000'
  //   example 9: number_format(0.9, 0);
  //   returns 9: '1'
  //  example 10: number_format('1.20', 2);
  //  returns 10: '1.20'
  //  example 11: number_format('1.20', 4);
  //  returns 11: '1.2000'
  //  example 12: number_format('1.2000', 3);
  //  returns 12: '1.200'
  //  example 13: number_format('1 000,50', 2, '.', ' ');
  //  returns 13: '100 050.00'
  //  example 14: number_format(1e-8, 8, '.', '');
  //  returns 14: '0.00000001'

  number = (number + '')
    .replace(/[^0-9+\-Ee.]/g, '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function(n, prec) {
      var k = Math.pow(10, prec);
      return '' + (Math.round(n * k) / k)
        .toFixed(prec);
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
    .split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '')
    .length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1).join('0');
  }
  return s.join(dec);
}

var MeioMask = {
  init: function () {
    this.extras();
    this.simple();
  },

  simple: function () {
    $('input[data-mask]').each(function() {
      var input = $(this);
      input.setMask( input.data('mask') );
    });
  },

  extras: function () {
    $.mask.rules.n = /[0-9]?/

    $.mask.masks['phone-br'] = { mask : '(99) 9999-9999n' };
    $.mask.masks['percent'] = { mask : '199' };
  }
};

$(document).on('ready', function () { MeioMask.init(); });

var Datepicker = {
  init: function () {
    this.simple();
  },

  simple: function () {
    $('.bs-datepicker').datepicker({ format: 'dd/mm/yyyy' });
  }
};

$(document).on('ready', function () { Datepicker.init(); });

//# sourceMappingURL=admin.js.map

function number_format(t,e,a,i){t=(t+"").replace(/[^0-9+\-Ee.]/g,"");var s=isFinite(+t)?+t:0,n=isFinite(+e)?Math.abs(e):0,r="undefined"==typeof i?",":i,o="undefined"==typeof a?".":a,h="",l=function(t,e){var a=Math.pow(10,e);return""+(Math.round(t*a)/a).toFixed(e)};return h=(n?l(s,n):""+Math.round(s)).split("."),h[0].length>3&&(h[0]=h[0].replace(/\B(?=(?:\d{3})+(?!\d))/g,r)),(h[1]||"").length<n&&(h[1]=h[1]||"",h[1]+=new Array(n-h[1].length+1).join("0")),h.join(o)}!function(t){"function"==typeof define&&define.amd?define(["jquery"],t):t("object"==typeof exports?require("jquery"):jQuery)}(function(t,e){function a(){return new Date(Date.UTC.apply(Date,arguments))}function i(){var t=new Date;return a(t.getFullYear(),t.getMonth(),t.getDate())}function s(t,e){return t.getUTCFullYear()===e.getUTCFullYear()&&t.getUTCMonth()===e.getUTCMonth()&&t.getUTCDate()===e.getUTCDate()}function n(t){return function(){return this[t].apply(this,arguments)}}function r(t){return t&&!isNaN(t.getTime())}function o(e,a){function i(t,e){return e.toLowerCase()}var s,n=t(e).data(),r={},o=new RegExp("^"+a.toLowerCase()+"([A-Z])");a=new RegExp("^"+a.toLowerCase());for(var h in n)a.test(h)&&(s=h.replace(o,i),r[s]=n[h]);return r}function h(e){var a={};if(m[e]||(e=e.split("-")[0],m[e])){var i=m[e];return t.each(g,function(t,e){e in i&&(a[e]=i[e])}),a}}var l=function(){var e={get:function(t){return this.slice(t)[0]},contains:function(t){for(var e=t&&t.valueOf(),a=0,i=this.length;i>a;a++)if(this[a].valueOf()===e)return a;return-1},remove:function(t){this.splice(t,1)},replace:function(e){e&&(t.isArray(e)||(e=[e]),this.clear(),this.push.apply(this,e))},clear:function(){this.length=0},copy:function(){var t=new l;return t.replace(this),t}};return function(){var a=[];return a.push.apply(a,arguments),t.extend(a,e),a}}(),d=function(e,a){this._process_options(a),this.dates=new l,this.viewDate=this.o.defaultViewDate,this.focusDate=null,this.element=t(e),this.isInline=!1,this.isInput=this.element.is("input"),this.component=this.element.hasClass("date")?this.element.find(".add-on, .input-group-addon, .btn"):!1,this.hasInput=this.component&&this.element.find("input").length,this.component&&0===this.component.length&&(this.component=!1),this.picker=t(v.template),this._buildEvents(),this._attachEvents(),this.isInline?this.picker.addClass("datepicker-inline").appendTo(this.element):this.picker.addClass("datepicker-dropdown dropdown-menu"),this.o.rtl&&this.picker.addClass("datepicker-rtl"),this.viewMode=this.o.startView,this.o.calendarWeeks&&this.picker.find("tfoot .today, tfoot .clear").attr("colspan",function(t,e){return parseInt(e)+1}),this._allow_update=!1,this.setStartDate(this._o.startDate),this.setEndDate(this._o.endDate),this.setDaysOfWeekDisabled(this.o.daysOfWeekDisabled),this.setDaysOfWeekHighlighted(this.o.daysOfWeekHighlighted),this.setDatesDisabled(this.o.datesDisabled),this.fillDow(),this.fillMonths(),this._allow_update=!0,this.update(),this.showMode(),this.isInline&&this.show()};d.prototype={constructor:d,_process_options:function(s){this._o=t.extend({},this._o,s);var n=this.o=t.extend({},this._o),r=n.language;switch(m[r]||(r=r.split("-")[0],m[r]||(r=f.language)),n.language=r,n.startView){case 2:case"decade":n.startView=2;break;case 1:case"year":n.startView=1;break;default:n.startView=0}switch(n.minViewMode){case 1:case"months":n.minViewMode=1;break;case 2:case"years":n.minViewMode=2;break;default:n.minViewMode=0}switch(n.maxViewMode){case 0:case"days":n.maxViewMode=0;break;case 1:case"months":n.maxViewMode=1;break;default:n.maxViewMode=2}n.startView=Math.min(n.startView,n.maxViewMode),n.startView=Math.max(n.startView,n.minViewMode),n.multidate!==!0&&(n.multidate=Number(n.multidate)||!1,n.multidate!==!1&&(n.multidate=Math.max(0,n.multidate))),n.multidateSeparator=String(n.multidateSeparator),n.weekStart%=7,n.weekEnd=(n.weekStart+6)%7;var o=v.parseFormat(n.format);if(n.startDate!==-(1/0)&&(n.startDate?n.startDate instanceof Date?n.startDate=this._local_to_utc(this._zero_time(n.startDate)):n.startDate=v.parseDate(n.startDate,o,n.language):n.startDate=-(1/0)),n.endDate!==1/0&&(n.endDate?n.endDate instanceof Date?n.endDate=this._local_to_utc(this._zero_time(n.endDate)):n.endDate=v.parseDate(n.endDate,o,n.language):n.endDate=1/0),n.daysOfWeekDisabled=n.daysOfWeekDisabled||[],t.isArray(n.daysOfWeekDisabled)||(n.daysOfWeekDisabled=n.daysOfWeekDisabled.split(/[,\s]*/)),n.daysOfWeekDisabled=t.map(n.daysOfWeekDisabled,function(t){return parseInt(t,10)}),n.daysOfWeekHighlighted=n.daysOfWeekHighlighted||[],t.isArray(n.daysOfWeekHighlighted)||(n.daysOfWeekHighlighted=n.daysOfWeekHighlighted.split(/[,\s]*/)),n.daysOfWeekHighlighted=t.map(n.daysOfWeekHighlighted,function(t){return parseInt(t,10)}),n.datesDisabled=n.datesDisabled||[],!t.isArray(n.datesDisabled)){var h=[];h.push(v.parseDate(n.datesDisabled,o,n.language)),n.datesDisabled=h}n.datesDisabled=t.map(n.datesDisabled,function(t){return v.parseDate(t,o,n.language)});var l=String(n.orientation).toLowerCase().split(/\s+/g),d=n.orientation.toLowerCase();if(l=t.grep(l,function(t){return/^auto|left|right|top|bottom$/.test(t)}),n.orientation={x:"auto",y:"auto"},d&&"auto"!==d)if(1===l.length)switch(l[0]){case"top":case"bottom":n.orientation.y=l[0];break;case"left":case"right":n.orientation.x=l[0]}else d=t.grep(l,function(t){return/^left|right$/.test(t)}),n.orientation.x=d[0]||"auto",d=t.grep(l,function(t){return/^top|bottom$/.test(t)}),n.orientation.y=d[0]||"auto";if(n.defaultViewDate){var c=n.defaultViewDate.year||(new Date).getFullYear(),u=n.defaultViewDate.month||0,p=n.defaultViewDate.day||1;n.defaultViewDate=a(c,u,p)}else n.defaultViewDate=i();n.showOnFocus=n.showOnFocus!==e?n.showOnFocus:!0,n.zIndexOffset=n.zIndexOffset!==e?n.zIndexOffset:10},_events:[],_secondaryEvents:[],_applyEvents:function(t){for(var a,i,s,n=0;n<t.length;n++)a=t[n][0],2===t[n].length?(i=e,s=t[n][1]):3===t[n].length&&(i=t[n][1],s=t[n][2]),a.on(s,i)},_unapplyEvents:function(t){for(var a,i,s,n=0;n<t.length;n++)a=t[n][0],2===t[n].length?(s=e,i=t[n][1]):3===t[n].length&&(s=t[n][1],i=t[n][2]),a.off(i,s)},_buildEvents:function(){var e={keyup:t.proxy(function(e){-1===t.inArray(e.keyCode,[27,37,39,38,40,32,13,9])&&this.update()},this),keydown:t.proxy(this.keydown,this),paste:t.proxy(this.paste,this)};this.o.showOnFocus===!0&&(e.focus=t.proxy(this.show,this)),this.isInput?this._events=[[this.element,e]]:this.component&&this.hasInput?this._events=[[this.element.find("input"),e],[this.component,{click:t.proxy(this.show,this)}]]:this.element.is("div")?this.isInline=!0:this._events=[[this.element,{click:t.proxy(this.show,this)}]],this._events.push([this.element,"*",{blur:t.proxy(function(t){this._focused_from=t.target},this)}],[this.element,{blur:t.proxy(function(t){this._focused_from=t.target},this)}]),this.o.immediateUpdates&&this._events.push([this.element,{"changeYear changeMonth":t.proxy(function(t){this.update(t.date)},this)}]),this._secondaryEvents=[[this.picker,{click:t.proxy(this.click,this)}],[t(window),{resize:t.proxy(this.place,this)}],[t(document),{mousedown:t.proxy(function(t){this.element.is(t.target)||this.element.find(t.target).length||this.picker.is(t.target)||this.picker.find(t.target).length||this.picker.hasClass("datepicker-inline")||this.hide()},this)}]]},_attachEvents:function(){this._detachEvents(),this._applyEvents(this._events)},_detachEvents:function(){this._unapplyEvents(this._events)},_attachSecondaryEvents:function(){this._detachSecondaryEvents(),this._applyEvents(this._secondaryEvents)},_detachSecondaryEvents:function(){this._unapplyEvents(this._secondaryEvents)},_trigger:function(e,a){var i=a||this.dates.get(-1),s=this._utc_to_local(i);this.element.trigger({type:e,date:s,dates:t.map(this.dates,this._utc_to_local),format:t.proxy(function(t,e){0===arguments.length?(t=this.dates.length-1,e=this.o.format):"string"==typeof t&&(e=t,t=this.dates.length-1),e=e||this.o.format;var a=this.dates.get(t);return v.formatDate(a,e,this.o.language)},this)})},show:function(){return this.element.attr("readonly")&&this.o.enableOnReadonly===!1?void 0:(this.isInline||this.picker.appendTo(this.o.container),this.place(),this.picker.show(),this._attachSecondaryEvents(),this._trigger("show"),(window.navigator.msMaxTouchPoints||"ontouchstart"in document)&&this.o.disableTouchKeyboard&&t(this.element).blur(),this)},hide:function(){return this.isInline?this:this.picker.is(":visible")?(this.focusDate=null,this.picker.hide().detach(),this._detachSecondaryEvents(),this.viewMode=this.o.startView,this.showMode(),this.o.forceParse&&(this.isInput&&this.element.val()||this.hasInput&&this.element.find("input").val())&&this.setValue(),this._trigger("hide"),this):this},remove:function(){return this.hide(),this._detachEvents(),this._detachSecondaryEvents(),this.picker.remove(),delete this.element.data().datepicker,this.isInput||delete this.element.data().date,this},paste:function(e){var a;if(e.originalEvent.clipboardData&&e.originalEvent.clipboardData.types&&-1!==t.inArray("text/plain",e.originalEvent.clipboardData.types))a=e.originalEvent.clipboardData.getData("text/plain");else{if(!window.clipboardData)return;a=window.clipboardData.getData("Text")}this.setDate(a),this.update(),e.preventDefault()},_utc_to_local:function(t){return t&&new Date(t.getTime()+6e4*t.getTimezoneOffset())},_local_to_utc:function(t){return t&&new Date(t.getTime()-6e4*t.getTimezoneOffset())},_zero_time:function(t){return t&&new Date(t.getFullYear(),t.getMonth(),t.getDate())},_zero_utc_time:function(t){return t&&new Date(Date.UTC(t.getUTCFullYear(),t.getUTCMonth(),t.getUTCDate()))},getDates:function(){return t.map(this.dates,this._utc_to_local)},getUTCDates:function(){return t.map(this.dates,function(t){return new Date(t)})},getDate:function(){return this._utc_to_local(this.getUTCDate())},getUTCDate:function(){var t=this.dates.get(-1);return"undefined"!=typeof t?new Date(t):null},clearDates:function(){var t;this.isInput?t=this.element:this.component&&(t=this.element.find("input")),t&&t.val(""),this.update(),this._trigger("changeDate"),this.o.autoclose&&this.hide()},setDates:function(){var e=t.isArray(arguments[0])?arguments[0]:arguments;return this.update.apply(this,e),this._trigger("changeDate"),this.setValue(),this},setUTCDates:function(){var e=t.isArray(arguments[0])?arguments[0]:arguments;return this.update.apply(this,t.map(e,this._utc_to_local)),this._trigger("changeDate"),this.setValue(),this},setDate:n("setDates"),setUTCDate:n("setUTCDates"),setValue:function(){var t=this.getFormattedDate();return this.isInput?this.element.val(t):this.component&&this.element.find("input").val(t),this},getFormattedDate:function(a){a===e&&(a=this.o.format);var i=this.o.language;return t.map(this.dates,function(t){return v.formatDate(t,a,i)}).join(this.o.multidateSeparator)},setStartDate:function(t){return this._process_options({startDate:t}),this.update(),this.updateNavArrows(),this},setEndDate:function(t){return this._process_options({endDate:t}),this.update(),this.updateNavArrows(),this},setDaysOfWeekDisabled:function(t){return this._process_options({daysOfWeekDisabled:t}),this.update(),this.updateNavArrows(),this},setDaysOfWeekHighlighted:function(t){return this._process_options({daysOfWeekHighlighted:t}),this.update(),this},setDatesDisabled:function(t){this._process_options({datesDisabled:t}),this.update(),this.updateNavArrows()},place:function(){if(this.isInline)return this;var e=this.picker.outerWidth(),a=this.picker.outerHeight(),i=10,s=t(this.o.container),n=s.width(),r=s.scrollTop(),o=s.offset(),h=[];this.element.parents().each(function(){var e=t(this).css("z-index");"auto"!==e&&0!==e&&h.push(parseInt(e))});var l=Math.max.apply(Math,h)+this.o.zIndexOffset,d=this.component?this.component.parent().offset():this.element.offset(),c=this.component?this.component.outerHeight(!0):this.element.outerHeight(!1),u=this.component?this.component.outerWidth(!0):this.element.outerWidth(!1),p=d.left-o.left,f=d.top-o.top;this.picker.removeClass("datepicker-orient-top datepicker-orient-bottom datepicker-orient-right datepicker-orient-left"),"auto"!==this.o.orientation.x?(this.picker.addClass("datepicker-orient-"+this.o.orientation.x),"right"===this.o.orientation.x&&(p-=e-u)):d.left<0?(this.picker.addClass("datepicker-orient-left"),p-=d.left-i):p+e>n?(this.picker.addClass("datepicker-orient-right"),p=d.left+u-e):this.picker.addClass("datepicker-orient-left");var g,m=this.o.orientation.y;if("auto"===m&&(g=-r+f-a,m=0>g?"bottom":"top"),this.picker.addClass("datepicker-orient-"+m),"top"===m?f-=a+parseInt(this.picker.css("padding-top")):f+=c,this.o.rtl){var v=n-(p+u);this.picker.css({top:f,right:v,zIndex:l})}else this.picker.css({top:f,left:p,zIndex:l});return this},_allow_update:!0,update:function(){if(!this._allow_update)return this;var e=this.dates.copy(),a=[],i=!1;return arguments.length?(t.each(arguments,t.proxy(function(t,e){e instanceof Date&&(e=this._local_to_utc(e)),a.push(e)},this)),i=!0):(a=this.isInput?this.element.val():this.element.data("date")||this.element.find("input").val(),a=a&&this.o.multidate?a.split(this.o.multidateSeparator):[a],delete this.element.data().date),a=t.map(a,t.proxy(function(t){return v.parseDate(t,this.o.format,this.o.language)},this)),a=t.grep(a,t.proxy(function(t){return t<this.o.startDate||t>this.o.endDate||!t},this),!0),this.dates.replace(a),this.dates.length?this.viewDate=new Date(this.dates.get(-1)):this.viewDate<this.o.startDate?this.viewDate=new Date(this.o.startDate):this.viewDate>this.o.endDate?this.viewDate=new Date(this.o.endDate):this.viewDate=this.o.defaultViewDate,i?this.setValue():a.length&&String(e)!==String(this.dates)&&this._trigger("changeDate"),!this.dates.length&&e.length&&this._trigger("clearDate"),this.fill(),this.element.change(),this},fillDow:function(){var t=this.o.weekStart,e="<tr>";for(this.o.calendarWeeks&&(this.picker.find(".datepicker-days .datepicker-switch").attr("colspan",function(t,e){return parseInt(e)+1}),e+='<th class="cw">&#160;</th>');t<this.o.weekStart+7;)e+='<th class="dow">'+m[this.o.language].daysMin[t++%7]+"</th>";e+="</tr>",this.picker.find(".datepicker-days thead").append(e)},fillMonths:function(){for(var t="",e=0;12>e;)t+='<span class="month">'+m[this.o.language].monthsShort[e++]+"</span>";this.picker.find(".datepicker-months td").html(t)},setRange:function(e){e&&e.length?this.range=t.map(e,function(t){return t.valueOf()}):delete this.range,this.fill()},getClassNames:function(e){var a=[],i=this.viewDate.getUTCFullYear(),n=this.viewDate.getUTCMonth(),r=new Date;return e.getUTCFullYear()<i||e.getUTCFullYear()===i&&e.getUTCMonth()<n?a.push("old"):(e.getUTCFullYear()>i||e.getUTCFullYear()===i&&e.getUTCMonth()>n)&&a.push("new"),this.focusDate&&e.valueOf()===this.focusDate.valueOf()&&a.push("focused"),this.o.todayHighlight&&e.getUTCFullYear()===r.getFullYear()&&e.getUTCMonth()===r.getMonth()&&e.getUTCDate()===r.getDate()&&a.push("today"),-1!==this.dates.contains(e)&&a.push("active"),(e.valueOf()<this.o.startDate||e.valueOf()>this.o.endDate||-1!==t.inArray(e.getUTCDay(),this.o.daysOfWeekDisabled))&&a.push("disabled"),-1!==t.inArray(e.getUTCDay(),this.o.daysOfWeekHighlighted)&&a.push("highlighted"),this.o.datesDisabled.length>0&&t.grep(this.o.datesDisabled,function(t){return s(e,t)}).length>0&&a.push("disabled","disabled-date"),this.range&&(e>this.range[0]&&e<this.range[this.range.length-1]&&a.push("range"),-1!==t.inArray(e.valueOf(),this.range)&&a.push("selected"),e.valueOf()===this.range[0]&&a.push("range-start"),e.valueOf()===this.range[this.range.length-1]&&a.push("range-end")),a},fill:function(){var i,s=new Date(this.viewDate),n=s.getUTCFullYear(),r=s.getUTCMonth(),o=this.o.startDate!==-(1/0)?this.o.startDate.getUTCFullYear():-(1/0),h=this.o.startDate!==-(1/0)?this.o.startDate.getUTCMonth():-(1/0),l=this.o.endDate!==1/0?this.o.endDate.getUTCFullYear():1/0,d=this.o.endDate!==1/0?this.o.endDate.getUTCMonth():1/0,c=m[this.o.language].today||m.en.today||"",u=m[this.o.language].clear||m.en.clear||"",p=m[this.o.language].titleFormat||m.en.titleFormat;if(!isNaN(n)&&!isNaN(r)){this.picker.find(".datepicker-days thead .datepicker-switch").text(v.formatDate(new a(n,r),p,this.o.language)),this.picker.find("tfoot .today").text(c).toggle(this.o.todayBtn!==!1),this.picker.find("tfoot .clear").text(u).toggle(this.o.clearBtn!==!1),this.picker.find("thead .datepicker-title").text(this.o.title).toggle(""!==this.o.title),this.updateNavArrows(),this.fillMonths();var f=a(n,r-1,28),g=v.getDaysInMonth(f.getUTCFullYear(),f.getUTCMonth());f.setUTCDate(g),f.setUTCDate(g-(f.getUTCDay()-this.o.weekStart+7)%7);var y=new Date(f);f.getUTCFullYear()<100&&y.setUTCFullYear(f.getUTCFullYear()),y.setUTCDate(y.getUTCDate()+42),y=y.valueOf();for(var D,k=[];f.valueOf()<y;){if(f.getUTCDay()===this.o.weekStart&&(k.push("<tr>"),this.o.calendarWeeks)){var w=new Date(+f+(this.o.weekStart-f.getUTCDay()-7)%7*864e5),_=new Date(Number(w)+(11-w.getUTCDay())%7*864e5),b=new Date(Number(b=a(_.getUTCFullYear(),0,1))+(11-b.getUTCDay())%7*864e5),C=(_-b)/864e5/7+1;k.push('<td class="cw">'+C+"</td>")}if(D=this.getClassNames(f),D.push("day"),this.o.beforeShowDay!==t.noop){var T=this.o.beforeShowDay(this._utc_to_local(f));T===e?T={}:"boolean"==typeof T?T={enabled:T}:"string"==typeof T&&(T={classes:T}),T.enabled===!1&&D.push("disabled"),T.classes&&(D=D.concat(T.classes.split(/\s+/))),T.tooltip&&(i=T.tooltip)}D=t.unique(D),k.push('<td class="'+D.join(" ")+'"'+(i?' title="'+i+'"':"")+">"+f.getUTCDate()+"</td>"),i=null,f.getUTCDay()===this.o.weekEnd&&k.push("</tr>"),f.setUTCDate(f.getUTCDate()+1)}this.picker.find(".datepicker-days tbody").empty().append(k.join(""));var x=this.picker.find(".datepicker-months").find(".datepicker-switch").text(this.o.maxViewMode<2?"Months":n).end().find("span").removeClass("active");if(t.each(this.dates,function(t,e){e.getUTCFullYear()===n&&x.eq(e.getUTCMonth()).addClass("active")}),(o>n||n>l)&&x.addClass("disabled"),n===o&&x.slice(0,h).addClass("disabled"),n===l&&x.slice(d+1).addClass("disabled"),this.o.beforeShowMonth!==t.noop){var M=this;t.each(x,function(e,a){if(!t(a).hasClass("disabled")){var i=new Date(n,e,1),s=M.o.beforeShowMonth(i);s===!1&&t(a).addClass("disabled")}})}k="",n=10*parseInt(n/10,10);var U=this.picker.find(".datepicker-years").find(".datepicker-switch").text(n+"-"+(n+9)).end().find("td");n-=1;for(var A,F=t.map(this.dates,function(t){return t.getUTCFullYear()}),V=-1;11>V;V++){if(A=["year"],i=null,-1===V?A.push("old"):10===V&&A.push("new"),-1!==t.inArray(n,F)&&A.push("active"),(o>n||n>l)&&A.push("disabled"),this.o.beforeShowYear!==t.noop){var S=this.o.beforeShowYear(new Date(n,0,1));S===e?S={}:"boolean"==typeof S?S={enabled:S}:"string"==typeof S&&(S={classes:S}),S.enabled===!1&&A.push("disabled"),S.classes&&(A=A.concat(S.classes.split(/\s+/))),S.tooltip&&(i=S.tooltip)}k+='<span class="'+A.join(" ")+'"'+(i?' title="'+i+'"':"")+">"+n+"</span>",n+=1}U.html(k)}},updateNavArrows:function(){if(this._allow_update){var t=new Date(this.viewDate),e=t.getUTCFullYear(),a=t.getUTCMonth();switch(this.viewMode){case 0:this.o.startDate!==-(1/0)&&e<=this.o.startDate.getUTCFullYear()&&a<=this.o.startDate.getUTCMonth()?this.picker.find(".prev").css({visibility:"hidden"}):this.picker.find(".prev").css({visibility:"visible"}),this.o.endDate!==1/0&&e>=this.o.endDate.getUTCFullYear()&&a>=this.o.endDate.getUTCMonth()?this.picker.find(".next").css({visibility:"hidden"}):this.picker.find(".next").css({visibility:"visible"});break;case 1:case 2:this.o.startDate!==-(1/0)&&e<=this.o.startDate.getUTCFullYear()||this.o.maxViewMode<2?this.picker.find(".prev").css({visibility:"hidden"}):this.picker.find(".prev").css({visibility:"visible"}),this.o.endDate!==1/0&&e>=this.o.endDate.getUTCFullYear()||this.o.maxViewMode<2?this.picker.find(".next").css({visibility:"hidden"}):this.picker.find(".next").css({visibility:"visible"})}}},click:function(e){e.preventDefault(),e.stopPropagation();var i,s,n,r=t(e.target).closest("span, td, th");if(1===r.length)switch(r[0].nodeName.toLowerCase()){case"th":switch(r[0].className){case"datepicker-switch":this.showMode(1);break;case"prev":case"next":var o=v.modes[this.viewMode].navStep*("prev"===r[0].className?-1:1);switch(this.viewMode){case 0:this.viewDate=this.moveMonth(this.viewDate,o),this._trigger("changeMonth",this.viewDate);break;case 1:case 2:this.viewDate=this.moveYear(this.viewDate,o),1===this.viewMode&&this._trigger("changeYear",this.viewDate)}this.fill();break;case"today":var h=new Date;h=a(h.getFullYear(),h.getMonth(),h.getDate(),0,0,0),this.showMode(-2);var l="linked"===this.o.todayBtn?null:"view";this._setDate(h,l);break;case"clear":this.clearDates()}break;case"span":r.hasClass("disabled")||(this.viewDate.setUTCDate(1),r.hasClass("month")?(n=1,s=r.parent().find("span").index(r),i=this.viewDate.getUTCFullYear(),this.viewDate.setUTCMonth(s),this._trigger("changeMonth",this.viewDate),1===this.o.minViewMode?(this._setDate(a(i,s,n)),this.showMode()):this.showMode(-1)):(n=1,s=0,i=parseInt(r.text(),10)||0,this.viewDate.setUTCFullYear(i),this._trigger("changeYear",this.viewDate),2===this.o.minViewMode&&this._setDate(a(i,s,n)),this.showMode(-1)),this.fill());break;case"td":r.hasClass("day")&&!r.hasClass("disabled")&&(n=parseInt(r.text(),10)||1,i=this.viewDate.getUTCFullYear(),s=this.viewDate.getUTCMonth(),r.hasClass("old")?0===s?(s=11,i-=1):s-=1:r.hasClass("new")&&(11===s?(s=0,i+=1):s+=1),this._setDate(a(i,s,n)))}this.picker.is(":visible")&&this._focused_from&&t(this._focused_from).focus(),delete this._focused_from},_toggle_multidate:function(t){var e=this.dates.contains(t);if(t||this.dates.clear(),-1!==e?(this.o.multidate===!0||this.o.multidate>1||this.o.toggleActive)&&this.dates.remove(e):this.o.multidate===!1?(this.dates.clear(),this.dates.push(t)):this.dates.push(t),"number"==typeof this.o.multidate)for(;this.dates.length>this.o.multidate;)this.dates.remove(0)},_setDate:function(t,e){e&&"date"!==e||this._toggle_multidate(t&&new Date(t)),e&&"view"!==e||(this.viewDate=t&&new Date(t)),this.fill(),this.setValue(),e&&"view"===e||this._trigger("changeDate");var a;this.isInput?a=this.element:this.component&&(a=this.element.find("input")),a&&a.change(),!this.o.autoclose||e&&"date"!==e||this.hide()},moveMonth:function(t,e){if(!r(t))return this.o.defaultViewDate;if(!e)return t;var a,i,s=new Date(t.valueOf()),n=s.getUTCDate(),o=s.getUTCMonth(),h=Math.abs(e);if(e=e>0?1:-1,1===h)i=-1===e?function(){return s.getUTCMonth()===o}:function(){return s.getUTCMonth()!==a},a=o+e,s.setUTCMonth(a),(0>a||a>11)&&(a=(a+12)%12);else{for(var l=0;h>l;l++)s=this.moveMonth(s,e);a=s.getUTCMonth(),s.setUTCDate(n),i=function(){return a!==s.getUTCMonth()}}for(;i();)s.setUTCDate(--n),s.setUTCMonth(a);return s},moveYear:function(t,e){return this.moveMonth(t,12*e)},dateWithinRange:function(t){return t>=this.o.startDate&&t<=this.o.endDate},keydown:function(t){if(!this.picker.is(":visible"))return void((40===t.keyCode||27===t.keyCode)&&(this.show(),t.stopPropagation()));var e,a,s,n=!1,r=this.focusDate||this.viewDate;switch(t.keyCode){case 27:this.focusDate?(this.focusDate=null,this.viewDate=this.dates.get(-1)||this.viewDate,this.fill()):this.hide(),t.preventDefault(),t.stopPropagation();break;case 37:case 39:if(!this.o.keyboardNavigation)break;e=37===t.keyCode?-1:1,t.ctrlKey?(a=this.moveYear(this.dates.get(-1)||i(),e),s=this.moveYear(r,e),this._trigger("changeYear",this.viewDate)):t.shiftKey?(a=this.moveMonth(this.dates.get(-1)||i(),e),s=this.moveMonth(r,e),this._trigger("changeMonth",this.viewDate)):(a=new Date(this.dates.get(-1)||i()),a.setUTCDate(a.getUTCDate()+e),s=new Date(r),s.setUTCDate(r.getUTCDate()+e)),this.dateWithinRange(s)&&(this.focusDate=this.viewDate=s,this.setValue(),this.fill(),t.preventDefault());break;case 38:case 40:if(!this.o.keyboardNavigation)break;e=38===t.keyCode?-1:1,t.ctrlKey?(a=this.moveYear(this.dates.get(-1)||i(),e),s=this.moveYear(r,e),this._trigger("changeYear",this.viewDate)):t.shiftKey?(a=this.moveMonth(this.dates.get(-1)||i(),e),s=this.moveMonth(r,e),this._trigger("changeMonth",this.viewDate)):(a=new Date(this.dates.get(-1)||i()),a.setUTCDate(a.getUTCDate()+7*e),s=new Date(r),s.setUTCDate(r.getUTCDate()+7*e)),this.dateWithinRange(s)&&(this.focusDate=this.viewDate=s,this.setValue(),this.fill(),t.preventDefault());break;case 32:break;case 13:if(!this.o.forceParse)break;r=this.focusDate||this.dates.get(-1)||this.viewDate,this.o.keyboardNavigation&&(this._toggle_multidate(r),n=!0),this.focusDate=null,this.viewDate=this.dates.get(-1)||this.viewDate,this.setValue(),this.fill(),this.picker.is(":visible")&&(t.preventDefault(),"function"==typeof t.stopPropagation?t.stopPropagation():t.cancelBubble=!0,this.o.autoclose&&this.hide());break;case 9:this.focusDate=null,this.viewDate=this.dates.get(-1)||this.viewDate,this.fill(),this.hide()}if(n){this.dates.length?this._trigger("changeDate"):this._trigger("clearDate");var o;this.isInput?o=this.element:this.component&&(o=this.element.find("input")),o&&o.change()}},showMode:function(t){t&&(this.viewMode=Math.max(this.o.minViewMode,Math.min(this.o.maxViewMode,this.viewMode+t))),this.picker.children("div").hide().filter(".datepicker-"+v.modes[this.viewMode].clsName).show(),this.updateNavArrows()}};var c=function(e,a){this.element=t(e),this.inputs=t.map(a.inputs,function(t){return t.jquery?t[0]:t}),delete a.inputs,p.call(t(this.inputs),a).on("changeDate",t.proxy(this.dateUpdated,this)),this.pickers=t.map(this.inputs,function(e){return t(e).data("datepicker")}),this.updateDates()};c.prototype={updateDates:function(){this.dates=t.map(this.pickers,function(t){return t.getUTCDate()}),this.updateRanges()},updateRanges:function(){var e=t.map(this.dates,function(t){return t.valueOf()});t.each(this.pickers,function(t,a){a.setRange(e)})},dateUpdated:function(e){if(!this.updating){this.updating=!0;var a=t(e.target).data("datepicker");if("undefined"!=typeof a){var i=a.getUTCDate(),s=t.inArray(e.target,this.inputs),n=s-1,r=s+1,o=this.inputs.length;if(-1!==s){if(t.each(this.pickers,function(t,e){e.getUTCDate()||e.setUTCDate(i)}),i<this.dates[n])for(;n>=0&&i<this.dates[n];)this.pickers[n--].setUTCDate(i);else if(i>this.dates[r])for(;o>r&&i>this.dates[r];)this.pickers[r++].setUTCDate(i);this.updateDates(),delete this.updating}}}},remove:function(){t.map(this.pickers,function(t){t.remove()}),delete this.element.data().datepicker}};var u=t.fn.datepicker,p=function(a){var i=Array.apply(null,arguments);i.shift();var s;if(this.each(function(){var e=t(this),n=e.data("datepicker"),r="object"==typeof a&&a;if(!n){var l=o(this,"date"),u=t.extend({},f,l,r),p=h(u.language),g=t.extend({},f,p,l,r);if(e.hasClass("input-daterange")||g.inputs){var m={inputs:g.inputs||e.find("input").toArray()};e.data("datepicker",n=new c(this,t.extend(g,m)))}else e.data("datepicker",n=new d(this,g))}"string"==typeof a&&"function"==typeof n[a]&&(s=n[a].apply(n,i))}),s===e||s instanceof d||s instanceof c)return this;if(this.length>1)throw new Error("Using only allowed for the collection of a single element ("+a+" function)");return s};t.fn.datepicker=p;var f=t.fn.datepicker.defaults={autoclose:!1,beforeShowDay:t.noop,beforeShowMonth:t.noop,beforeShowYear:t.noop,calendarWeeks:!1,clearBtn:!1,toggleActive:!1,daysOfWeekDisabled:[],daysOfWeekHighlighted:[],datesDisabled:[],endDate:1/0,forceParse:!0,format:"mm/dd/yyyy",keyboardNavigation:!0,language:"en",minViewMode:0,maxViewMode:2,multidate:!1,multidateSeparator:",",orientation:"auto",rtl:!1,startDate:-(1/0),startView:0,todayBtn:!1,todayHighlight:!1,weekStart:0,disableTouchKeyboard:!1,enableOnReadonly:!0,container:"body",immediateUpdates:!1,title:""},g=t.fn.datepicker.locale_opts=["format","rtl","weekStart"];t.fn.datepicker.Constructor=d;var m=t.fn.datepicker.dates={en:{days:["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],daysShort:["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],daysMin:["Su","Mo","Tu","We","Th","Fr","Sa"],months:["January","February","March","April","May","June","July","August","September","October","November","December"],monthsShort:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],today:"Today",clear:"Clear",titleFormat:"MM yyyy"}},v={modes:[{clsName:"days",navFnc:"Month",navStep:1},{clsName:"months",navFnc:"FullYear",navStep:1},{clsName:"years",navFnc:"FullYear",navStep:10}],isLeapYear:function(t){return t%4===0&&t%100!==0||t%400===0},getDaysInMonth:function(t,e){return[31,v.isLeapYear(t)?29:28,31,30,31,30,31,31,30,31,30,31][e]},validParts:/dd?|DD?|mm?|MM?|yy(?:yy)?/g,nonpunctuation:/[^ -\/:-@\[\u3400-\u9fff-`{-~\t\n\r]+/g,parseFormat:function(t){if("function"==typeof t.toValue&&"function"==typeof t.toDisplay)return t;var e=t.replace(this.validParts,"\x00").split("\x00"),a=t.match(this.validParts);if(!e||!e.length||!a||0===a.length)throw new Error("Invalid date format.");return{separators:e,parts:a}},parseDate:function(i,s,n){function r(){var t=this.slice(0,u[l].length),e=u[l].slice(0,t.length);return t.toLowerCase()===e.toLowerCase()}if(!i)return e;if(i instanceof Date)return i;if("string"==typeof s&&(s=v.parseFormat(s)),s.toValue)return s.toValue(i,s,n);var o,h,l,c=/([\-+]\d+)([dmwy])/,u=i.match(/([\-+]\d+)([dmwy])/g);if(/^[\-+]\d+[dmwy]([\s,]+[\-+]\d+[dmwy])*$/.test(i)){for(i=new Date,l=0;l<u.length;l++)switch(o=c.exec(u[l]),h=parseInt(o[1]),o[2]){case"d":i.setUTCDate(i.getUTCDate()+h);break;case"m":i=d.prototype.moveMonth.call(d.prototype,i,h);break;case"w":i.setUTCDate(i.getUTCDate()+7*h);break;case"y":i=d.prototype.moveYear.call(d.prototype,i,h)}return a(i.getUTCFullYear(),i.getUTCMonth(),i.getUTCDate(),0,0,0)}u=i&&i.match(this.nonpunctuation)||[],i=new Date;var p,f,g={},y=["yyyy","yy","M","MM","m","mm","d","dd"],D={yyyy:function(t,e){return t.setUTCFullYear(e)},yy:function(t,e){return t.setUTCFullYear(2e3+e)},m:function(t,e){if(isNaN(t))return t;for(e-=1;0>e;)e+=12;for(e%=12,t.setUTCMonth(e);t.getUTCMonth()!==e;)t.setUTCDate(t.getUTCDate()-1);return t},d:function(t,e){return t.setUTCDate(e)}};D.M=D.MM=D.mm=D.m,D.dd=D.d,i=a(i.getFullYear(),i.getMonth(),i.getDate(),0,0,0);var k=s.parts.slice();if(u.length!==k.length&&(k=t(k).filter(function(e,a){return-1!==t.inArray(a,y)}).toArray()),u.length===k.length){var w;for(l=0,w=k.length;w>l;l++){if(p=parseInt(u[l],10),o=k[l],isNaN(p))switch(o){case"MM":f=t(m[n].months).filter(r),p=t.inArray(f[0],m[n].months)+1;break;case"M":f=t(m[n].monthsShort).filter(r),p=t.inArray(f[0],m[n].monthsShort)+1}g[o]=p}var _,b;for(l=0;l<y.length;l++)b=y[l],b in g&&!isNaN(g[b])&&(_=new Date(i),D[b](_,g[b]),isNaN(_)||(i=_))}return i},formatDate:function(e,a,i){if(!e)return"";if("string"==typeof a&&(a=v.parseFormat(a)),a.toDisplay)return a.toDisplay(e,a,i);var s={d:e.getUTCDate(),D:m[i].daysShort[e.getUTCDay()],DD:m[i].days[e.getUTCDay()],m:e.getUTCMonth()+1,M:m[i].monthsShort[e.getUTCMonth()],MM:m[i].months[e.getUTCMonth()],yy:e.getUTCFullYear().toString().substring(2),yyyy:e.getUTCFullYear()};s.dd=(s.d<10?"0":"")+s.d,s.mm=(s.m<10?"0":"")+s.m,e=[];for(var n=t.extend([],a.separators),r=0,o=a.parts.length;o>=r;r++)n.length&&e.push(n.shift()),e.push(s[a.parts[r]]);return e.join("")},headTemplate:'<thead><tr><th colspan="7" class="datepicker-title"></th></tr><tr><th class="prev">&#171;</th><th colspan="5" class="datepicker-switch"></th><th class="next">&#187;</th></tr></thead>',contTemplate:'<tbody><tr><td colspan="7"></td></tr></tbody>',footTemplate:'<tfoot><tr><th colspan="7" class="today"></th></tr><tr><th colspan="7" class="clear"></th></tr></tfoot>'};v.template='<div class="datepicker"><div class="datepicker-days"><table class=" table-condensed">'+v.headTemplate+"<tbody></tbody>"+v.footTemplate+'</table></div><div class="datepicker-months"><table class="table-condensed">'+v.headTemplate+v.contTemplate+v.footTemplate+'</table></div><div class="datepicker-years"><table class="table-condensed">'+v.headTemplate+v.contTemplate+v.footTemplate+"</table></div></div>",t.fn.datepicker.DPGlobal=v,t.fn.datepicker.noConflict=function(){return t.fn.datepicker=u,this},t.fn.datepicker.version="1.5.0",t(document).on("focus.datepicker.data-api click.datepicker.data-api",'[data-provide="datepicker"]',function(e){
var a=t(this);a.data("datepicker")||(e.preventDefault(),p.call(a,"show"))}),t(function(){p.call(t('[data-provide="datepicker-inline"]'))})}),!function(t){if(!t.browser){var e=function(t){t=t.toLowerCase();var e=/(chrome)[ \/]([\w.]+)/.exec(t)||/(webkit)[ \/]([\w.]+)/.exec(t)||/(opera)(?:.*version|)[ \/]([\w.]+)/.exec(t)||/(msie) ([\w.]+)/.exec(t)||t.indexOf("compatible")<0&&/(mozilla)(?:.*? rv:([\w.]+)|)/.exec(t)||[];return e[2]||"0"};t.browser={mozilla:/mozilla/.test(navigator.userAgent.toLowerCase())&&!/webkit/.test(navigator.userAgent.toLowerCase()),webkit:/webkit/.test(navigator.userAgent.toLowerCase()),opera:/opera/.test(navigator.userAgent.toLowerCase()),msie:/msie/.test(navigator.userAgent.toLowerCase()),android:navigator.userAgent.toLowerCase().indexOf("mozilla/5.0")>-1&&navigator.userAgent.toLowerCase().indexOf("android ")>-1&&navigator.userAgent.toLowerCase().indexOf("applewebkit")>-1,version:e(navigator.userAgent)}}var a=(null!=window.orientation,t.browser.opera||t.browser.mozilla&&parseFloat(t.browser.version.substr(0,3))<1.9?"input":"paste"),i=function(e){e=t.event.fix(e||window.event),e.type="paste";var a=e.target;setTimeout(function(){t.event.dispatch.call(a,e)},1)};t.event.special.paste={setup:function(){this.addEventListener?this.addEventListener(a,i,!1):this.attachEvent&&this.attachEvent("on"+a,i)},teardown:function(){this.removeEventListener?this.removeEventListener(a,i,!1):this.detachEvent&&this.detachEvent("on"+a,i)}},t.extend({mask:{rules:{z:/[a-z]/,Z:/[A-Z]/,a:/[a-zA-Z]/,"*":/[0-9a-zA-Z]/,"@":/[0-9a-zA-ZçÇáàãâéèêíìóòôõúùü]/},keyRepresentation:{8:"backspace",9:"tab",13:"enter",16:"shift",17:"control",18:"alt",27:"esc",33:"page up",34:"page down",35:"end",36:"home",37:"left",38:"up",39:"right",40:"down",45:"insert",46:"delete",116:"f5",123:"f12",224:"command"},signals:{"+":"","-":"-"},options:{attr:"alt",mask:null,type:"fixed",maxLength:-1,defaultValue:"",signal:!1,textAlign:!0,selectCharsOnFocus:!0,autoTab:!0,setSize:!1,fixedChars:"[(),.:/ -]",onInvalid:function(){},onValid:function(){},onOverflow:function(){},onFocus:function(){},onBlur:function(){}},masks:{phone:{mask:"(99) 9999-9999"},"phone-us":{mask:"(999) 999-9999"},cpf:{mask:"999.999.999-99"},cnpj:{mask:"99.999.999/9999-99"},date:{mask:"39/19/9999"},"date-us":{mask:"19/39/9999"},cep:{mask:"99999-999"},time:{mask:"29:59"},cc:{mask:"9999 9999 9999 9999"},integer:{mask:"999.999.999.999",type:"reverse"},decimal:{mask:"99,999.999.999.999",type:"reverse",defaultValue:"000"},"decimal-us":{mask:"99.999,999,999,999",type:"reverse",defaultValue:"000"},"signed-decimal":{mask:"99,999.999.999.999",type:"reverse",defaultValue:"+000"},"signed-decimal-us":{mask:"99,999.999.999.999",type:"reverse",defaultValue:"+000"}},init:function(){if(!this.hasInit){var e,a=this,i=this.keyRepresentation;for(this.ignore=!1,e=0;9>=e;e++)this.rules[e]=new RegExp("[0-"+e+"]");this.keyRep=i,this.ignoreKeys=[],t.each(i,function(t){a.ignoreKeys.push(parseInt(t,10))}),this.hasInit=!0}},set:function(e,a){var i=this,s=t(e),n="maxLength";return a=a||{},this.init(),s.each(function(){a.attr&&(i.options.attr=a.attr);var e=t(this),s=t.extend({},i.options),r=e.attr(s.attr),o="";if(o="string"==typeof a?a:""!==r?r:null,o&&(s.mask=o),i.masks[o]&&(s=t.extend(s,i.masks[o])),"object"==typeof a&&a.constructor!=Array&&(s=t.extend(s,a)),t.metadata&&(s=t.extend(s,e.metadata())),null!=s.mask){s.mask+="",e.data("mask")&&i.unset(e);var h=s.defaultValue,l="reverse"===s.type,d=new RegExp(s.fixedChars,"g");if(-1===s.maxLength&&(s.maxLength=e.attr(n)),s=t.extend({},s,{fixedCharsReg:new RegExp(s.fixedChars),fixedCharsRegG:d,maskArray:s.mask.split(""),maskNonFixedCharsArray:s.mask.replace(d,"").split("")}),"fixed"!=s.type&&!l||!s.setSize||e.attr("size")||e.attr("size",s.mask.length),l&&s.textAlign&&e.css("text-align","right"),""!==this.value||""!==h){var c=i.string(""!==this.value?this.value:h,s);this.defaultValue=c,e.val(c)}"infinite"==s.type&&(s.type="repeat"),e.data("mask",s),e.removeAttr(n),e.bind("keydown.mask",{func:i._onKeyDown,thisObj:i},i._onMask).bind("keypress.mask",{func:i._onKeyPress,thisObj:i},i._onMask).bind("keyup.mask",{func:i._onKeyUp,thisObj:i},i._onMask).bind("paste.mask",{func:i._onPaste,thisObj:i},i._onMask).bind("drop.mask",{func:i._onPaste,thisObj:i},i._onMask).bind("focus.mask",i._onFocus).bind("blur.mask",i._onBlur).bind("change.mask",i._onChange)}})},unset:function(e){var a=t(e);return a.each(function(){var e=t(this);if(e.data("mask")){var a=e.data("mask").maxLength;-1!=a&&e.attr("maxLength",a),e.unbind(".mask").removeData("mask")}})},string:function(e,a){this.init();var i={};switch("string"!=typeof e&&(e=String(e)),typeof a){case"string":this.masks[a]?i=t.extend(i,this.masks[a]):i.mask=a;break;case"object":i=a}i.fixedChars||(i.fixedChars=this.options.fixedChars);var s=new RegExp(i.fixedChars),n=new RegExp(i.fixedChars,"g");if("reverse"===i.type&&i.defaultValue&&"undefined"!=typeof this.signals[i.defaultValue.charAt(0)]){var r=e.charAt(0);i.signal="undefined"!=typeof this.signals[r]?this.signals[r]:this.signals[i.defaultValue.charAt(0)],i.defaultValue=i.defaultValue.substring(1)}return this.__maskArray(e.split(""),i.mask.replace(n,"").split(""),i.mask.split(""),i.type,i.maxLength,i.defaultValue,s,i.signal)},_onFocus:function(e){var a=t(this),i=a.data("mask");i.inputFocusValue=a.val(),i.changed=!1,i.selectCharsOnFocus&&a.select(),i.onFocus(this,e)},_onBlur:function(e){var a=t(this),i=a.data("mask");i.inputFocusValue==a.val()||i.changed||a.trigger("change"),i.onBlur(this,e)},_onChange:function(){t(this).data("mask").changed=!0},_onMask:function(e){var a=e.data.thisObj,i={};return i._this=e.target,i.$this=t(i._this),i.data=i.$this.data("mask"),i.$this.attr("readonly")||!i.data?!0:(i[i.data.type]=!0,i.value=i.$this.val(),i.nKey=a.__getKeyNumber(e),i.range=a.__getRange(i._this),i.valueArray=i.value.split(""),e.data.func.call(a,e,i))},_onKeyDown:function(e,a){if(this.ignore=t.inArray(a.nKey,this.ignoreKeys)>-1||(e.ctrlKey||e.metaKey||e.altKey)&&e.key,this.ignore){var i=this.keyRep[a.nKey];a.data.onValid.call(a._this,i||"",a.nKey)}return!0},_onKeyUp:function(t,e){return 9===e.nKey||16===e.nKey?!0:e.repeat?(this.__autoTab(e),!0):this._onPaste(t,e)},_onPaste:function(e,a){a.reverse&&this.__changeSignal(e.type,a);var i=this.__maskArray(a.valueArray,a.data.maskNonFixedCharsArray,a.data.maskArray,a.data.type,a.data.maxLength,a.data.defaultValue,a.data.fixedCharsReg,a.data.signal);return a.$this.val(i),!a.reverse&&a.data.defaultValue.length&&a.range.start===a.range.end&&this.__setRange(a._this,a.range.start,a.range.end),!t.browser.msie&&!t.browser.safari||a.reverse||this.__setRange(a._this,a.range.start,a.range.end),this.ignore?!0:(this.__autoTab(a),!0)},_onKeyPress:function(t,e){if(this.ignore)return!0;e.reverse&&this.__changeSignal(t.type,e);var a=String.fromCharCode(e.nKey),i=e.range.start,s=e.value,n=e.data.maskArray;if(e.reverse){var r=s.substr(0,i),o=s.substr(e.range.end,s.length);s=r+a+o,e.data.signal&&i-e.data.signal.length>0&&(i-=e.data.signal.length)}var h=s.replace(e.data.fixedCharsRegG,"").split(""),l=this.__extraPositionsTill(i,n,e.data.fixedCharsReg);if(e.rsEp=i+l,e.repeat&&(e.rsEp=0),!this.rules[n[e.rsEp]]||-1!=e.data.maxLength&&h.length>=e.data.maxLength&&e.repeat)return e.data.onOverflow.call(e._this,a,e.nKey),!1;if(!this.rules[n[e.rsEp]].test(a))return e.data.onInvalid.call(e._this,a,e.nKey),!1;e.data.onValid.call(e._this,a,e.nKey);var d=this.__maskArray(h,e.data.maskNonFixedCharsArray,n,e.data.type,e.data.maxLength,e.data.defaultValue,e.data.fixedCharsReg,e.data.signal,l);return e.repeat||e.$this.val(d),e.reverse?this._keyPressReverse(t,e):e.fixed?this._keyPressFixed(t,e):!0},_keyPressFixed:function(t,e){return e.range.start==e.range.end?(0===e.rsEp&&0===e.value.length||e.rsEp<e.value.length)&&this.__setRange(e._this,e.rsEp,e.rsEp+1):this.__setRange(e._this,e.range.start,e.range.end),!0},_keyPressReverse:function(e,a){return t.browser.msie&&(0===a.range.start&&0===a.range.end||a.range.start!=a.range.end)&&this.__setRange(a._this,a.value.length),!1},__autoTab:function(t){if(t.data.autoTab&&(t.$this.val().length>=t.data.maskArray.length&&!t.repeat||-1!=t.data.maxLength&&t.valueArray.length>=t.data.maxLength&&t.repeat)){var e=this.__getNextInput(t._this,t.data.autoTab);e&&(t.$this.trigger("blur"),e.focus().select())}},__changeSignal:function(t,e){if(e.data.signal!==!1){var a="paste"===t?e.value.charAt(0):String.fromCharCode(e.nKey);this.signals&&"undefined"!=typeof this.signals[a]&&(e.data.signal=this.signals[a])}},__getKeyNumber:function(t){return t.charCode||t.keyCode||t.which},__maskArray:function(t,e,a,i,s,n,r,o,h){switch("reverse"===i&&t.reverse(),t=this.__removeInvalidChars(t,e,"repeat"===i||"infinite"===i),n&&(t=this.__applyDefaultValue.call(t,n)),t=this.__applyMask(t,a,h,r),i){case"reverse":return t.reverse(),(o||"")+t.join("").substring(t.length-a.length);case"infinite":case"repeat":var l=t.join("");return-1!==s&&t.length>=s?l.substring(0,s):l;default:return t.join("").substring(0,a.length)}return""},__applyDefaultValue:function(t){var e,a=t.length,i=this.length;for(e=i-1;e>=0&&this[e]==t.charAt(0);e--)this.pop();for(e=0;a>e;e++)this[e]||(this[e]=t.charAt(e));return this},__removeInvalidChars:function(t,e,a){for(var i=0,s=0;i<t.length;i++)e[s]&&this.rules[e[s]]&&!this.rules[e[s]].test(t[i])&&(t.splice(i,1),a||s--,i--),a||s++;return t},__applyMask:function(t,e,a,i){"undefined"==typeof a&&(a=0);for(var s=0;s<t.length+a;s++)e[s]&&i.test(e[s])&&t.splice(s,0,e[s]);return t},__extraPositionsTill:function(t,e,a){for(var i=0;a.test(e[t++]);)i++;return i},__getNextInput:function(e,a){var i=e.form;if(null==i)return null;var s,n=i.elements,r=t.inArray(e,n)+1,o=n.length,h=null;for(s=r;o>s;s++)if(h=t(n[s]),this.__isNextInput(h,a))return h;var l,d,c=document.forms,u=t.inArray(e.form,c)+1,p=c.length;for(l=u;p>l;l++)for(d=c[l].elements,o=d.length,s=0;o>s;s++)if(h=t(d[s]),this.__isNextInput(h,a))return h;return null},__isNextInput:function(t,e){var a=t.get(0);return a&&(a.offsetWidth>0||a.offsetHeight>0)&&"FIELDSET"!=a.nodeName&&(e===!0||"string"==typeof e&&t.is(e))},__setRange:function(t,e,a){if("undefined"==typeof a&&(a=e),t.setSelectionRange)t.setSelectionRange(e,a);else{var i=t.createTextRange();i.collapse(),i.moveStart("character",e),i.moveEnd("character",a-e),i.select()}},__getRange:function(e){if(!t.browser.msie&&!t.browser.android)return{start:e.selectionStart,end:e.selectionEnd};var a={start:0,end:0},i=document.selection.createRange();return a.start=0-i.duplicate().moveStart("character",-1e5),a.end=a.start+i.text.length,a},unmaskedVal:function(e){return t(e).val().replace(t.mask.fixedCharsRegG,"")}}}),t.fn.extend({setMask:function(e){return t.mask.set(this,e)},unsetMask:function(){return t.mask.unset(this)},unmaskedVal:function(){return t.mask.unmaskedVal(this[0])}})}(jQuery);var Activation={init:function(){this.total=$("#total-amount"),this.kitDetails=$("#kit-details"),this.freightDetails=$("#freight-details"),this.chooseKit(),this.chooseForm(),this.chooseAddress(),this.chooseInstallment(),$("#activation-loading").hide()},setDetails:function(t,e,a){t.data("amount",e),t.find(".name").text(a),t.find(".amount").text(number_format(e,2,",",""))},calculateTotal:function(t){void 0==t&&(amount=parseFloat(this.kitDetails.data("amount")),freight=parseFloat(this.freightDetails.data("amount")),t=amount+freight),this.total.data("amount",t).text(number_format(t,2,",",""))},calculateInstallments:function(){total=this.total.data("amount"),total>0&&$.ajax({url:"/maxipago/installments/"+total,success:function(t){var e="";$.each(t,function(t,a){amount=number_format(a.amount,2,",",""),text=a.quantity+" x "+amount+" = R$ "+number_format(a.total,2,",",""),e+='<option value="'+a.quantity+'" data-total="'+a.total+'">'+text+"</option>"}),$("#select-installments").html(e)}})},chooseKit:function(){$('input[name="order[kit_id]"]:checked').length>0&&Activation.setKitAttributes($('input[name="order[kit_id]"]:checked')),$('input[name="order[kit_id]"]').on("change",function(){Activation.setKitAttributes($(this))})},setKitAttributes:function(t){amount=t.data("amount"),this.setKitDetails(amount,t.data("name")),Activation.getCostumes(t.val()),Activation.calculateTotal(),Activation.calculateInstallments()},setKitDetails:function(t,e){this.setDetails(this.kitDetails,t,e)},chooseAddress:function(){$('input[name="order[address_id]"]:checked').length>0&&Activation.setAddressAttributes($('input[name="order[address_id]"]:checked')),$('input[name="order[address_id]"]').on("change",function(){Activation.setAddressAttributes($(this))})},setAddressAttributes:function(t){Activation.calculateFreight(t),Activation.calculateTotal(),Activation.calculateInstallments()},calculateFreight:function(t){var e=t.data("cep");""==e?(Activation.setFreightDetails(0,"Retirar no CD:"),Activation.calculateTotal(),Activation.calculateInstallments()):(Application.showLoading(),$.ajax({url:"/correios/freight/"+e,success:function(t){var e=t.success?t.price:0,a=e>0?"Frete PAC:":"Retirar no CD:";Activation.setFreightDetails(e,a),Activation.calculateTotal(),Activation.calculateInstallments()}}).always(function(){Application.hideLoading()}))},setFreightDetails:function(t,e){this.setDetails(this.freightDetails,t,e)},chooseInstallment:function(){$("#select-installments").on("change",function(){total=parseFloat($(this).find("option:selected").data("total")),Activation.calculateTotal(total)})},getCostumes:function(t){""!=t&&($("#activation-loading").fadeIn(),$.ajax({url:"/costumes/"+t+"/activation",success:function(t){$("#kit-costumes").html(t)}}).always(function(){$("#activation-loading").fadeOut()}))},chooseForm:function(){Activation.cardType($('input[name="card[form]"]:checked').val()),Activation.showCardForm($('input[name="card[form]"]:checked').val()),$('input[name="card[form]"]').on("change",function(){Activation.showCardForm($(this).val()),Activation.cardType($(this).val())})},isCard:function(t){return"paypal"==t||"boleto"==t||void 0==t},cardType:function(t){Activation.isCard(t)?$("#card-type").val(0):$("#card-type").val(1)},showCardForm:function(t){Activation.isCard(t)?$("#activation-form-card").slideUp():$("#activation-form-card").slideDown()}};$(document).on("ready",function(){Activation.init()});var Distributors={init:function(){this.seekSponsor()},seekSponsor:function(){var t=$('input[name="distributor[sponsor_id]"]'),e=$("#seek-sponsor");e.on("click",function(){alert(t.val()),""!=t.val()&&(Application.showLoading(),$.ajax({url:"/backoffice/distributors/seeksponsor/"+t.val(),success:function(t){t.success?$("#distributor[sponsor_name]").val(t.name):alert()}}).always(function(){Application.hideLoading()}))})}};$(document).on("ready",function(){Distributors.init()});var Application={init:function(){this.confirmation()},hideLoading:function(){$("#ajax-loading").fadeOut("slow")},showLoading:function(){$("#ajax-loading").fadeIn("fast")},confirmation:function(){$("[data-confirm]").click(function(t){return console.log($(this).data("confirm")),confirmation=confirm($(this).data("confirm"))})}};$(document).on("ready",function(){Application.init()});var MeioMask={init:function(){this.extras(),this.simple()},simple:function(){$("input[data-mask]").each(function(){var t=$(this);t.setMask(t.data("mask"))})},extras:function(){$.mask.rules.n=/[0-9]?/,$.mask.masks["phone-br"]={mask:"(99) 9999-9999n"},$.mask.masks.percent={mask:"199"}}};$(document).on("ready",function(){MeioMask.init()});var Datepicker={init:function(){this.simple()},simple:function(){$(".bs-datepicker").datepicker({format:"dd/mm/yyyy"})}};$(document).on("ready",function(){Datepicker.init()});
//# sourceMappingURL=admin.js.map
